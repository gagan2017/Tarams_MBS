﻿using System;
using Android.App;
using Android.Content;
using MathrubhumiSales.Mobile.Droid;
[assembly: Xamarin.Forms.Dependency(typeof(AppProperties))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class AppProperties : IAppProperties
	{
		ISharedPreferences prefs = Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private);

		public string get(string pKey)
		{
			return prefs.GetString(pKey, "");

		}

		public double getDouble(string pKey)
		{
			return double.Parse(prefs.GetString(pKey, "0"));
		}

		public int getInt(string pKey)
		{
			return int.Parse(prefs.GetString(pKey, "0"));
		}

		public void set(string pData, string pKey)
		{
			var prefEditor = prefs.Edit();
			prefEditor.PutString(pKey, pData);
			prefEditor.Commit();
		}

		public void setDouble(double pData, string pKey)
		{
			var prefEditor = prefs.Edit();
			prefEditor.PutString(pKey, pData.ToString());
			prefEditor.Commit();
		}

		public void setInt(int pData, string pKey)
		{
			var prefEditor = prefs.Edit();
			prefEditor.PutString(pKey, pData.ToString());
			prefEditor.Commit();
		}
	}
}
