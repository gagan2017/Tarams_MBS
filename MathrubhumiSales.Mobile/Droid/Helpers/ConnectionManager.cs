﻿using System;
using Android.App;
using Android.Content;
using Android.Net;
using MathrubhumiSales.Mobile.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(ConnectionManager))]

namespace MathrubhumiSales.Mobile.Droid
{
	public class ConnectionManager :Activity ,IConnectionCheck
	{
		public ConnectionManager()
		{
		}
		public bool IsConnected()
		{
			ConnectivityManager cm = (ConnectivityManager)GetSystemService(Context.ConnectivityService);
			NetworkInfo netInfo = cm.ActiveNetworkInfo;
			return netInfo != null && netInfo.IsConnectedOrConnecting;
		}
	}
}
