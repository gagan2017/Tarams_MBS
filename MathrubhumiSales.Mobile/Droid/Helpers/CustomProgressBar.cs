﻿using System;
using MathrubhumiSales.Mobile.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(CustomProgressBar))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class CustomProgressBar:ICustomPrgressBar
	{
		public void Start(double Time)
		{
			LogInActivity.Show();
		}

		public void Stop()
		{
			LogInActivity.Hide();	
		}
	}
}
