﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using MathrubhumiSales.Mobile.Api;

namespace MathrubhumiSales.Mobile.Droid
{
	[Activity(NoHistory = true, Theme = "@android:style/Theme.Black.NoTitleBar.Fullscreen")]
	public class SplashActivity : Activity
	{
		Intent intent;
		Task fadeInFadeOut;
		ImageView splashImage;
		ImageView Refresh;
		Task startupWork;
		Animation animationRotate, animation;
		TextView errorText;
		bool IsFadeIn = true;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.splash_layout);
			splashImage = FindViewById<ImageView>(Resource.Id.splashScreenImage);
			Refresh = FindViewById<ImageView>(Resource.Id.refreshButon);
			animation = AnimationUtils.LoadAnimation(this.ApplicationContext, Resource.Animation.blinking_animation);
			splashImage.StartAnimation(animation);
		
			animationRotate = AnimationUtils.LoadAnimation(this.ApplicationContext, Resource.Animation.rotate_animation);
			Refresh.StartAnimation(animationRotate);
			Refresh.Visibility = ViewStates.Gone;
			// Create your application here
		}

		void Refresh_Click(object sender, EventArgs e)
		{
			if (!isOnline())
			{
				Toast.MakeText(ApplicationContext, "No Internet connect and try again", ToastLength.Long).Show();
				Refresh.Animation.Cancel();
				return;
			}
			Refresh.StartAnimation(animationRotate);
			startupWork = new Task(() => { SimulateStartup(); });
			startupWork.Start();
		}

		protected override void OnResume()
		{
			base.OnResume();
			startupWork = new Task(() => { SimulateStartup(); });
			startupWork.Start();


		}
		public override void OnBackPressed() { }

		// Simulates background work that happens behind the splash screen
		async void SimulateStartup()
		{

			Refresh.Click -= Refresh_Click;
			if (!isOnline())
			{
				Refresh.Click += Refresh_Click;
				Refresh.Animation.Cancel();
				return;
			}
		
		
			intent = new Intent(Application.Context, typeof(LogInActivity));
			StartActivity(intent);
			this.Finish();
		}
		public bool isOnline()
		{
			ConnectivityManager cm = (ConnectivityManager)GetSystemService(Context.ConnectivityService);

			NetworkInfo netInfo = cm.ActiveNetworkInfo;
			return netInfo != null && netInfo.IsConnectedOrConnecting;
		}
	}
}
