﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;

namespace MathrubhumiSales.Mobile.Droid
{

	[Activity(Label = "MathrubhumiSales.Mobile",MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

	public class LogInActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		static ProgressDialog progressDialog;
		static LogInActivity Current;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			Current = this;
			base.OnCreate(savedInstanceState);
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

			LoadApplication(new App(IsConnected()));

		}
		public bool IsConnected()
		{
			ConnectivityManager cm = (ConnectivityManager)GetSystemService(Context.ConnectivityService);
			NetworkInfo netInfo = cm.ActiveNetworkInfo;
			return netInfo != null && netInfo.IsConnectedOrConnecting;
		}
		public static void Show()
		{
			if (Current != null)
				progressDialog = ProgressDialog.Show(Current, "", "Loading ..", true, true);
		}
		public static void Hide()
		{
			if (Current != null)
				progressDialog.Hide();
		}
	}
}
