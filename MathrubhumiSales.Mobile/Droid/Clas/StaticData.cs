using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile.Droid
{
	public static class StaticData
	{
		public static UserModel User { get; set; }
		public static string name { get; set; }
		public static string ImageUrl { get; set; }
		public static string GoogleId { get; set; }
		public static string Email { get; set; }

	}
}
