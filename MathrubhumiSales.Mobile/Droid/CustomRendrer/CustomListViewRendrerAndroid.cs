﻿using System;
using Android.Views;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRendererAndroid))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class CustomListViewRendererAndroid : ListViewRenderer
	{
		double currentQuota;
		static CustomListView Current;
		public static void Tapped(CustomListViewCellContents view)
		{
			if (Current != null)
			{
				Current.invokeListTapped(view);
			}
		}
		public CustomListViewRendererAndroid()
		{

		}

		public override bool DispatchTouchEvent(MotionEvent touch)
		{
			currentQuota = ((touch.GetX() - TouchDispatcher.StartingBiasX) / (double)this.Width);
			//Console.WriteLine(currentQuota);

			Current = this.Element as CustomListView;
			if (touch.Action == MotionEventActions.Cancel)
			{
				if (TouchDispatcher.TouchingView != null && TouchDispatcher.TouchingView2 != null)
				{
					CustomListViewCellContents touchedElement = (TouchDispatcher.TouchingView as CustomListViewCellContents);

					(this.Element as CustomListView).invokeListSwyped(TouchDispatcher.TouchingView2);
					TouchDispatcher.TouchingView = TouchDispatcher.TouchingView2 = null;
				}


			}

			return base.DispatchTouchEvent(touch);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
		{
			base.OnElementChanged(e);
			var listView = this.Control as Android.Widget.ListView;
			listView.ChoiceMode = Android.Widget.ChoiceMode.Single;
			listView.SetBackgroundResource(Resource.Drawable.list_item_selector);
			this.Control.ItemsCanFocus = false;
			if (e.NewElement == null)
			{
				listView.NestedScrollingEnabled = true;
			}
			if (e.OldElement == null)
			{
				listView.NestedScrollingEnabled = true;
			}


		}

	}
}
