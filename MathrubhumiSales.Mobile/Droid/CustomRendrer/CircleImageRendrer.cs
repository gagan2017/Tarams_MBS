﻿using System;
using Android.Graphics;
using Android.Views;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CircleImage), typeof(CircleImageRendrer))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class CircleImageRendrer : ImageRenderer
	{
		int ImageAlpha;
		private readonly MyGestureListener _listener;
		private readonly GestureDetector _detector;

		public CircleImageRendrer()
		{
			_listener = new MyGestureListener();
			_detector = new GestureDetector(_listener);
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Image> e)
		{
			base.OnElementChanged(e);

			_listener.Obj = Element as CircleImage;

        		GenericMotion += (s, a) => _detector.OnTouchEvent(a.Event);
        		Touch += (s, a) => _detector.OnTouchEvent(a.Event);

			if (e.OldElement == null)
			{

				if ((int)Android.OS.Build.VERSION.SdkInt < 18)

					SetLayerType(Android.Views.LayerType.Software, null);
			}

		}


		protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
		{
			try
			{
				var radius = Math.Min(Width, Height) / 2;
				var strokeWidth = 10;
				radius -= strokeWidth / 2;

				//Create path to clip
				var path = new Path();
				path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
				canvas.Save();
				canvas.ClipPath(path);

				var result = base.DrawChild(canvas, child, drawingTime);

				canvas.Restore();

				// Create path for circle border
				path = new Path();
				path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);

				var paint = new Paint();
				paint.AntiAlias = true;
				paint.StrokeWidth = 5;
				paint.SetStyle(Paint.Style.Stroke);
				paint.Color = global::Android.Graphics.Color.Transparent;

				canvas.DrawPath(path, paint);

				//Properly dispose
				paint.Dispose();
				path.Dispose();
				return result;
			}
			catch (Exception ex)
			{

			}

			return base.DrawChild(canvas, child, drawingTime);
		}
	}
}
