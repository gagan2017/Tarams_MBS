﻿using System;
using MathrubhumiSales.Mobile.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(Entry), typeof(CustomEntryRenderer))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class CustomEntryRenderer : EntryRenderer
	{
		public CustomEntryRenderer()
		{
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
		{
			base.OnElementChanged(e);
			this.Control.SetBackgroundResource(Resource.Drawable.edit_text_background);
		}
	}
}
