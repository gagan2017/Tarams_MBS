﻿using System;
using MathrubhumiSales.Mobile.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(CustomPickerRenederer))]
namespace MathrubhumiSales.Mobile.Droid
{
	public class CustomPickerRenederer : PickerRenderer
	{
		protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);
			this.Element.HeightRequest = 40;
			this.Control.SetBackgroundResource(Resource.Drawable.gradient_spinner);
			this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

		}
		public CustomPickerRenederer()
		{

		}

	}
}
