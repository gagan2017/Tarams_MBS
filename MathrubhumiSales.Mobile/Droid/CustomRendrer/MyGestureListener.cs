﻿using System;
using Android.Views;

namespace MathrubhumiSales.Mobile.Droid
{
	public class MyGestureListener : GestureDetector.SimpleOnGestureListener
	{
		public ICustomGesture Obj { get; set; }
		public override bool OnDoubleTap(MotionEvent e)
		{
			return base.OnDoubleTap(e);

		}
		public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			if (Obj != null)
			{
				Obj.OnSwype();
			}
			return base.OnFling(e1, e2, velocityX, velocityY);


		}

		public override void OnLongPress(MotionEvent e)
		{
			base.OnLongPress(e);
			if (Obj != null)
			{
				Obj.OnLongPress();
			}

		}

		public override bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			return base.OnScroll(e1, e2, distanceX, distanceY);

		}

		public override void OnShowPress(MotionEvent e)
		{
			base.OnShowPress(e);
			if (Obj != null)
			{
				Obj.OnShowPress();
			}
		}

		public override bool OnSingleTapUp(MotionEvent e)
		{
			if (Obj != null)
			{
				Obj.OnTouched();
			}
			return base.OnSingleTapUp(e);
		}

		public override bool OnSingleTapConfirmed(MotionEvent e)
		{
			if (Obj != null)
			{
				Obj.OnSingleTap();
			}
			return base.OnSingleTapConfirmed(e);
		}
	}
}
