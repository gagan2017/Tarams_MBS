﻿using System;
using Android.Views;

namespace MathrubhumiSales.Mobile.Droid
{
	public class ListViewGesture : GestureDetector.SimpleOnGestureListener
	{
		public static void OnTapInvoke()
		{
			
		}
		public CustomListViewCellContents view;
		public ListViewGesture()
		{

		}
		public override void OnLongPress(MotionEvent e)
		{
			Console.WriteLine("OnLongPress");
			base.OnLongPress(e);
		}

		public override bool OnDoubleTap(MotionEvent e)
		{
			Console.WriteLine("OnDoubleTap");
			return base.OnDoubleTap(e);
		}

		public override bool OnDoubleTapEvent(MotionEvent e)
		{
			Console.WriteLine("OnDoubleTapEvent");
			return base.OnDoubleTapEvent(e);
		}

		public override bool OnSingleTapUp(MotionEvent e)
		{

			return base.OnSingleTapUp(e);
		}

		public override bool OnDown(MotionEvent e)
		{
			Console.WriteLine("OnDown");
			return base.OnDown(e);
		}

		public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			Console.WriteLine(e1 + " " + e2 + " velocityX  " + velocityX + " velocityY " + velocityY);
			return base.OnFling(e1, e2, velocityX, velocityY);
		}

		public override bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			Console.WriteLine("OnScroll" + distanceX + " " + distanceY);
			if (getApproxCondition(distanceX, distanceY))
			{
				TouchDispatcher.TouchingView2 = view;
			}
			return base.OnScroll(e1, e2, distanceX, distanceY);
		}

		bool getApproxCondition(float distanceX, float distanceY)
		{
			if (distanceX > 5)
			{
				if (distanceY < (distanceY + 3))
				{
					return true;
				}
			}
			return false;
		}



		public override void OnShowPress(MotionEvent e)
		{
			Console.WriteLine("OnShowPress");
			base.OnShowPress(e);
		}

		public override bool OnSingleTapConfirmed(MotionEvent e)
		{
			CustomListViewRendererAndroid.Tapped(view);
			Console.WriteLine("OnSingleTapConfirmed");
			return base.OnSingleTapConfirmed(e);
		}
	}
}
