﻿using System;
using Android.Views;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Droid;
using Xamarin.Forms.Platform.Android;
[assembly: Xamarin.Forms.ExportRenderer(typeof(CustomListViewCellContents), typeof(CustomViewRendrer))]

namespace MathrubhumiSales.Mobile.Droid
{
	public class CustomViewRendrer : ViewRenderer
	{
		private readonly ListViewGesture _listner;
		                            
		static void InvokeCurrentElement(CustomListViewCellContents View)
		{
			
		}
		private readonly GestureDetector _detector;
		CustomListViewCellContents currentElement;
		public CustomViewRendrer()
		{
			_listner = new ListViewGesture();
			_detector = new GestureDetector(_listner);
		}
		public override bool DispatchTouchEvent(MotionEvent touch)
		{

			if (TouchDispatcher.TouchingView == null && touch.ActionMasked == MotionEventActions.Down && (this.Element as CustomListViewCellContents).SwipeCompleted == false)
			{
				TouchDispatcher.TouchingView = Element as CustomListViewCellContents;
				TouchDispatcher.StartingBiasX = touch.GetX();
				TouchDispatcher.StartingBiasY = touch.GetY();
				TouchDispatcher.InitialTouch = DateTime.Now;
			}
			return base.DispatchTouchEvent(touch);
		}


		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged(e);
			_listner.view = Element as CustomListViewCellContents;
			if (e.NewElement == null)
			{
				this.GenericMotion -= HandleGenericMotion;
				this.Touch -= HandleTouch;
			}

			if (e.OldElement == null)
			{

				this.GenericMotion += HandleGenericMotion;
				this.Touch += HandleTouch;
			}



		}

		void HandleTouch(object sender, TouchEventArgs e)
		{
			_detector.OnTouchEvent(e.Event);
		}

		void HandleGenericMotion(object sender, GenericMotionEventArgs e)
		{
			_detector.OnTouchEvent(e.Event);
		}
	}
}
