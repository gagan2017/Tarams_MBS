﻿using System;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRendererIos))]

namespace MathrubhumiSales.Mobile.iOS
{
	public class CustomListViewRendererIos : ListViewRenderer
	{
		static CustomListView CurrentElement;
		public CustomListViewRendererIos()
		{

		}
		public static void swipedLeft(CustomListViewCellContents view)
		{
			if (CurrentElement != null)
			{
				CurrentElement.invokeListSwyped(view);
			}
		}
		public static void TappedOnItem()
		{
			
		}

		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				// Unsubscribe
			}

			if (e.NewElement != null)
			{
				CurrentElement = e.NewElement as CustomListView;
				// = e.NewElement as CustomListView;
				//	Control.Source = new NativeiOSListViewSource(e.NewElement as NativeListView);
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			/*if (e.PropertyName == NativeListView.ItemsProperty.PropertyName)
			{
				Control.Source = ;
			}*/
		}


	}
}
