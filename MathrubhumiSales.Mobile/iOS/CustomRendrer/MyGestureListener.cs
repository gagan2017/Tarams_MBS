﻿using System;
using UIKit;

namespace MathrubhumiSales.Mobile.iOS
{
	public class MyGestureListener
	{
		public UILongPressGestureRecognizer longPressGestureRecognizer;
		public UIPinchGestureRecognizer pinchGestureRecognizer;
		public UIPanGestureRecognizer panGestureRecognizer;
		public UISwipeGestureRecognizer swipeGestureRecognizer;
		public UIRotationGestureRecognizer rotationGestureRecognizer;
		public UITapGestureRecognizer tapGestureRecognizer;
		public ICustomGesture Obj { get; set; }
		public MyGestureListener(ICustomGesture obj)
		{
			this.Obj = obj;
			longPressGestureRecognizer = new UILongPressGestureRecognizer(CGUILongPressGestureRecognizer);
			pinchGestureRecognizer = new UIPinchGestureRecognizer(CGUIPinchGestureRecognizer);
			panGestureRecognizer = new UIPanGestureRecognizer(CGUISwipeGestureRecognizer);
			swipeGestureRecognizer = new UISwipeGestureRecognizer(CGUISwipeGestureRecognizer);
			rotationGestureRecognizer = new UIRotationGestureRecognizer(CGUIRotationGestureRecognizer);
			tapGestureRecognizer = new UITapGestureRecognizer(CGUITapGestureRecognizer);
		}



		void CGUILongPressGestureRecognizer()
		{
			if (Obj != null)
			{
				Obj.OnLongPress();
			}
		}
		void CGUITapGestureRecognizer()
		{
			if (Obj != null)
			{
				Obj.OnSingleTap();
			}
		}
		void CGUIPinchGestureRecognizer()
		{

			if (Obj != null)
			{
				Obj.OnTouched();
			}
		}
		void CGUISwipeGestureRecognizer()
		{
			if (Obj != null)
			{
				Obj.OnSwype();
			}
		}
		void CGUIRotationGestureRecognizer()
		{
		}
		void CGUIPanGestureRecognizer()
		{
			if (Obj != null)
			{
				Obj.OnTouched();
			}
		}

	}
}
