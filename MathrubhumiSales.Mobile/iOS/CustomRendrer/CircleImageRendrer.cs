﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(CircleImage), typeof(CircleImageRendrer))]

namespace MathrubhumiSales.Mobile.iOS
{
	public class CircleImageRendrer : ImageRenderer
	{
		MyGestureListener Listener { get; set; }
		public CircleImageRendrer()
		{


		}
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			var item = Element as CircleImage;
			if (Listener == null)
			{
				Listener = new MyGestureListener(item);
			}

			if (e.NewElement == null)
			{
				if (Listener.longPressGestureRecognizer != null)
				{
					this.RemoveGestureRecognizer(Listener.longPressGestureRecognizer);
				}
				if (Listener.pinchGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(Listener.pinchGestureRecognizer);
				}
				if (Listener.panGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(Listener.panGestureRecognizer);
				}
				if (Listener.swipeGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(Listener.swipeGestureRecognizer);
				}
				if (Listener.rotationGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(Listener.rotationGestureRecognizer);
				}
				if (Listener.tapGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(Listener.tapGestureRecognizer);
				}
			}

			if (e.OldElement == null)
			{
				this.AddGestureRecognizer(Listener.longPressGestureRecognizer);
				this.AddGestureRecognizer(Listener.pinchGestureRecognizer);
				this.AddGestureRecognizer(Listener.panGestureRecognizer);
				this.AddGestureRecognizer(Listener.swipeGestureRecognizer);
				this.AddGestureRecognizer(Listener.rotationGestureRecognizer);
				this.AddGestureRecognizer(Listener.tapGestureRecognizer);
			}


			CreateCircle();



		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				e.PropertyName == VisualElement.WidthProperty.PropertyName)
			{
				CreateCircle();
			}
		}
		private void CreateCircle()
		{
			try
			{
				double min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = (float)(min / 2.0);
				Control.Layer.MasksToBounds = false;
				Control.Layer.BorderColor = Color.Transparent.ToCGColor();
				Control.Layer.BorderWidth = 3;
				Control.ClipsToBounds = true;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Unable to create circle image: " + ex);
			}
		}
	}
}
