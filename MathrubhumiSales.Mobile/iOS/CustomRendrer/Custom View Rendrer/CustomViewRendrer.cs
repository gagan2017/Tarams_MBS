﻿using System;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(CustomListViewCellContents), typeof(CustomViewRendrerIos))]
namespace MathrubhumiSales.Mobile.iOS
{
	public class CustomViewRendrerIos : ViewRenderer
	{
		UISwipeGestureRecognizer swipeGestureRecognizer;
		UITapGestureRecognizer tapGestureRecognizer;
		public CustomViewRendrerIos()
		{

		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
			{

				if (tapGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(tapGestureRecognizer);
				}

				if (swipeGestureRecognizer != null)
				{

					this.RemoveGestureRecognizer(swipeGestureRecognizer);
				}

			}

			if (e.OldElement == null)
			{
				tapGestureRecognizer = new UITapGestureRecognizer((obj) =>
						{

						});
				swipeGestureRecognizer = new UISwipeGestureRecognizer((obj) =>
				{

					CustomListViewRendererIos.swipedLeft(e.NewElement as CustomListViewCellContents);
					Console.WriteLine("Swipe");

				})
				{
					Direction = UISwipeGestureRecognizerDirection.Left
				};
				this.AddGestureRecognizer(tapGestureRecognizer);
				this.AddGestureRecognizer(swipeGestureRecognizer);
			}
		}
	}
}
