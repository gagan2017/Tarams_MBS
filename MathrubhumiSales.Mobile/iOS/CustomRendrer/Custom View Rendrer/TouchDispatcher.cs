﻿using System;
namespace MathrubhumiSales.Mobile.iOS
{
	public static class TouchDispatcher
	{

		public static CustomListViewCellContents TouchingView { get; internal set; }
		public static float StartingBiasX { get; internal set; }
		public static float StartingBiasY { get; internal set; }
		public static float Width { get; internal set; }
		public static DateTime InitialTouch { get; internal set; }
		public static CustomListViewCellContents TouchingView2 { get; internal set; }
		public static CustomListViewCellContents Tap { get; internal set; }

		static TouchDispatcher()
		{
			TouchingView = null;
			StartingBiasX = 0;
			StartingBiasY = 0;
		}
	}
}
