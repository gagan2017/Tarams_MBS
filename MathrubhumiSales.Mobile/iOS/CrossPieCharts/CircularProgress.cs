﻿using System;
using UIKit;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile.iOS
{
	public class CircularProgress : UIView
	{
		int mDuration;
		int mProgress;

		Color mBackgroundColor;
		Color mPrimaryColor;
		float mStrokeWidth;
		public CircularProgress()
		{
		}
		public interface IOnProgressChangeListener
		{
			void onChange(int duration, int progress, float rate);
		}
		public IOnProgressChangeListener mOnChangeListener;
		public void setMax(int max)
		{
			if (max < 0)
			{
				max = 0;
			}
			mDuration = max;
		}
		public int getMax()
		{
			return mDuration;
		}

	}
}
