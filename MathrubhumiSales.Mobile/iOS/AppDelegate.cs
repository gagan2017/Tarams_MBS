﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using NControl.iOS;
using UIKit;

namespace MathrubhumiSales.Mobile.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		static UIActivityIndicatorView activitySpinner;

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			
			global::Xamarin.Forms.Forms.Init();
			global::Xamarin.Auth.Presenters.XamarinIOS.AuthenticationConfiguration.Init();
			activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray)
			{
				HidesWhenStopped = true
			};

			CustomProgressBar.StartLoader += ProgressEx_StartLoader;
			CustomProgressBar.StopLoader += ProgressEx_StopLoader;
			LoadApplication(new App(IsConnected()));
			return base.FinishedLaunching(app, options);
		}
		public bool IsConnected()
		{
			if (Reachability.InternetConnectionStatus() == NetworkStatus.ReachableViaCarrierDataNetwork ||
			   Reachability.InternetConnectionStatus() == NetworkStatus.ReachableViaWiFiNetwork)
				return true;
			return false;
		}
		void ProgressEx_StartLoader(object sender, EventArgs e)
		{
			InvokeOnMainThread(() =>
				{
					if (activitySpinner != null)
						activitySpinner.StartAnimating();
				});
		}
		void ProgressEx_StopLoader(object sender, EventArgs e)
		{
			InvokeOnMainThread(() =>
				{
					if (activitySpinner != null)
						activitySpinner.StopAnimating();
				});
		}

	}
}
/*new Shared.Models.UserModel()
			{
				AuthModel=new Shared.Models.AuthModel()
				{
					Email="gagan.mydata@gmail.com",
					 Token="3e649ba0-eeca-47c6-83d9-f72d71d87839"
				},
				ContactNumber="7894561237",
				FirstName="Monish",
				LastName="L",
				Id=1009,
				Designation=new Models.DesignationModel()
				{
					Id=9,
					Name="Dev1"
				},
				ProfilePicUrl="https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50 "

			})*/