﻿using System;
using MathrubhumiSales.Mobile.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(ConnectionManager))]
namespace MathrubhumiSales.Mobile.iOS
{
	public class ConnectionManager :IConnectionCheck
	{
		public bool IsConnected()
		{
			if(Reachability.InternetConnectionStatus()==NetworkStatus.ReachableViaCarrierDataNetwork||
			   Reachability.InternetConnectionStatus()==NetworkStatus.ReachableViaWiFiNetwork)
			return true;
			return false;
		}
	}
}
