﻿using System;
using Foundation;
using MathrubhumiSales.Mobile.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(AppProperties))]

namespace MathrubhumiSales.Mobile.iOS
{
	public class AppProperties : IAppProperties
	{
		public AppProperties()
		{
		}

		public string get(string pKey)
		{
			var res = NSUserDefaults.StandardUserDefaults.StringForKey(pKey);

			return NSUserDefaults.StandardUserDefaults.StringForKey(pKey);
		}

		public double getDouble(string pKey)
		{
			var res = NSUserDefaults.StandardUserDefaults.DoubleForKey(pKey);
			return NSUserDefaults.StandardUserDefaults.DoubleForKey(pKey);
		}

		public int getInt(string pKey)
		{
			var res = ((int)(NSUserDefaults.StandardUserDefaults.DoubleForKey(pKey)));
			return ((int)(NSUserDefaults.StandardUserDefaults.DoubleForKey(pKey)));
		}


		public void set(string pData, string pKey)
		{
			NSUserDefaults.StandardUserDefaults.SetString(pData, pKey);
		}

		public void setDouble(double pData, string pKey)
		{
			NSUserDefaults.StandardUserDefaults.SetDouble(pData, pKey);
		}

		public void setInt(int pData, string pKey)
		{
			NSUserDefaults.StandardUserDefaults.SetDouble(pData, pKey);
		}
	}
}
