﻿using System;
using MathrubhumiSales.Mobile.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(CustomProgressBar))]
namespace MathrubhumiSales.Mobile.iOS
{
	public class CustomProgressBar :ICustomPrgressBar
	{
		public static event EventHandler StartLoader;
		public static event EventHandler StopLoader;

		public void Start(double Time)
		{
			StartLoader?.Invoke(null, new EventArgs());
		}

		public void Stop()
		{
			StopLoader?.Invoke(null, new EventArgs());

		}
	}
}
