﻿using System;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class TempContentView : ContentView
	{
		public ContentView CurrentItemTapped;
		public CustomLabel text { get; set; }

		public Image close { get; set; }
		public TempContentView(string Text)
		{
			CurrentItemTapped = this;
			text = new CustomLabel { Text = Text };
			close = new Image()
			{
				HeightRequest = 15,
				WidthRequest = 15,
				Aspect = Aspect.Fill,
				Source = "close.png"
			};
			var content = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,

			};

			content.Children.Add(text);
			content.Children.Add(close);
			Content = content;
		}


	}
}

