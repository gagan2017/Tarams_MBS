﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class AddTaskListViewCell : ViewCell
	{
		public AddTaskListViewCell()
		{
			InitializeComponent();
			this.Tapped+= Handle_Tapped;
		}

		private void Handle_Tapped(object sender, EventArgs e)
		{
			checkBox.View_Tapped(sender,e);
		}
	}
}
