﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class DateTimeView : ContentView
	{
		public DateTime date { get; set; }
		public TimeSpan time { get; set; }
		public DateTime datetime { get; set; }
		public DateTimeView()
		{
			InitializeComponent();

			timePicker.Time = DateTime.Now.TimeOfDay;
			timePicker.PropertyChanged += Handle_TimeSelected;
			endatetime.PlaceholderColor = Color.Black;
			datetime = date = datePicker.Date = DateTime.Now;
			datePicker.Unfocused += DatePicker_Unfocused; ;
		}
		async void DatePicker_Unfocused(object sender, EventArgs e)
		{
			endatetime.IsVisible = true;
			Device.BeginInvokeOnMainThread(() =>
			{
				timePicker.Focus();
			});
		}
		async void Dattime_Tapped(object sender, EventArgs e)
		{
			endatetime.IsVisible = true;
			Device.BeginInvokeOnMainThread(() =>
			{
				datePicker.Focus();
			});
		}
		void Handle_DateSelected(object sender, Xamarin.Forms.DateChangedEventArgs e)
		{
			date = datePicker.Date;
			setdatetime();
		}

		void Handle_TimeSelected(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			setdatetime();
		}



		void setdatetime()
		{
			time = timePicker.Time;
			var timearray = JsonConvert.SerializeObject(time);
			var datetimeformat = JsonConvert.SerializeObject(date);
			try
			{
				var timearray1 = datetimeformat.Split('T');
				var timearray2 = timearray1[1].Split('+');

				var timearray3 = timearray.Split('"');
				timearray2[0] = timearray3[1].ToString();

				timearray1[1] = timearray2[0];
				datetimeformat = timearray1[0] + "T" + timearray1[1] + "\"";


				date = JsonConvert.DeserializeObject<DateTime>(datetimeformat);
				endatetime.Text = "date=" + FactoryDate._instance.getDate(date.Date) + " And Time=" + FactoryDate._instance.getTime(date.TimeOfDay);
			}
			catch (Exception e)
			{

			}




			//00:00:00
			//"\"2017-05-31T00:00:00+05:30\"
		}
	}
}
