﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ToDoDetailPage2 : ContentPage
	{
		ToDoDetailPage2ViewModel bc;

		public TapGestureRecognizer Tapped;
		public ToDoDetailPage2(CommentsBlueItem _blueItem)
		{
			Tapped = new TapGestureRecognizer();
			bc = new ToDoDetailPage2ViewModel(_blueItem.task);

			bc._blueItem = _blueItem;
			BindingContext = bc;
			InitializeComponent();
			bc.Start();
			listcmnt.ItemsSource = bc.listofcomments;
			Tapped.Tapped += Tapped_Tapped; ;
			attachImage.GestureRecognizers.Add(Tapped);
			listcmnt.IsPullToRefreshEnabled = true;
			listcmnt.Refreshing += Listcmnt_Refreshing;

		}

		async void Listcmnt_Refreshing(object sender, EventArgs e)
		{

			await bc.GetData();
			listcmnt.ItemsSource = bc.listofcomments;
			if (bc.listofcomments.Count > 0)
				listcmnt.ScrollTo(bc.listofcomments[bc.listofcomments.Count - 1], ScrollToPosition.End, true);
			listcmnt.IsRefreshing = false;
		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as Comments;

			if (e == null) return; // has been set to null, do not 'process' tapped event




			((ListView)sender).SelectedItem = null; //
		}

		async void Tapped_Tapped(object sender, EventArgs e)
		{
			var sen = sender as Image;
			if (sen.Id == attachImage.Id)
			{

			}
			if (sen.Id == sendImage.Id)
			{
				var res = await bc.Post(enComment.Text);

				if (!res)
				{
					await DisplayAlert("Error", "Couldnt comment", "Ok!");
				}
				else
				{
					enComment.Text = "";
				}
				listcmnt.BeginRefresh();
			}
		}


		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (((Entry)sender).Text.Trim().Length != 0)
			{
				if (!sendImage.GestureRecognizers.Contains(Tapped))
				{
					sendImage.GestureRecognizers.Add(Tapped);
				}
			}
			else
			{
				sendImage.GestureRecognizers.Remove(Tapped);
			}
		}

	}
}
