﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientPage : ContentPage
	{

		string currentPage = "";
		ContentView temp;
		AddStyles styles;
		ClientPageViewModel clientModel;
		List<CommentsRedItem> predList;
		CommentsRedItem Pred;
		Factory factory;
		ClientListingModel clients;
		public event EventHandler Refresh;
		static ClientPage cP;
		public ClientPage(CommentsRedItem pred, ClientListingModel clients)
		{
			this.predList = new List<CommentsRedItem>();
			if (clients != null)
			{
				clientModel = new ClientPageViewModel(this.predList, clients);
			}
			if (pred != null)
			{
				clientModel = new ClientPageViewModel(this.predList, pred);
			}
			this.clients = clients;
			Pred = pred;
			InitializeComponent();
			styles = new AddStyles();
			styles.setLabelClckedStyle(one);
			clientModel.Update += ClientModel_Update;
			Refresh += ClientPage_Refresh;
			cP = this;
		}

		async void ClientPage_Refresh(object sender, EventArgs e)
		{
			cP.predList = new List<CommentsRedItem>();
			if (cP.clients != null)
			{
				var list = await new CommentFactory().FindEleFromApi(clients.model.Id);
				foreach (var item in list)
				{
					cP.predList.Add(item);
				}
			}
			if (cP.Pred != null)
			{
				var list = await DummyListItemViewModel.Instance.getObjOfTappedItem(Pred);
				foreach (var item in list)
				{
					cP.predList.Add(item);
				}
			}
			factory.GiveRefresh(predList);

		}

		async public static Task refresh()
		{
			cP.Refresh.Invoke(null, new EventArgs());
		}

		void ClientModel_Update(object sender, EventArgs e)
		{

			clientName.Text = predList[0].Heading;
			temp = new Factory(predList).getContent(one.Text);
			content.Children.Add(temp);
		}


		void Handle_Tapped(object sender, System.EventArgs e)
		{
			if (temp != null)
			{
				content.Children.Remove(temp);
			}

			styles.setLabelClckedStyle((Label)sender);
			factory = new Factory(predList);
				temp = factory.getContent(((Label)sender).Text);
			currentPage = ((Label)sender).Text;
			content.Children.Add(temp);
		}
		void Handle_Image_Tapped(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(getCurrentPage(currentPage));
		}

		Page getCurrentPage(string currentPage)
		{
			if (currentPage == "Meetings") return new ScheduleMeeting();
			if (currentPage == "Proposals") return new AddProposalPage();
			return null;
		}
	}
}
