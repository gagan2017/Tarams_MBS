﻿using System;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public static class CustomGesture
	{
		public static readonly BindableProperty TapCommandProperty = BindableProperty.CreateAttached("TapCommand", typeof(ICommand), typeof(CustomGesture), null, propertyChanged: CommandChanged);
		public static readonly BindableProperty TapCommand2Property = BindableProperty.CreateAttached("TapCommand2", typeof(Command<Point>), typeof(CustomGesture), null, propertyChanged: CommandChanged);
		public static readonly BindableProperty SwipeLeftCommandProperty = BindableProperty.CreateAttached("SwipeLeftCommand", typeof(ICommand), typeof(CustomGesture), null, propertyChanged: CommandChanged);
		public static readonly BindableProperty SwipeRightCommandProperty = BindableProperty.CreateAttached("SwipeRightCommand", typeof(ICommand), typeof(CustomGesture), null, propertyChanged: CommandChanged);
		public static readonly BindableProperty SwipeTopCommandProperty = BindableProperty.CreateAttached("SwipeTopCommand", typeof(ICommand), typeof(CustomGesture), null, propertyChanged: CommandChanged);
		public static readonly BindableProperty SwipeBottomCommandProperty = BindableProperty.CreateAttached("SwipeBottomCommand", typeof(ICommand), typeof(CustomGesture), null, propertyChanged: CommandChanged);

		public static ICommand GetTapCommand(BindableObject view)
		{
			return (ICommand)view.GetValue(TapCommandProperty);
		}

		public static Command<Point> GetTapCommand2(BindableObject view)
		{
			return (Command<Point>)view.GetValue(TapCommand2Property);
		}

		public static void SetTapCommand(BindableObject view, ICommand value)
		{
			view.SetValue(TapCommandProperty, value);
		}

		public static void SetTapCommand2(BindableObject view, Command<Point> value)
		{
			view.SetValue(TapCommand2Property, value);
		}

		private static void CommandChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var view = bindable as View;
			if (view != null)
			{
				var effect = GetOrCreateEffect(view);
			}
		}

		public static ICommand GetSwipeLeftCommand(BindableObject view)
		{
			return (ICommand)view.GetValue(SwipeLeftCommandProperty);
		}

		public static void SetSwipeLeftCommand(BindableObject view, ICommand value)
		{
			view.SetValue(SwipeLeftCommandProperty, value);
		}

		public static ICommand GetSwipeRightCommand(BindableObject view)
		{
			return (ICommand)view.GetValue(SwipeRightCommandProperty);
		}

		public static void SetSwipeRightCommand(BindableObject view, ICommand value)
		{
			view.SetValue(SwipeRightCommandProperty, value);
		}

		public static ICommand GetSwipeTopCommand(BindableObject view)
		{
			return (ICommand)view.GetValue(SwipeTopCommandProperty);
		}

		public static void SetSwipeTopCommand(BindableObject view, ICommand value)
		{
			view.SetValue(SwipeTopCommandProperty, value);
		}

		public static ICommand GetSwipeBottomCommand(BindableObject view)
		{
			return (ICommand)view.GetValue(SwipeBottomCommandProperty);
		}

		public static void SetSwipeBottomCommand(BindableObject view, ICommand value)
		{
			view.SetValue(SwipeBottomCommandProperty, value);
		}

		private static GestureEffect GetOrCreateEffect(View view)
		{
			var effect = (GestureEffect)view.Effects.FirstOrDefault(e => e is GestureEffect);
			if (effect == null)
			{
				effect = new GestureEffect();
				view.Effects.Add(effect);
			}
			return effect;
		}

		class GestureEffect : RoutingEffect
		{
			public GestureEffect() : base("CustomGesture.CustomGestureRenderer")
			{
			}
		}
	}
}
