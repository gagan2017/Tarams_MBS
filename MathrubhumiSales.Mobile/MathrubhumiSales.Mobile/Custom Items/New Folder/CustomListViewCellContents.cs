using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CustomListViewCellContents : ContentView
	{
		public readonly BindableProperty CommentBlueItemProperty =
			BindableProperty.Create<CustomListViewCellContents, string>(w => w.CommentBlueItemId, default(string));

		public string CommentBlueItemId
		{
			get
			{
				return (string)GetValue(CommentBlueItemProperty);
			}
			set
			{
				SetValue(CommentBlueItemProperty, value);
			}
		}
		public bool IsMoved = false;
		public bool SwipeCompleted { get; set; }

		public bool PerformTranslation(double currentQuota)
		{
			if (Math.Abs(currentQuota) >= 0.3)
			{
				return true;
			}
			return false;
		}

		public bool Complete(double currentQuota)
		{
			if (Math.Abs(currentQuota) >= 0.3)
			{
				return true;
			}
			return false;
		}
	}
}
