﻿using System;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CustomBarGraph : StackLayout
	{
		double ipercentageBelow;
		LayoutOptions textalign;
		LayoutOptions textalignbottom;
		string barText = "";
		string bottomBarText = "";
		public string TopBarText
		{
			get { return barText; }
			set
			{
				barText = value;
				lblPercentageTop.Text = value.ToString();
			}
		}
		public string BottomBarText
		{
			get { return bottomBarText; }
			set
			{
				ispercentage = false;
				bottomBarText = value;
				lblPercentageTop.Text = value.ToString();
			}
		}
		public LayoutOptions TextAlignBottom
		{
			get
			{
				return textalignbottom;
			}
			set
			{
				textalignbottom = value;
				lblPercentageBelow.HorizontalOptions = value;
			}
		}


		public LayoutOptions TextAlignTop
		{
			get
			{
				return textalign;
			}
			set
			{
				textalign = value;
				lblPercentageTop.HorizontalOptions = value;
			}
		}
		double ipercentageTop;
		int height = 10;
		bool percentageBelow = true;
		bool ispercentage = true;
		public bool IsPercentage
		{
			get { return ispercentage; }
			set { ispercentage = value; }
		}


		int width = 100;

		public int Widths
		{
			get { return width; }
			set
			{
				width = value; FirstLayer.WidthRequest = value; SecondLayer.WidthRequest = value;
			}
		}

		public static readonly BindableProperty BottomColorProperty =
					BindableProperty.Create<CustomBarGraph, Color>(w => w.BottomColor, default(Color));

		public Color BottomColor
		{
			get { return (Color)GetValue(BottomColorProperty); }
			set
			{
				SetValue(BottomColorProperty, value);

			}
		}
		public static readonly BindableProperty TopColorProperty =
		   BindableProperty.Create<CustomBarGraph, Color>(w => w.TopColor, default(Color));

		public Color TopColor
		{
			get { return (Color)GetValue(TopColorProperty); }
			set
			{
				SetValue(TopColorProperty, value);

			}
		}
		protected override void OnPropertyChanged(string propertyName)
		{
			base.OnPropertyChanged(propertyName);

			if (propertyName == BottomColorProperty.PropertyName)
			{
				SecondLayer.BackgroundColor = BottomColor;
			}
			if (propertyName == TopColorProperty.PropertyName)
			{
				FirstLayer.BackgroundColor = TopColor;
			}

		}

		public bool ShowPercentageBelow
		{
			get { return percentageBelow; }
			set
			{
				percentageBelow = value;

				lblPercentageBelow.IsVisible = value;
			}
		}
		bool percentageTop = true;
		public bool ShowPercentageTop
		{
			get { return percentageTop; }
			set
			{
				percentageTop = value;
				lblPercentageTop.IsVisible = value;
			}
		}
		public int Heights
		{
			get { return height; }
			set
			{
				height = value; FirstLayer.HeightRequest = value; 
				SecondLayer.HeightRequest = value;
				lblPercentageBelow.Font = Font.SystemFontOfSize(value-4);
				lblPercentageTop.Font = Font.SystemFontOfSize(value-4);

			}
		}

		public double Percentage
		{
			get { return ipercentageTop; }
			set
			{
				ipercentageTop = value;
				FirstLayer.WidthRequest = ((value) * width / 100);
				lblPercentageTop.Text = value.ToString() + "%";
			}
		}
		public double PercentageBelow
		{
			get { return ipercentageBelow; }
			set
			{
				ipercentageBelow = value;
				if (ispercentage)
				{
					lblPercentageBelow.Text = value.ToString();
				}

			}
		}
		AbsoluteLayout Main;
		StackLayout FirstLayer = new StackLayout();
		StackLayout SecondLayer = new StackLayout();

		Label lblPercentageBelow = new Label()
		{
			HorizontalOptions = LayoutOptions.EndAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand
			                               ,TextColor=Color.White

		};
		Label lblPercentageTop = new Label()
		{
			HorizontalOptions = LayoutOptions.EndAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand,
			Text = "" ,TextColor=Color.White
		};
		public CustomBarGraph()
		{

			FirstLayer.HorizontalOptions = LayoutOptions.Fill;



			FirstLayer.Orientation = StackOrientation.Horizontal;
			FirstLayer.VerticalOptions = LayoutOptions.Fill;

			SecondLayer.HorizontalOptions = LayoutOptions.Fill;



			SecondLayer.Orientation = StackOrientation.Horizontal;
			SecondLayer.VerticalOptions = LayoutOptions.Fill;


			SecondLayer.Children.Add(lblPercentageBelow);
			FirstLayer.Children.Add(lblPercentageTop);


			Main = new AbsoluteLayout()
			{

				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = { SecondLayer, FirstLayer }
			};
			this.Children.Add(Main);
		}
	}
}

