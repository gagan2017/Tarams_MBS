﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CircleImage : Image, ICustomGesture
	{
		int click;
		public CircleImage()
		{

		}
		public event EventHandler LongPress;
		public event EventHandler ShowPres;
		public event EventHandler SingleTap;
		public event EventHandler Swype;
		public event EventHandler Touched;
		public void OnSingleTap()
		{
			SingleTap?.Invoke(this, new EventArgs());
		}
		async public void OnTouched()
		{
			Touched?.Invoke(this, new EventArgs());



		}

		public void OnSwype()
		{
			Swype?.Invoke(this, new EventArgs());
		}

		public void OnLongPress()
		{
			LongPress?.Invoke(this, new EventArgs());
		}

		public void OnShowPress()
		{
			ShowPres?.Invoke(this, new EventArgs());
		}
	}
}
