﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public sealed class CustomCheckbox : StackLayout
	{
		static List<CustomCheckbox> _listcheckbox;
		public static event EventHandler InvokeCheckBoxClicked;
		public static readonly BindableProperty TextProperty =
		BindableProperty.Create<CustomCheckbox, string>(w => w.Text, default(string));


		public static readonly BindableProperty TextProperty1 =
		BindableProperty.Create<CustomCheckbox, string>(w => w.Text1, default(string));
		public string Text1
		{
			get { return (string)GetValue(TextProperty1); }
			set
			{
				SetValue(TextProperty1, value);

			}
		}
		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set
			{
				SetValue(TextProperty, value);

			}
		}
		public static readonly BindableProperty IsTextVissibleProperty =
			BindableProperty.Create<CustomCheckbox, bool>(w => w.IsTextVissible, true);

		public bool IsTextVissible
		{
			get { return (bool)GetValue(IsTextVissibleProperty); }
			set
			{
				SetValue(IsTextVissibleProperty, value);

			}
		}
		public static readonly BindableProperty IsCheckedProperty =
			BindableProperty.Create<CustomCheckbox, bool>(w => w.IsChecked, default(bool));

		public bool IsChecked
		{
			get { return (bool)GetValue(IsCheckedProperty); }
			set
			{
				SetValue(IsCheckedProperty, value);

			}
		}
		CustomLabel _checkboxLabel = new CustomLabel()
		{
			HorizontalOptions = LayoutOptions.StartAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand
		};
		public TapGestureRecognizer v;
		Image _checkboximage = new Image()
		{
			WidthRequest = 30,
			HeightRequest = 30,
			Source = "unchecked.png",
			HorizontalOptions = LayoutOptions.StartAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand
		};





		protected override void OnPropertyChanged(string propertyName)
		{
			base.OnPropertyChanged(propertyName);

			if (propertyName == TextProperty.PropertyName)
			{
				_checkboxLabel.Text = Text;
			}
			if (propertyName == TextProperty1.PropertyName)
			{
				this.Text1 = Text1;
			}
			if (propertyName == IsTextVissibleProperty.PropertyName)
			{
				_checkboxLabel.IsVisible = IsTextVissible;
			}
			if (propertyName == IsCheckedProperty.PropertyName)
			{
				if (IsChecked)
				{
					_checkboximage.Source = "checked.png";

				}
				else
				{
					_checkboximage.Source = "unchecked.png";

				}
			}
		}
		public CustomCheckbox()
		{
			if (_listcheckbox == null)
			{
				_listcheckbox = new List<CustomCheckbox>();
			}
			_listcheckbox.Add(this);
			this.VerticalOptions = LayoutOptions.StartAndExpand;

			v = new TapGestureRecognizer();
			v.Tapped += V_Tapped;
			_checkboxLabel.Text = Text;

			var s = new StackLayout();

			s.Orientation = StackOrientation.Horizontal;
			s.VerticalOptions = LayoutOptions.StartAndExpand;
			s.HorizontalOptions = LayoutOptions.StartAndExpand;

			s.Children.Add(_checkboximage);
			s.Children.Add(_checkboxLabel);
			this.Children.Add(s);



			s.GestureRecognizers.Add(v);
		}

		public void V_Tapped(object sender, EventArgs e)
		{
			if (IsChecked)
			{
				InvokeCheckBoxClicked?.Invoke(Text, new EventArgs());
				_checkboximage.Source = "unchecked.png";
				IsChecked = false;
			}
			else
			{
				InvokeCheckBoxClicked?.Invoke(Text, new EventArgs());
				_checkboximage.Source = "checked.png";
				IsChecked = true;
			}
		}
		public void View_Tapped(object sender, EventArgs e)
		{
			if (IsChecked)
			{
				_checkboximage.Source = "unchecked.png";
				IsChecked = false;
			}
			else
			{
				_checkboximage.Source = "checked.png";
				IsChecked = true;
			}
		}
		public static List<string> getCheckedItems(List<string> str)
		{
			var returnList = new List<string>();
			foreach (var item in str)
			{
				var v = _listcheckbox.Find((obj) => (obj.Text == item && obj.IsChecked == true));
				if (v != null)
				{
					returnList.Add(v.Text);
				}

			}
			return returnList;
		}
	}
}

