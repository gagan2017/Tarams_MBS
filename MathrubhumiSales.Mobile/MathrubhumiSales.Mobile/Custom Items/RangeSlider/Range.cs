﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class Range : Slider
	{
		double range;
		public Range()
		{
		}
		public double RangeValue
		{
			get { return range; }
			set { range = value; }
		}
	}

}