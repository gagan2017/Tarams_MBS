using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CustomListView : ListView
	{
		int countMove = 0;
		public event EventHandler ItemSwypeLeft;
		public event EventHandler TaskItemTapped;
		public int id;
		public CustomListView()
		{
			ItemSwypeLeft += CustomListView_ItemSwypeLeft;
		}

		void CustomListView_ItemSwypeLeft(object sender, EventArgs e)
		{

		}

		public bool SwipeCompleted { get; set; }

		public void invokeListSwyped(object source)
		{
			var v = source as CustomListViewCellContents;
			if (v == null) return;
			var sen = v.FindByName<Label>("id");
			ItemSwypeLeft?.Invoke(sen, new EventArgs());
		}

		public void invokeListTapped(CustomListViewCellContents touchingView2)
		{

			if (touchingView2 == null) return;
			var sen = touchingView2.FindByName<Label>("id");
			TaskItemTapped?.Invoke(sen, new EventArgs());
		}
	}
}
