﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CustomRadioButton : StackLayout
	{
		static List<CustomRadioButton> listCustomRadiobutton = new List<CustomRadioButton>();
		static CustomRadioButton thisRadiobutton;
		CircleImage image = new CircleImage()
		{
			HeightRequest = 30,
			WidthRequest = 30,
			Aspect = Aspect.Fill,
			HorizontalOptions = LayoutOptions.CenterAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand,
			Source = "notselected.png"
		};


		public static readonly BindableProperty TextProperty =
		BindableProperty.Create<CustomRadioButton, string>(w => w.Text, default(string));

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set
			{
				SetValue(TextProperty, value);

			}
		}
		public static readonly BindableProperty IsSelectedProperty =
			BindableProperty.Create<CustomRadioButton, bool>(w => w.IsSelected, false);

		public bool IsSelected
		{
			get { return (bool)GetValue(IsSelectedProperty); }
			set
			{
				SetValue(IsSelectedProperty, value);

			}
		}
		public static readonly BindableProperty IsTextVissibleProperty =
			BindableProperty.Create<CustomRadioButton, bool>(w => w.IsTextVissible, true);

		public bool IsTextVissible
		{
			get { return (bool)GetValue(IsTextVissibleProperty); }
			set
			{
				SetValue(IsSelectedProperty, value);

			}
		}
		protected override void OnPropertyChanged(string propertyName)
		{
			base.OnPropertyChanged(propertyName);

			if (propertyName == TextProperty.PropertyName)
			{
				lblText.Text = Text;
			}
			if (propertyName == IsSelectedProperty.PropertyName)
			{
				
			}
			if (propertyName == IsTextVissibleProperty.PropertyName)
			{
				lblText.IsVisible = IsTextVissible;
				if (IsTextVissible)
				{
					Children.Remove(lblText);
				}
				else
				{
					if(!Children.Contains(lblText)) Children.Add(lblText);
				}
			}


		}



		CustomLabel lblText = new CustomLabel()
		{
			HorizontalOptions = LayoutOptions.EndAndExpand,
			VerticalOptions = LayoutOptions.CenterAndExpand
		};
		TapGestureRecognizer tapped;
		public CustomRadioButton()
		{
			tapped = new TapGestureRecognizer();
			tapped.Tapped += Tapped_Tapped;

			listCustomRadiobutton.Add(this);
			this.HorizontalOptions = LayoutOptions.EndAndExpand;
			this.VerticalOptions = LayoutOptions.CenterAndExpand;
			this.GestureRecognizers.Add(tapped);
			this.Children.Add(lblText);
			this.Children.Add(image);
		}

		public void Tapped_Tapped(object sender, EventArgs e)
		{

			var v = listCustomRadiobutton.Find((obj) => obj == thisRadiobutton);
			if (v != null)
			{
				v.image.Source = "notselected.png";
				v.IsSelected = false;
			}
			((CustomRadioButton)sender).image.Source = "selected.png";
			((CustomRadioButton)sender).IsSelected = true;
			thisRadiobutton = ((CustomRadioButton)sender);
		}
		public static CustomRadioButton getSelectedItem(List<string> listitems)
		{
			CustomRadioButton returnitem = new CustomRadioButton();
			foreach (var item in listitems)
			{
				var v = listCustomRadiobutton.Find((obj) => (obj.IsSelected == true && obj.Text == item));
				if (v != null)
				{
					returnitem = v;
				}
			}
			return returnitem;
		}
	}
}
