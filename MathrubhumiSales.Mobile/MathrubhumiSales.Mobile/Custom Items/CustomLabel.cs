﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{

	public class CustomLabel : Label, IObserver
	{
		public CustomLabel()
		{
			Sessions.instance.sliders.Add(this);
			SetLabelFontSize(PointSize);
		}

		double _pointSize = Sessions.instance.sliders.getValue() + 10;

		public readonly BindableProperty PointSizeProperty = BindableProperty.Create(
			"PointSize", typeof(double), typeof(CustomLabel), 8.0, propertyChanged: OnPointSizeChanged);

		public double PointSize
		{
			set { SetValue(PointSizeProperty, value); }
			get { return (double)GetValue(PointSizeProperty); }
		}
		static void OnPointSizeChanged(BindableObject bindable, object oldValue, object newValue)
		{

			((CustomLabel)bindable).OnPointSizeChanged((double)oldValue, (double)newValue);
		}

		public void Update()
		{
			SetLabelFontSize(PointSize);
			_pointSize = Sessions.instance.sliders.getValue() + 10;
		}

		void OnPointSizeChanged(double oldValue, double newValue)
		{
			SetLabelFontSize(newValue);
		}
		void SetLabelFontSize(double pointSize)
		{
			FontSize = pointSize + Sessions.instance.sliders.getValue() + 10;
		}
	}
}
