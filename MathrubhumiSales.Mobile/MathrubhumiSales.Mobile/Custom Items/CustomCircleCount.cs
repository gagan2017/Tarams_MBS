﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CustomCircleCount : Frame
	{
		Label label = new Label()
		{
			VerticalOptions = LayoutOptions.CenterAndExpand,
			HorizontalOptions = LayoutOptions.CenterAndExpand,


		};
		public static readonly BindableProperty CountOfRemainingUserProperty =
			BindableProperty.Create<CustomCircleCount, int>(w => w.CountOfRemainingUser, default(int));

		public int CountOfRemainingUser
		{
			get { return (int)GetValue(CountOfRemainingUserProperty); }
			set
			{
				SetValue(CountOfRemainingUserProperty, value);

			}
		}
		public static readonly BindableProperty CircleHeightProperty =
			BindableProperty.Create<CustomCircleCount, double>(w => w.CircleHeight, 30);

		public double CircleHeight
		{
			get { return (double)GetValue(CircleHeightProperty); }
			set
			{
				SetValue(CircleHeightProperty, value);

			}
		}
		protected override void OnPropertyChanged(string propertyName)
		{
			base.OnPropertyChanged(propertyName);

			if (propertyName == CountOfRemainingUserProperty.PropertyName)
			{
				if (CountOfRemainingUser == -1)
				{
					this.OutlineColor = this.BackgroundColor = Color.Transparent;
				}
				else
				{
					this.BackgroundColor = Color.FromHex("#E4E4E4");
					label.Text = "+" + CountOfRemainingUser;
				}
			}


		}
		public CustomCircleCount()
		{

			this.VerticalOptions = LayoutOptions.StartAndExpand;
			this.HorizontalOptions = LayoutOptions.StartAndExpand;

			this.OutlineColor = Color.Black;
			this.HasShadow = false;
			BackgroundColor = Color.FromHex("#E4E4E4");
			Content = label;
			HeightRequest = 20;
			WidthRequest = 20;
			CornerRadius = 30;
		}
	}
}
