﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Class;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class AddDesignationPage : ContentPage
	{
		MathrubhumiSales.Shared.Models.ProposalModel model;

		public AddDesignationPage()
		{
			InitializeComponent();



		}
		public void proposal(MathrubhumiSales.Shared.Models.ProposalModel model)
		{
			this.model = model;

			nameHead.Text = "Create Proposal";


		}
		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
				Name
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			string name = Name.Text;
			string status = "";
			indicator.IsVisible = true;
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			if (model == null)
			{
				status = "Designation";
				await new ApiAgencyDesignation().Post(name);
			}
			else
			{
				model.Name = name;
				status = "Proposal";

				await new ApiProposal().Post(model);
			}
			DependencyService.Get<ICustomPrgressBar>().Stop();
			indicator.IsVisible = false;
			await DisplayAlert("Status", status + " added", "ok");
			Navigation.PopAsync();
		}

		void CreateButton_Clicked(object sender, EventArgs e)
		{
		}
}
}
