﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class PreLoadingPages
	{
		public string Key_MainPage = "MainPage";
		public string Key_ProfilePage = "ProfilePage";
		public string Key_ClientListingPage = "Clients";
		public string Key_Project = "Projects";
		public event EventHandler Loaded;
		static PreLoadingPages _instance { get; set; }
		public static PreLoadingPages Instance { get { if (_instance == null) _instance = new PreLoadingPages(); return _instance;} }
		Dictionary<string, object> pages;
		private PreLoadingPages()
		{
			pages = new Dictionary<string, object>();
		}
		public void Load()
		{
			pages.Add(Key_MainPage, new MainPage());
			pages.Add(Key_ProfilePage, new ProfilePage());
			pages.Add(Key_ClientListingPage, new ClientListing());
			pages.Add(Key_Project, new ProjectPage());
			Loaded.Invoke(this, new EventArgs());
		}

		internal object getPage(string val)
		{
			return pages[val];
		}
	}
}
