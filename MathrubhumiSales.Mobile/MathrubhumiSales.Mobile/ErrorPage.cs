﻿using System;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ErrorPage : ContentPage
	{
		public ErrorPage()
		{
			Content = new ErrorContent();
		}
	}
	public class ErrorContent : ContentView
	{
		public ErrorContent()
		{
			Content = new StackLayout
			{
				Children = {
					new Label
					{
						Text = "Under Progress" ,
						HorizontalOptions=LayoutOptions.Center,
						VerticalOptions=LayoutOptions.CenterAndExpand
					}
				}
			};
		}
	}
}


