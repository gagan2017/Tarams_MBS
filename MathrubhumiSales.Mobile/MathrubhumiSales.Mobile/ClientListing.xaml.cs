﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientListing : ContentPage
	{
		List<Frame> frameList;
		ContentView temp;
		AddStyles styles;
		ClientLisitingViewModel ClientLisitingViewModel;
		protected override void OnAppearing()
		{
			base.OnAppearing();
			ClientLisitingViewModel.Refresh();
		}
		public ClientListing()
		{
			ClientLisitingViewModel = new ClientLisitingViewModel();
			BindingContext = ClientLisitingViewModel;
			InitializeComponent();
			listview.IsPullToRefreshEnabled = true;
			listview.Refreshing += Listview_Refreshing;
			styles = new AddStyles();
			ActiveContent(false);
			styles.setLabelClckedStyle(one);
			listview.ItemTemplate = new ClientListingDataTemplate();
		}

		async void Listview_Refreshing(object sender, EventArgs e)
		{
			await ClientLisitingViewModel.getData();
			listview.IsRefreshing = false;
		}

		void Filter_Activated(object sender, System.EventArgs e)
		{
			//Navigation.PushAsync(FactoryPages._instance.GetPage("ClientListingFilter"));
		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			styles.setLabelClckedStyle((Label)sender);
			if (((Label)sender).Text == "All")
			{
				listview.ItemsSource = ClientLisitingViewModel.listitemsAll;
				ActiveContent(true);

			}
			else
			{
				listview.ItemsSource = ClientLisitingViewModel.listitemsRecent;
				ActiveContent(false);
			}

		}

		void ActiveContent(bool v)
		{
			a2.IsVisible = a3.IsVisible = a4.IsVisible = a5.IsVisible = a1.IsVisible = v;
		}

		void Handle_Image_Tapped(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new CreateClient(), true);
		}
		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as ClientListingModel;
			if (e == null) return;
			Navigation.PushAsync(new ClientPage(null, v));
			((ListView)sender).SelectedItem = null; // de-select t
		}
	}
}
