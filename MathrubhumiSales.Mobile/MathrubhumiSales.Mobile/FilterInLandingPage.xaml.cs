﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class FilterInLandingPage : ContentPage
	{
		TapGestureRecognizer CreateLabel;
		FilterLandingPageViewModel CheckBoxLabelBinding;
		public FilterInLandingPage()
		{
			CheckBoxLabelBinding = new FilterLandingPageViewModel();
			BindingContext = CheckBoxLabelBinding;
			InitializeComponent();
			CreateLabel2.IsVisible = false;
			CreateLabel = new TapGestureRecognizer();
			CreateLabel.Tapped += CreateLabel_Tapped;
			CreateLabel1.GestureRecognizers.Add(CreateLabel);


		}

		private void CreateLabel_Tapped(object sender, EventArgs e)
		{
			CreateLabel1.IsVisible = false;
			CreateLabel2.IsVisible = true;
		}

		private void Reset_All_Activated(object sender, EventArgs e)
		{

		}

		private void FilterApply_Clicked(object sender, EventArgs e)
		{
			List<string> checkboxitems = new List<string>();
			foreach (var item in CheckBoxLabelBinding.listitems)
			{
				checkboxitems.Add(item.Listname);
			}

			checkboxitems.Add("Todo Cards");
			checkboxitems.Add("Client Cards");
			checkboxitems.Add("High Priority Cards");

			var checkeditems = CustomCheckbox.getCheckedItems(checkboxitems);
		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{

			var v = e.SelectedItem;
			if (e == null) return; // has been set to null, do not 'process' tapped event



			((ListView)sender).SelectedItem = null; // de-select the r
		}
	}
}
