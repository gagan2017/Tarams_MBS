﻿using System;
using MathrubhumiSales.Mobile.Class;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AddProposalPage : ContentPage
	{
		Entry summary, amount;
		System.Collections.Generic.List<ProposalModel> models;
		public AddProposalPage(System.Collections.Generic.List<ProposalModel> model = null)
		{
			this.models = model;
			var stack = new StackLayout();
			var Label = new CustomLabel()
			{
				Text = "Summary"
			};
			var Label2 = new CustomLabel()
			{
				Text = "Amount"
			};
			summary = new Entry()
			{
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Fill

			};
			amount = new Entry()
			{
				HorizontalOptions = LayoutOptions.Fill,
				Keyboard = Keyboard.Numeric
			};
			var button = new Button()
			{
				HorizontalOptions = LayoutOptions.Fill,
				BackgroundColor = Color.FromHex("#006A90"),
				TextColor = Color.White,
				Text = "Create Proposal",
				VerticalOptions = LayoutOptions.End
			};
			button.Clicked += Button_Clicked;
			stack.Children.Add(Label);
			stack.Children.Add(summary);
			stack.Children.Add(Label2);
			stack.Children.Add(amount);
			stack.Children.Add(button);
			stack.Padding = 6;

			Content = stack;

		}

		async void Button_Clicked(object sender, EventArgs e)
		{
			if (ValidationClass._instance.Validate(new System.Collections.Generic.List<Entry>()
			{
				summary,amount
			}))
			{
				DisplayAlert("Error", "Fill all Fields", "OK!");
				return;
			}
			ProposalModel model = new ProposalModel();
			model.Summary = summary.Text;
			model.Amount = int.Parse(amount.Text);
			model.DateAndTime = DateTime.Now;
			model.DealStatusDate = DateTime.Now;
			model.DealStatus = Shared.Entities.Enums.DealStatus.Running;
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			if (models != null)
			{
				models.Add(model);
			}
			else
			{
				
				await new ApiProposal().Post(model);
			}
			DependencyService.Get<ICustomPrgressBar>().Stop();




			DisplayAlert("Success", "Proposal added", "OK!");
			Navigation.PopAsync();

		}
	}
}

