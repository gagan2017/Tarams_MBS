﻿using MathrubhumiSales.Shared.Entities.Enums;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Mobile;

namespace MathrubhumiSales.Shared.Models
{
	public class ClientModel : BaseModel,IBaseInterface
	{
		public ClientCategory Category { get; set; }
		public Verticals Verticals { get; set; }
		public List<BrandModel> Brands { get; set; }
		public List<ProductModel> Products { get; set; }
		public List<AgencyModel> Agencies { get; set; }
		public List<ClientContactModel> ClientContacts { get; set; }
		public string Address { get; set; }


	}
}
