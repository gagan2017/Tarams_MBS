﻿namespace MathrubhumiSales.Shared.Models
{
    public class AuthModel
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public string GmailId { get; set; }
    }
}
