﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Models;

namespace MathrubhumiSales.Shared.Models
{
	public class TaskModel : BaseModel,IBaseInterface
	{
		public string TaskSummary { get; set; }
		public DateTime TaskDate { get; set; }
		public string Comment { get; set; }
		public LabelModel Labels { get; set; }
		public DateTime CreatedTime { get; set; }
		public List<GroupModel> Teams { get; set; }
		public List<UserModel> Users { get; set; }
		public UserModel User { get; set; }
		public bool IsHighPriority { get; set; }
		public int NoOfComments { get; set; }
		public TaskModel()
		{

		}
		public TaskModel(string mess1, string mess2)
		{
			
			TaskSummary = mess1;
			TaskDate = DateTime.Now;
			Comment = mess1;
			Labels = new LabelModel()
			{
				Name = "Add Label"
			};
			Teams = new List<GroupModel>();
			Users = new List<UserModel>()
			{
				new UserModel(){
					FirstName="Eror"
				}
			};

		}

	}
}
