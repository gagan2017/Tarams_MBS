﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Models;

namespace MathrubhumiSales.Shared.Models
{
	public class ProjectModel : BaseModel,IBaseInterface
	{

		public string ProjectName { get; set; }
		public DateTime TimeAndDate { get; set; }
		public List<UserModel> Users { get; set; }
		public bool Completed { get; set; }
		public ProjectModel()
		{

		}
		public ProjectModel(int i)
		{
			ProjectName = "no Project yet";
			TimeAndDate = DateTime.Now;
			Users = new List<UserModel>();
			Completed = true;
		}

	}
}
