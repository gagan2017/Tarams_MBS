﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Shared.Entities.Enums;

namespace MathrubhumiSales.Shared.Models
{
	public class ProposalModel : BaseModel,IBaseInterface
	{
		public int Amount { get; set; }
		public DateTime DateAndTime { get; set; }
		public DateTime DealStatusDate { get; set; }
		public string Summary { get; set; }
		public DealStatus DealStatus { get; set; }
		public ProposalModel()
		{
			DealStatusDate=DateAndTime = DateTime.Now;
		}
		public ProposalModel(string s)
		{
			Summary = s;
		}
	}
}
