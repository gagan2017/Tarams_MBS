﻿using MathrubhumiSales.Mobile;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Api.Models
{
	public class IdModel
	{
		public int Id { get; set; }
		public AuthModel AuthModel
		{
			get; set;
		}
		public IdModel()
		{
			if (Sessions.instance.CurrentUser == null)
			{
				AuthModel = null;
				return;
			}
			AuthModel = new AuthModel()
			{
				Email = Sessions.instance.CurrentUser.AuthModel.Email,
				Token = Sessions.instance.CurrentUser.AuthModel.Token,
				GmailId = Sessions.instance.CurrentUser.AuthModel.GmailId
			};
		}
	}
}