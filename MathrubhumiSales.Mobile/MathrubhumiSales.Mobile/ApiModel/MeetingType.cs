﻿using System;
namespace MathrubhumiSales.Mobile
{
	public enum MeetingType
	{
		TelephonicMeeting,
		EmailMeeting,
		GroupMeeting
	}
}
