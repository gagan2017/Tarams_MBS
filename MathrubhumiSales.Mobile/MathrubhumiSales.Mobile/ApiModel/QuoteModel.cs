﻿using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Models;

namespace MathrubhumiSales.Shared.Models
{
   public class QuoteModel : BaseModel,IBaseInterface

    {
        public string Quote { get; set; }
        public string By { get; set; }
    }
}
