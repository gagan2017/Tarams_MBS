﻿using MathrubhumiSales.Mobile.Models;

namespace MathrubhumiSales.Shared.Models
{
	public class UserModel 
	{
		public AuthModel AuthModel { get; set; }
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ContactNumber { get; set; }
		public string ProfilePicUrl { get; set; }
		public DesignationModel Designation {get;set;}
		public bool IsDeleted { get; set; }

	}
	public class UserMethod
	{
		public UserModel setuser(UserModel umodel)
		{
			return new UserModel()
			{
				Id = umodel.Id,
				FirstName = umodel.FirstName,
				LastName = umodel.LastName,
				ContactNumber = umodel.ContactNumber,
				ProfilePicUrl = umodel.ProfilePicUrl,
				Designation = umodel.Designation,
				IsDeleted = umodel.IsDeleted,
				AuthModel=new AuthModel()
				{ 
					Email= umodel.AuthModel.Email,
					GmailId = umodel.AuthModel.GmailId,
					Token = umodel.AuthModel.Token
				}

			};
		}

	}
}
