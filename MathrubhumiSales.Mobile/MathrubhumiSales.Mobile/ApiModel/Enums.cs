﻿using System.Collections.Generic;

namespace MathrubhumiSales.Shared.Entities.Enums
{
	public enum Verticals
	{
		Television,
		FMCG,
		Automobiles,
		Construction

	}
	public enum ClientCategory
	{
		ConstructionCategory,
		HealthcareCategory,
		FMCGCategory
	}
	public enum DealStatus
	{
		Running,
		Closed,
		Cancelled
	}
	public class ClientCategoryConverter
	{

		public ClientCategoryConverter()
		{

		}
		public ClientCategory GetEnum(string s)
		{
			if (s == "Construction") return ClientCategory.ConstructionCategory;
			if (s == "Health care") return ClientCategory.HealthcareCategory;
			return ClientCategory.FMCGCategory;

		}
		public string Getstring(ClientCategory s)
		{
			if (s == ClientCategory.ConstructionCategory) return "Construction";
			if (s == ClientCategory.HealthcareCategory) return "Health";
			return "FMCG";
		}


	}
}
