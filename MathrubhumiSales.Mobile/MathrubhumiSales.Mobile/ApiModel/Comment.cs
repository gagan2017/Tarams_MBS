﻿using System;
using MathrubhumiSales.Mobile.BaseModelView;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class TaskComment : MathrubhumiSales.Mobile.Models.BaseModel
	{
		public DateTime CommentedTime { get; set; }
		public string Comment { get; set; }
		public UserModel User { get; set; }
		public TaskModel Task { get; set; }
	}
}
