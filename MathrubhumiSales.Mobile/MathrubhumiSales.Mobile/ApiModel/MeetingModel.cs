﻿using MathrubhumiSales.Shared.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Mobile;

namespace MathrubhumiSales.Shared.Models
{
	public class MeetingModel : BaseModel, IBaseInterface
	{
		public UserModel User { get; set; }
		public ClientModel Client { get; set; }
		public ProposalModel Proposal { get; set; }
		public DateTime MeetingTime { get; set; }
		public string Comment { get; set; }
		public MeetingType MeetingType { get; set; }
		public LabelModel Label { get; set; }
		public bool IsHighPriority { get; set; }
		public List<AgencyModel> Agencies { get; set; }
		public List<UserModel> Users { get; set; }
		public List<ProjectModel> Projects { get; set; }
		public DealStatus DealStatus { get; set; }
		public MeetingModel()
		{
			User = new UserModel(){
				AuthModel =new AuthModel(){
					Email=Sessions.instance.CurrentUser.AuthModel.Email,
					GmailId=Sessions.instance.CurrentUser.AuthModel.GmailId,
					Token=Sessions.instance.CurrentUser.AuthModel.Token,
				},
				ContactNumber=Sessions.instance.CurrentUser.ContactNumber,
				Designation=new DesignationModel()
				{
					Id=Sessions.instance.CurrentUser.Designation.Id,
					Name=Sessions.instance.CurrentUser.Designation.Name
				},
				Id=Sessions.instance.CurrentUser.Id,
				FirstName=Sessions.instance.CurrentUser.FirstName,
				LastName=Sessions.instance.CurrentUser.LastName,
				IsDeleted=Sessions.instance.CurrentUser.IsDeleted,
				ProfilePicUrl=Sessions.instance.CurrentUser.ProfilePicUrl

			};
		}
		public MeetingModel(string id)
		{
			this.Client = new ClientModel();
			Proposal = new ProposalModel("No Proposal");

			Comment = id;
			MeetingType = MeetingType.EmailMeeting;
			Label = new LabelModel() { Name = "Add Label" };
			Agencies = new List<AgencyModel>();
			Agencies.Add(new AgencyModel());
			Users = new List<UserModel>();
			Users.Add(new UserModel());
			Projects = new List<ProjectModel>();
			Projects.Add(new ProjectModel());
		}
	}
}
