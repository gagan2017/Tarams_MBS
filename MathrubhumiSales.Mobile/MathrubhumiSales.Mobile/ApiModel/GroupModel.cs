﻿using System.Collections.Generic;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Models;

namespace MathrubhumiSales.Shared.Models
{
    public class GroupModel : BaseModel,IBaseInterface
    {
        public List<UserModel> Members { get; set; }
        public AuthModel AuthModel { get; set; }
    }

  
}
