﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Api.Models
{
	public class SearchModel
	{
		public string SearchText { get; set; }
		public AuthModel AuthModel { get; set; }
		public  SearchModel()
		{
			
		}
		public SearchModel(string s)
		{
			SearchText = s;
		}
	}
}
