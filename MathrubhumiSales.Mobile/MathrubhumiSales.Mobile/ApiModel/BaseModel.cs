﻿
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile.Models
{
	public class BaseModel 
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public AuthModel AuthModel { get; set; }
	}
}
