﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile.Models
{
	public class AgencyModel : BaseModel,IBaseInterface
	{
		public string Branch { get; set; }
		public List<AgencyContactModel> AgencyContacts { get; set; }
	}


	public class AgencyContactModel : BaseModel,IBaseInterface
	{
		public int AgencyId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int DesignationId { get; set; }
		public string Designation { get; set; }
		public string Email { get; set; }
		public string ContactNumber { get; set; }
	}


}
