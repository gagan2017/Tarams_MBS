﻿namespace MathrubhumiSales.Shared.Models
{
    public class BaseContactModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int DesignationId { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public AuthModel AuthModel { get; set; }
    }
}
