﻿using System.IO;
using System.Text;
using Xamarin.Forms;

namespace MathrubhumiSales.Shared.Models
{
	public class ClientContactModel : BaseContactModel
	{
		public int ClientId { get; set; }
		public byte[] ProfilePic { get; set; }
		public string ProfilePicpath { get; set; }


	}
}
