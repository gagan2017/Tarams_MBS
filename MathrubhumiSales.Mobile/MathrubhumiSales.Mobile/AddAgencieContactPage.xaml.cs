﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Class;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;
using XamFormsImageResize;

namespace MathrubhumiSales.Mobile
{
	public partial class AddAgencieContactPage : ContentPage
	{
		List<DesignationModel> list { get; set; }
		AgencyModel model;
		async protected override void OnAppearing()
		{

			base.OnAppearing();

			list = await new ApiAgencyDesignation().GetAll();
			picker.Items.Clear();
			foreach (var item in list)
			{
				if (item.Name != null)
				{
					picker.Items.Add(item.Id + " " + item.Name);

				}
			}

			picker.Items.Add("+Add Designation");
		}

		public AddAgencieContactPage(AgencyModel model = null)
		{
			this.model = model;
			InitializeComponent();



		}

		void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			var sen = sender as Picker;
			if (picker.SelectedItem == "+Add Designation")
			{
				Navigation.PushAsync(new AddDesignationPage());
				picker.SelectedIndex = -1;
			}
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
				enFirstName,enLastName,enContactNumber,enEmail
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			if (ValidationClass._instance.EmailValidation(enEmail.Text))
			{
				await DisplayAlert("Error", "Please give valid email", "Ok!");
				return;
			}
			if (ValidationClass._instance.PhnoValidation(enContactNumber.Text))
			{
				await DisplayAlert("Error", "Please give valid Phone number", "ok!");
				return;
			}

			string v = (string)picker.SelectedItem;
			if (v == null)
			{
				await DisplayAlert("Error", "Please Select Designation", "ok!");
				return;
			}
			var arr = v.Split(' ');
			var selecteditem = list.Find((obj) => obj.Id == int.Parse(arr[0].ToString()));
			indicator.IsVisible = true;
			var parameter = new AgencyContactModel()
			{
				Name = enFirstName.Text,
				FirstName = enFirstName.Text,
				LastName = enLastName.Text,
				ContactNumber = enContactNumber.Text,
				Email = enEmail.Text,
				DesignationId = selecteditem.Id,
				Designation = selecteditem.Name
			};

			if (model.AgencyContacts == null)
			{
				model.AgencyContacts = new List<AgencyContactModel>();
			}

			model.AgencyContacts.Add(parameter);
			if (model.Id == 0)
			{
				await DisplayAlert("Completed", "Agency Contact added", "Ok");
				indicator.IsVisible = false;
				Navigation.PopAsync();
				return;

			}
			parameter.AgencyId = model.Id;

			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await new ApiAgencyContact().Post(parameter);
			DependencyService.Get<ICustomPrgressBar>().Stop();

			await DisplayAlert("Completed", "Agency Contact added", "Ok");
			indicator.IsVisible = false;
			Navigation.PopAsync();
		}



	}
}
