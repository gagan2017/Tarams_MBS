﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ManageUsersPage : ContentPage
	{
		protected override void OnAppearing()
		{
			base.OnAppearing();
			bc.start();
		}
		ManageUserViewModel bc;
		public ManageUsersPage()
		{
			bc = new ManageUserViewModel();
			BindingContext = bc;
			InitializeComponent();
			bc.start();
			listItems.IsPullToRefreshEnabled = true;
			listItems.ItemsSource = bc.listuser;
			listItems.Refreshing += ListItems_Refreshing;
			addbtn.GestureRecognizers.Add(new TapGestureRecognizer((obj) => Navigation.PushAsync(new UserManagementCreateTeam(new CreateGroupViewModel()))));
		}
		void ListItems_Refreshing(object sender, EventArgs e)
		{
			bc.start();
			this.listItems.IsRefreshing = false;
		}
		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as ListItems;
			if (e == null) return; // has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null; // de-select the ro
		}
		 
	}
}
