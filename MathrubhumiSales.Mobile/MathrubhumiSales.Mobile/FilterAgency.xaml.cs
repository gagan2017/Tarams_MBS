﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class FilterAgency : ContentPage
	{
		public FilterAgency()
		{
			InitializeComponent();
		}
		private void Button_Clicked(object sender, EventArgs e)
		{

		}

		private void FilterApply_Clicked(object sender, EventArgs e)
		{

		}

		void Value_Changed(object sender, System.EventArgs e)
		{
			var slider = sender as RangeSlider;
			if (slider.LowerValue == slider.UpperValue)
			{
				slider.LowerValue--;
				return;
			}
			slider.LowerValue = (int)slider.LowerValue;
			slider.UpperValue = (int)slider.UpperValue;

			lblFrom.Text = ((int)slider.LowerValue).ToString();
			lblTo.Text = ((int)slider.UpperValue).ToString();
		}
	}
}
