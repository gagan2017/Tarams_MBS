﻿using System;
using CrossPieCharts.FormsPlugin.Abstractions;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class App : Application
	{
		public App(bool IsConnected)
		{
			InitializeComponent();


			/*if (DependencyService.Get<IConnectionCheck>().IsConnected())
			{
				
			}*/
			/*if (!IsConnected)
			{
				MainPage = new NavigationPage(new ErrorPage());
				return;
			}*/
			if (Sessions.instance.CurrentUser == null)
			{
				MainPage = new NavigationPage(FactoryPages._instance.GetPage("GoogleSignIn"));

			}
			else
			{
				if (GetPage())
				{
					MainPage = new NavigationPage(FactoryPages._instance.GetPage("HomePage"));
				}
				else
				{
					MainPage = new NavigationPage(FactoryPages._instance.GetCaroselPage("MainPage"));
				}
			}
			(MainPage as NavigationPage).BarTextColor = Color.Gray;
			(MainPage as NavigationPage).BarBackgroundColor = Color.Transparent;

		}


		bool GetPage()
		{
			if (Sessions.instance.GetDisplay == DateTime.Now.DayOfYear)
			{
				return true;
			}
			return false;
		}
		protected override void OnStart()
		{
		}

		protected override void OnSleep()
		{

		}

		protected override void OnResume()
		{

		}
		bool getPage()
		{
			if (Sessions.instance.GetDisplay == DateTime.Now.DayOfYear)
			{
				return true;
			}
			return false;
			if (Sessions.instance.GetDisplay == DateTime.Now.DayOfYear)
			{
				if (DateTime.Now.DayOfYear == 365 || DateTime.Now.DayOfYear == 366)
				{
					Sessions.instance.GetDisplay = 1;
				}
				else
				{
					Sessions.instance.GetDisplay = DateTime.Now.DayOfYear + 1;
				}
				return true;
			}
			return false;
			//return new MainPage();

		}
	}
}
