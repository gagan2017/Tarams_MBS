﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ProposalPageListView : ViewCell
	{
		public ProposalPageListView()
		{
			InitializeComponent();

		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			ClientPageProposals.optionsTapped(int.Parse(ProposalId.Text), int.Parse(MeetingId.Text));
		}
	}
}
