﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ClientListingDataTemplate : Xamarin.Forms.DataTemplateSelector
	{
		private readonly DataTemplate listDataTemplate;
		public ClientListingDataTemplate()
		{
			listDataTemplate = new DataTemplate(typeof(ClientListingViewCell));
		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			return listDataTemplate;
		}
	}
}
