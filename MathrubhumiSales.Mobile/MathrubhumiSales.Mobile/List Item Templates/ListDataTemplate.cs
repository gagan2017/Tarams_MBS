﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ListDataTemplate : Xamarin.Forms.DataTemplateSelector
	{
		private readonly DataTemplate redDataTemplate;
		private readonly DataTemplate blueDataTemplate;
		private readonly DataTemplate headingDataTemplate;
		public ListDataTemplate()
		{
			this.redDataTemplate = new DataTemplate(typeof(ListItemTemplateRed));
			this.blueDataTemplate = new DataTemplate(typeof(ListItemsTemplateBlue));
			this.headingDataTemplate = new DataTemplate(typeof(ListItemHeadingTemplate));

		}

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			var messageVm = item as ListItemModel;
			if (messageVm == null)
				return null;
			if (messageVm._redItem != null) return this.redDataTemplate;
			if (messageVm._blueItem != null) return this.blueDataTemplate;
			return this.headingDataTemplate;

		}
	}
}
