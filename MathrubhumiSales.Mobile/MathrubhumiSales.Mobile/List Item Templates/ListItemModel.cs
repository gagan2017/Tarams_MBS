﻿using System;
using MathrubhumiSales.Mobile.Model;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ListItemModel
	{
		public int Priority { get; set; }
		public bool IsHeading { get; set; }
		public string Heading { get; set; }
		public bool IsRed { get; set; }
		public Command SwypeLeftCommand;
		public DateTime Time
		{
			get
			{
				if (_redItem != null) return _redItem.UpdateTime;
				return _blueItem.UpdateTime;

			}
		}
		public CommentsRedItem _redItem { get; set; }
		public CommentsBlueItem _blueItem { get; set; }
		public ListItemModel()
		{
		}
	}
}
