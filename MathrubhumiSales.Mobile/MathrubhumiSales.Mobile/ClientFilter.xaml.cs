﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientFilter : ContentPage
	{
		ClientListingFilterViewModel bc;
		public ClientFilter()
		{
			bc = new ClientListingFilterViewModel();
			BindingContext = bc;

			InitializeComponent();
		}

		void Handle_Activated(object sender, System.EventArgs e)
		{
			InitializeComponent();
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			bc.Refresh(CustomCheckbox.getCheckedItems(bc.checkboxlist));
			var v = bc;
		}

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
		}
	}
}
