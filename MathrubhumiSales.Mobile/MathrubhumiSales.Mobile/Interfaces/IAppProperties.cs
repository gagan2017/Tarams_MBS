﻿using System;
namespace MathrubhumiSales.Mobile
{
	public interface IAppProperties
	{
		void set(string pData, string pKey);
		string get(string pKey);
		void setDouble(double pData, string pKey);
		double getDouble(string pKey);
		void setInt(int pData, string pKey);
		int getInt(string pKey);
	}
}
