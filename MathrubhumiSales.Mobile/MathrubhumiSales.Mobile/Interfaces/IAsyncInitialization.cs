﻿using System;
using System.Threading.Tasks;

namespace MathrubhumiSales.Interfaces
{
	public interface IAsyncInitialization
	{
		Task Initialization { get; }
	}
}
