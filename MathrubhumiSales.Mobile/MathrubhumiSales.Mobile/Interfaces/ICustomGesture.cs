﻿using System;
namespace MathrubhumiSales.Mobile
{
	public interface ICustomGesture
	{
		event EventHandler SingleTap;
		event EventHandler Touched;
		event EventHandler Swype;
		event EventHandler LongPress;
		event EventHandler ShowPres;
		void OnSingleTap();
		void OnTouched();
		void OnSwype();
		void OnLongPress();
		void OnShowPress();
	}
	abstract public class CustomGestures : ICustomGesture
	{
		public event EventHandler LongPress;
		public event EventHandler ShowPres;
		public event EventHandler SingleTap;
		public event EventHandler Swype;
		public event EventHandler Touched;

		public void OnSingleTap()
		{
			SingleTap?.Invoke(this, new EventArgs());
		}
		public void OnTouched()
		{
			Touched?.Invoke(this, new EventArgs());
		}

		public void OnSwype()
		{
			Swype?.Invoke(this, new EventArgs());
		}

		public void OnLongPress()
		{
			LongPress?.Invoke(this, new EventArgs());
		}

		public void OnShowPress()
		{
			ShowPres?.Invoke(this, new EventArgs());
		}
	}
}
