﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Models;
using Newtonsoft.Json;
/*Api Designation : Get
http://10.191.3.111/MathrubhumiApi/api/AgencyDesignation?idModel={"Id":-1,"AuthModel":{"Token":null,"Email":"gagan.s.paril@gmail.com","GmailId":null}} Gott output


Api Designation : Post 
http://10.191.3.111/MathrubhumiApi/api/AgencyDesignation?designationModel={"Id":0,"Name":"Abcde","AuthModel":{"Token":null,"Email":"gagan.s.paril@gmail.com","GmailId":null}}
*/
namespace MathrubhumiSales.Mobile.Api
{
	public class ApiAgencyDesignation : ApiBase
	{
		public ApiAgencyDesignation()
		{

		}
		public async Task<List<DesignationModel>> GetAll()
		{
			Url = Head + "api/AgencyDesignation?idModel=" + JsonConvert.SerializeObject(idModel);

			var response =await base.getAllAsync();
			var ret = JsonConvert.DeserializeObject<List<DesignationModel>>(response);
			return ret;
		
		

		}
	public async Task<string> Post(string name)
	{
		var item = new DesignationModel();
			item.Name = name;
			item.AuthModel = idModel.AuthModel;
			this.model = item;
			Url = Head + "api/AgencyDesignation"                                              ;
			var response =await  base.Post();

			return null;
	}


}
}
