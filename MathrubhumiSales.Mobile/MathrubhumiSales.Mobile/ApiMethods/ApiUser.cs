﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile.Api
{

	public class ApiUser : ApiBase
	{
		
		public ApiUser()
		{
			
		}
		public async Task<List<UserModel>> GetAll(SearchModel searchmodel)
		{
			using (var client = new HttpClient())
			{
				searchmodel.AuthModel = Sessions.instance.CurrentUser.AuthModel;
				Url = Head + "api/User?searchModel=" + JsonConvert.SerializeObject(searchmodel);
				string response = "";
				try
				{
					response = await client.GetStringAsync(Url);
					return JsonConvert.DeserializeObject<List<UserModel>>(response.ToString());
				}
				catch
				{
					return new List<UserModel>();
				}

			}

		}
		public async Task<UserModel> Post(AuthModel param)
		{
			Url = Head + "api/Authentication";
			using (var client = new HttpClient())
			{
				//param.Id = 0;
				var str = JsonConvert.SerializeObject(param);
				var content = new StringContent(str, Encoding.UTF8, "application/json");
				try
				{
					var response = await client.PostAsync(Url, content);
					if (response.Content == null) return null;
					return JsonConvert.DeserializeObject<UserModel>(response.Content.ToString());
				}
				catch (Exception E)
				{
					
				}


			}
			return null;
		}
	}
}
