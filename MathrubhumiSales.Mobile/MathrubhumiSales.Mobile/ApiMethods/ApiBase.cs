﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Mobile.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile.Api
{

	public class ApiBase
	{
		public string Head;
		public string Url = "-1";
		public string idString;
		public IdModel idModel;
		public BaseModel model;
		public ApiBase()
		{
			Head = "http://10.191.2.233/MathrubhumiApi/";
			idModel = new IdModel();
			idModel.Id = -1;
			idString = Newtonsoft.Json.JsonConvert.SerializeObject(idModel);

		}

		public virtual async Task<string> getAllAsync()
		{
			if (Url == "-1") return null;
			using (var client = new HttpClient())
			{
				try
				{
					var response = await client.GetStringAsync(Url);
					return response.ToString();
				}
				catch (Exception e)
					{
					return "";
				}

			}
		}
		public virtual async Task<HttpResponseMessage> Post()
		{
			if (Url == "-1") return null;
			if (model == null) return null;

			using (var client = new HttpClient())
			{
				var str = JsonConvert.SerializeObject(model);
				var content = new StringContent(str, Encoding.UTF8, "application/json");
				try
				{
					var response = await client.PostAsync(Url, content);
					return response;
				}
				catch (Exception E)
				{

				}


			}
			return null;
		}

	}
}
