﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiComents : ApiBase
	{
		public ApiComents()
		{
		}
		async public Task<List<TaskComment>> getAll(TaskModel model)
		{
			idModel.Id = model.Id;
			Url = Head + "api/Comment?idModel=" + JsonConvert.SerializeObject(idModel);
			var res = await base.getAllAsync();
			if (res == "")
			{
				return new List<TaskComment>()
				{
					new TaskComment()
					{
						Comment="error check with internet",
						CommentedTime=DateTime.Now,
						User=null
					}
				};
			}
			return JsonConvert.DeserializeObject <List<TaskComment>>(res.ToString());
		}
		async public Task<bool> Post(TaskComment m)
		{
			Url = Head + "api/Comment";
			model = m;
			var res = await base.Post();
			return res.IsSuccessStatusCode;
		}
	}
}
