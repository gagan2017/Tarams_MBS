﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiProposal : ApiBase
	{

		public async Task<List<ProposalModel>> GetAll(int id=0)
		{
			using (var client = new HttpClient())
			{
				idModel.Id = id;
				Url = Head + "api/Proposal?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				var response = await base.getAllAsync();
				return JsonConvert.DeserializeObject<List<ProposalModel>>(response.ToString());
			}
		}

		async internal Task<HttpResponseMessage> Post(ProposalModel projectModle)
		{
			Url = Head + "api/Proposal";
			this.model = projectModle;
			return await base.Post();
		}
	}

}
