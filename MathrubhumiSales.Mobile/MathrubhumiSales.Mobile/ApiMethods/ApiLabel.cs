﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiLabel : ApiBase
	{
		public ApiLabel()
		{
		}
		public async Task<List<LabelModel>> GetAll()
		{
			using (var client = new HttpClient())
			{
				Url = Head + "api/Label?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				var response = await base.getAllAsync();
				return JsonConvert.DeserializeObject<List<LabelModel>>(response.ToString());

			}
		}
	}
}
