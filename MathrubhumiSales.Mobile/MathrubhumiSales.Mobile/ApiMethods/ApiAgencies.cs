﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Api.Models;

/*
  Agency Post: "http://10.191.3.111/MathrubhumiApi/api/Agency?agencyModel={"Branch":null,"AgencyContacts":null,"Id":0,"Name":null,"AuthModel":null}"

Agency get:http://10.191.3.111/MathrubhumiApi/api/Agency?idModel={"Id":-1,"AuthModel":{"Token":null,"Email":"gagan.s.paril@gmail.com","GmailId":null}} Gott Output
*/
namespace MathrubhumiSales.Mobile.Api
{
	class ApiAgencies : ApiBase
	{
		public ApiAgencies()
		{



		}
		public async Task<List<AgencyModel>> GetAll()
		{

			Url = Head + "api/Agency?idModel=" + idString;
			var str = await base.getAllAsync();
			try
			{
				return JsonConvert.DeserializeObject<List<AgencyModel>>(str);
			}
			catch(Exception e)
			{
				return null;
			}

		}
		//


		public async Task<BaseModel> Post(AgencyModel model)
		{
			this.model = model;
			Url = Head + "api/Agency" ;
			var response =await base.Post();

			return null;
		}
	}
}
