﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiClientDesignation : ApiBase
	{
		public async Task<List<DesignationModel>> GetAll()
		{
			using (var client = new HttpClient())
			{
				Url = Head + "api/ClientDesignation?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				string response = "";
				try
				{
					response = await client.GetStringAsync(Url);
					var retstring = response.ToString();
					return JsonConvert.DeserializeObject<List<DesignationModel>>(retstring);
				}
				catch (Exception e)
				{
					return null;
				}


			}
		}
	}
}
