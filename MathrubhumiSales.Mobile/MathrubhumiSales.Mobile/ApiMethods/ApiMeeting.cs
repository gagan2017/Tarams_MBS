using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiMeeting : ApiBase
	{
		async internal Task<HttpResponseMessage> Post(MeetingModel meetingModel)
		{
			Url = Head + "api/Meeting";
			this.model = meetingModel;
			return await base.Post();
		}
		public async Task<List<MeetingModel>> getAll(int cm)
		{
			idModel.Id = cm;
			Url = Head + "api/Meeting?idModel=" + JsonConvert.SerializeObject(idModel);
			string ret = await base.getAllAsync();

			if (ret == "")
			{
				var retItem = new List<MeetingModel>();
				retItem.Add(new MeetingModel("Error Check With Your internet"));
				return retItem;
			}
			if (ret == "[]")
			{
				var retItem = new List<MeetingModel>();
				retItem.Add(new MeetingModel("There Are No Meeting"));
				return retItem;
			}
			try
			{
				return JsonConvert.DeserializeObject<List<MeetingModel>>(ret);
			}
			catch (Exception e)
			{
				var retItem = new List<MeetingModel>();
				retItem.Add(new MeetingModel("Error in meeting"));
				return retItem;
			}
		}
		public async Task<CreateMeetingModel> getAllBeforPost()
		{
			idModel.Id = -1;
			Url = Head + "api/Meeting?idModel=" + JsonConvert.SerializeObject(idModel);
			string ret = await base.getAllAsync();
			return JsonConvert.DeserializeObject<CreateMeetingModel>(ret);
		}

		public async Task UpDateData(MeetingModel m)
		{
			Url = Head + "api/Meeting";
			model = m;
			await base.Post();
		}
	}
}
