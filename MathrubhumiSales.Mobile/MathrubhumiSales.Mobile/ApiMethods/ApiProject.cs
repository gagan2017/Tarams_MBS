﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiProject : ApiBase
	{
		public ApiProject()
		{
		}
		public async Task<List<ProjectModel>> GetAll()
		{
			using (var client = new HttpClient())
			{
				Url = Head + "api/Project?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				var response = await base.getAllAsync();
				return JsonConvert.DeserializeObject<List<ProjectModel>>(response.ToString());
			}
		}

		async internal Task<HttpResponseMessage> Post(ProjectModel projectModle)
		{
			Url = Head + "api/Project";
			this.model = projectModle;
			return await base.Post();
		}
	}
}
