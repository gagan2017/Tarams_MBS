﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class LaunchingPageCountModel
	{
		public AuthModel AuthModel { get; set; }
		public int ClientCount { get; set; }
		public int ProjectCount { get; set; }
		public int TeamCount { get; set; }
	}
}
