﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class CreateMeetingModel : BaseModel
	{
		public List<ClientModel> Clients { get; set; }
		public List<ProposalModel> Proposals { get; set; }
		public List<LabelModel> Labels { get; set; }
		public List<AgencyModel> Agencies { get; set; }
		public List<UserModel> Users { get; set; }
		public List<ProjectModel> Projects { get; set; }

	}
}
