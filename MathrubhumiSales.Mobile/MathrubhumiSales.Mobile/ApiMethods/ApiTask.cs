﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiTask : ApiBase
	{
		public ApiTask()
		{
		}
		public async Task<List<TaskModel>> GetAll()
		{
			using (var client = new HttpClient())
			{
				Url = Head + "api/Task?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				string response = "";
				response = await base.getAllAsync();
				var retstring = response.ToString();
				if (retstring == "[]"|| retstring=="null")
					{
						return new List<TaskModel>()
							{
								new TaskModel( "No Task in databse Create One","Add Label")
								
							};
					}

				try
				{
					return JsonConvert.DeserializeObject<List<TaskModel>>(retstring);
				}
				catch(Exception e)
				{
					return new List<TaskModel>()
							{
								new TaskModel( "Error","Error check your internet")
								
							};
				}

			}

		}
		public async Task<bool> Post(TaskModel model)
		{
			this.model = model;
			Url = Head + "api/Task";
			var ret = await base.Post();

			return ret.IsSuccessStatusCode;
		}

	}
}
