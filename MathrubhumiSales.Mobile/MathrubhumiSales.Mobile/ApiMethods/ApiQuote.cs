﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;
/*

Api Quotes : Get
http://10.191.3.111/MathrubhumiApi/api/Quote?authModel={"Token":null,"Email":"gagan.s.paril@gmail.com","GmailId":null}
getting null

*/
namespace MathrubhumiSales.Mobile.Api
{
	public class ApiQuote : ApiBase
	{
		public ApiQuote()
		{
		}
		public async Task<List<QuoteModel>> GetAll()
		{
			
			using (var client = new HttpClient())
			{
				Url = Head + "api/Quote?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				string response = "";
				if (Sessions.instance.GetQuote == ""|| Sessions.instance.GetQuote=="[]")
				{
					response = await base.getAllAsync();
					Sessions.instance.GetQuote = response.ToString();
				}
				else
				{
					response = Sessions.instance.GetQuote;
				}
				return JsonConvert.DeserializeObject<List<QuoteModel>>(response.ToString());
			}

		}
	}
}
