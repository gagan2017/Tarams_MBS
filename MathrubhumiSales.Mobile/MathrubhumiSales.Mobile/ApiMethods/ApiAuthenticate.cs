﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile.Api
{
	public class ApiAuthenticate : ApiBase
	{
		public ApiAuthenticate()
		{
			//api/Authentication
		}

		public async Task<string> Post(AuthModel model)
		{

			using (var client = new HttpClient())
			{
				var str = JsonConvert.SerializeObject(model);
				var content = new StringContent(str, Encoding.UTF8, "application/json");
				try
				{

					var response = await client.PostAsync(Head + "api/Authentication", content
														 );
				}
				catch (Exception E)
				{
				}


			}
			return null;
		}
		//deleting not done
		public async Task<string> Delete(AuthModel model)
		{

			using (var client = new HttpClient())
			{
				var str = JsonConvert.SerializeObject(model);
				var content = new StringContent(str, Encoding.UTF8, "application/json");
				try
				{

					var response = await client.PostAsync(Url + "api/Authentication" , content );
				}
				catch (Exception E)
				{
				}


			}
			return null;
		}
	}
}

