﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile.Api
{
	public class ApiGroup : ApiBase
	{
		static ApiGroup _instance { get; set; }
		public static ApiGroup Instance { get { if (_instance == null) _instance = new ApiGroup(); return _instance; } }

		public ApiGroup()
		{
		}
		public async Task<List<GroupModel>> GetAll()
		{
			using (var client = new HttpClient())
			{
				Url = Head + "api/Group?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
				string response = "";
				try
				{
					response = await client.GetStringAsync(Url);
					var retstring = response.ToString();
					return JsonConvert.DeserializeObject<List<GroupModel>>(retstring);
				}
				catch (Exception e)
				{
					return null;
				}

			}

		}
		public async Task<string> Post(GroupModel model)
		{

			using (var client = new HttpClient())
			{
				this.model = model;
				Url = Head + "api/Group";
				var ret = await base.Post();

			}
			return null;
		}

	}
}
