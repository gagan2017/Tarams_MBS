﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile
{
	public class ApiAuthModel
	{
		async public Task<UserModel> Post(AuthModel Model)
		{
			UserModel retitem = null;
			using (var client = new HttpClient())
			{
				var str = JsonConvert.SerializeObject(Model);
				var content = new StringContent(str, Encoding.UTF8, "application/json");
				try
				{
					var response = await client.PostAsync("http://10.191.2.233/MathrubhumiApi/api/Authentication", content);
					if (response.StatusCode==System.Net.HttpStatusCode.OK)
					{
						var res = await response.Content.ReadAsStringAsync();
						var ret = JsonConvert.DeserializeObject<UserModel>(res);
 						return ret ;
					}

					return null;

				}
				catch (Exception E)
				{
					return new UserModel()
					{
						Id = -1
					};
				}


			}

		}
	}
}
