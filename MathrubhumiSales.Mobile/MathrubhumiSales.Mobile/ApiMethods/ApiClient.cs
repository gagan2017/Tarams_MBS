﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;

namespace MathrubhumiSales.Mobile.Api
{
	public class ApiClient : ApiBase
	{
		
		public ApiClient()
		{
		}
		public async Task<List<ClientModel>> getAll()
		{


			idModel.Id = 0;
			Url = Head + "api/Client?authModel=" + JsonConvert.SerializeObject(idModel.AuthModel);
			var ret = await base.getAllAsync();
			if (ret == "")
			{
				List<ClientModel> retitem=new List<ClientModel>();
				retitem = new List<ClientModel>()
				{
					new ClientModel(){
						Id=-1,
						Name="No Clients as of Now",
						Agencies=new List<Models.AgencyModel>(),
						Brands=new List<BrandModel>(),
						ClientContacts=new List<ClientContactModel>(),
						Products=new List<ProductModel>()
					}
				};
				return retitem;

			}
			return JsonConvert.DeserializeObject<List<ClientModel>>(ret);
		}

		public async Task<HttpResponseMessage> Post(ClientModel cModel)
		{

			using (var client = new HttpClient())
			{
				var str = JsonConvert.SerializeObject(model);

				this.model = cModel;
				Url = Head + "api/Client";
				return await base.Post();
			}
			return null;
		}
		/*	public async Task<string> PostProduct(QuoteModel model)
			{

				using (var client = new HttpClient())
				{
					var str = JsonConvert.SerializeObject(model);

					var content = new StringContent(str, Encoding.UTF8, "application/json");

					var url = Url + "api/Product?productModel=" + str;

					var response = await client.PostAsync(url, content);
				}
				return null;
			}
			public async Task<string> PostBrand(QuoteModel model)
			{

				using (var client = new HttpClient())
				{
					var str = JsonConvert.SerializeObject(model);

					var content = new StringContent(str, Encoding.UTF8, "application/json");

					var url = Url + "api/Brand?prandModel=" + str;

					var response = await client.PostAsync(url, content);
				}
				return null;
			}*/
	}
}
