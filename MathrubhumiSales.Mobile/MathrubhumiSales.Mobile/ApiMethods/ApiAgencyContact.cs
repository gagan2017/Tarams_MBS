﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Models;
/*agency Contact Get :http://10.191.3.111/MathrubhumiApi/api/Agency/AgencyContact?idModel={"Id":-1,"AuthModel":{"Token":null,"Email":"gagan.s.paril@gmail.com","GmailId":null}} getting same output for Agency and AgencyContact
agency Contact Post:
http://10.191.3.111/MathrubhumiApi/api/Agency/AgencyContact?agencyContactModel={"AgencyId":1,"FirstName":"gagan","LastName":"s","DesignationId":2,"Email":"gagan.s.patil88@gmail.com","ContactNumber":"9901719599","Id":0,"Name":"gagan","AuthModel":null}
data=: 
{"AgencyId":1,"FirstName":"gagan","LastName":"s","DesignationId":2,"Email":"gagan.s.patil88@gmail.com","ContactNumber":"9901719599","Id":0,"Name":"gagan","AuthModel":null}*/
namespace MathrubhumiSales.Mobile.Api
{
	class ApiAgencyContact : ApiBase
	{
		public ApiAgencyContact()
		{

		}
		public async Task<List<AgencyContactModel>> GetAll()
		{
			Url = Head + "api/AgencyContact" + "?idModel=" + idString;
			var retItem = await base.getAllAsync();

			return JsonConvert.DeserializeObject<List<AgencyContactModel>>(retItem);


		}
		public async Task<List<AgencyContactModel>> GetById(int id)
		{
			idModel.Id = id;
			Url = Head + "api/AgencyContact" + "?idModel=" + JsonConvert.SerializeObject(idModel);

			var retItem = await base.getAllAsync();

			return JsonConvert.DeserializeObject<List<AgencyContactModel>>(retItem);
		}
		public async Task<string> Post(AgencyContactModel model)
		{
			this.model = model;
			Url = Head + "api/AgencyContact";
			var retItem = await base.Post();
			return null;
		}
	}
}
