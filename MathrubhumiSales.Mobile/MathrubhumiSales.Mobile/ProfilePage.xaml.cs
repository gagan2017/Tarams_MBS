﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ProfilePage : ContentPage
	{
		async protected override void OnDisappearing()
		{
			base.OnDisappearing();

		}
		async protected override void OnAppearing()
		{
			base.OnAppearing();

		}


		public ProfilePage()
		{

			var bindable = new ProfilePageViewModel();

			BindingContext = bindable;
			InitializeComponent();
			var imgSource = new UriImageSource()
			{
				Uri = new Uri(bindable.profile.imgUrl),
				CachingEnabled = true,
			};

			profileimage.Source =imgSource;
			indicator.SetBinding(ActivityIndicator.IsRunningProperty, "IsLoading");
			indicator.SetBinding(ActivityIndicator.IsVisibleProperty, "IsLoading");
			indicator.BindingContext = profileimage;

			sVal.Value = (Sessions.instance.AppTextSize);

		}
		async void Handle_Item_Tapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as ListItems;
			if (e == null) return;
			((ListView)sender).SelectedItem = null;
			// has been set to null, do not 'process' tapped event
			DependencyService.Get<ICustomPrgressBar>().Start(0);

			await Navigation.PushAsync(FactoryPages._instance.GetPage(v.Listname));
			DependencyService.Get<ICustomPrgressBar>().Stop();
			 // de-select the row

		}

		void Handle_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
		{

			var s = (Slider)sender;
			var newStep = Math.Round(e.NewValue );

			s.Value = newStep ;

			Sessions.instance.AppTextSize = s.Value;
		}
	}
}
