﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

using Newtonsoft.Json;
namespace MathrubhumiSales.Mobile
{
	public class Sessions
	{
		IdModel id;
		static Sessions _instance;
		public static Sessions instance
		{
			get
			{
				if (_instance == null)
					_instance = new Sessions();
				return _instance;
			}

		}
		public UserModel CurrentUser
		{
			get
			{
				return JsonConvert.DeserializeObject<UserModel>(CurrentUserString);
			}
			set
			{
				CurrentUserString = JsonConvert.SerializeObject(value);
			}
		}
		private Sessions()
		{


		}

		public string CurrentUserString
		{
			get
			{
				var x = DependencyService.Get<IAppProperties>().get(Keys.Key_UserObject);
				if (x == (null) || x == "") return "null";
				return x;
			}
			set
			{
				DependencyService.Get<IAppProperties>().set(value, Keys.Key_UserObject);
			}
		}
		public string AccessToken
		{
			get
			{
				return DependencyService.Get<IAppProperties>().get(Keys.Key_IsLogIn);
			}
			set
			{
				DependencyService.Get<IAppProperties>().set(value, Keys.Key_AccessToken);
			}
		}

		public Sliders sliders = Sliders.GetInstance();
		public double AppTextSize
		{
			get
			{
				var v = DependencyService.Get<IAppProperties>().getDouble(Keys.Key_Text_Size);
				sliders.setValue(v);
				return v;

				//	return -1;
			}
			set
			{
				DependencyService.Get<IAppProperties>().setDouble(value, Keys.Key_Text_Size);

				sliders.setValue(value);
			}
		}
		public string GetQuote
		{
			get
			{
				return DependencyService.Get<IAppProperties>().get(Keys.Key_GetQuote);

			}
			set
			{
				DependencyService.Get<IAppProperties>().set(value, Keys.Key_GetQuote);
			}
		}
		public int GetDisplay
		{
			get
			{
				var v = DependencyService.Get<IAppProperties>().getInt(Keys.Key_GetDisplayDay);
				if (v == 0)
					return DateTime.Now.DayOfYear;
				return v;
			}
			set
			{
				DependencyService.Get<IAppProperties>().setInt(value, Keys.Key_GetDisplayDay);
			}
		}



	}
}
