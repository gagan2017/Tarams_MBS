﻿using System;
namespace MathrubhumiSales.Mobile
{
	public interface ICustomPrgressBar
	{
		void Start(double Time);
		void Stop();
	}
}
