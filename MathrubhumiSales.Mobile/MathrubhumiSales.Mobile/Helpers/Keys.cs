﻿using System;
namespace MathrubhumiSales.Mobile
{
	public static class Keys
	{
		public static string Key_AccessToken = "AccessToken";
		public static string Key_IsLogIn = "IsLogIn";
		public static string Key_isFTime = "Key_isFTime";
		public static string Key_Text_Size = "Key_Text_Size";
		public static string Key_Name = "Name";
		public static string Key_Email = "Email";
		public static string Key_Phoneno = "PhoneNo";
		public static string StaticDataKey_Id = "Id";
		public static string Key_ProfilePicUrl = "ProfilePicUrl";
		public static string Key_UserObject = "CurrentUserObject";
		public static string Key_StartTime = "StartTime";
		public static string Key_EndTime = "EndTime";
		public static string Key_LastName = "LastName";
		public static string Key_GetQuote = "GetQuote";
		public static string Key_GetDisplayDay = "GetDisplayDay";
		public static string Key_MainPage = "MainPage";

	}
}
