﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class BaseModelAndroidIos
	{
		UserModel CurrentUser { get; set; }
		public string Name { get; set; }
		public string Lname { get; set; }
		public string Token { get; set; }
		public string ProfilePicUrl { get; set; }
		public string Email { get; set; }
		public string Phoneno { get; set; }
		public string Id { get; set; }
		public void SetUser(UserModel User)
		{
			CurrentUser = User;
		}
		public UserModel GetUser()
		{
			return CurrentUser;
		}
	}
}
