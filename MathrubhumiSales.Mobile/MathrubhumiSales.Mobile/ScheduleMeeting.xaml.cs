﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ScheduleMeeting : ContentPage
	{
		ScheduleMeetingViewModel model;
		TapGestureRecognizer Ataappe, Ptapped;
		Image activated;
		bool starTapped = false;
		bool listTapped = false;
		public ScheduleMeeting()
		{

			model = new ScheduleMeetingViewModel();
			InitializeComponent();
			Ataappe = new TapGestureRecognizer();
			Ataappe.Tapped += Taappe_Tapped;
			Ptapped = new TapGestureRecognizer();
			Ptapped.Tapped += Ptapped_Tapped;
			AgencyListView.IsVisible = false;
			ProjectListView.IsVisible = false;
			model.Start(client, proposal, label, activity);
			CustomCheckbox.InvokeCheckBoxClicked += CustomCheckbox_InvokeCheckBoxClicked;
		}

		void CustomCheckbox_InvokeCheckBoxClicked(object sender, EventArgs e)
		{
			var sen = int.Parse((string)sender);
			model.setUserbyId(sen);
		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			var sen = (Image)sender;
			if (starTapped)
			{
				sen.Source = "StarNotSelected.png";
				starTapped = false;
				model.setIsMarked(false);
				return;
			}
			sen.Source = "StarSelected.png";
			starTapped = true;
			model.setIsMarked(true);
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			var sen = (CustomLabel)sender;
			switch (sen.Text)
			{
				case "+ Contact": Contact.IsVisible = true; btAgency.GestureRecognizers.Add(Handleclicked); break;
				case "+ Agency": Agency.IsVisible = true; btProduct.GestureRecognizers.Add(Handleclicked); break;
				case "+ Project": addagencybutton.IsVisible = false; Project.IsVisible = true; break;
			}
			sen.IsVisible = false;
		}
		void Handle_Plus_Tapped(object sender, System.EventArgs e)
		{

		}
		void Handle_Add_Clicked(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new AddAgencyPage());
		}
		async void Handle_Schedule_Clicked(object sender, System.EventArgs e)
		{
			/*if (comment == null || comment.Text.Trim().Length == 0)
			{
				await DisplayAlert("error", "Provide Comment ", "Ok!");

				return;
			}*/
			if (model.ValidateNull())
			{
				await DisplayAlert("error", model.status, "Ok!");
				return;
			}
			model.setDateTime(dateTime.date);
			var tim = await DisplayAlert("Alert", "Are You sure to schedule meeting at " + FactoryDate._instance.getdateTime(model.getMeetingTime()), "Ok", "Cancel");
			if (!tim)
			{
				return;
			}
			if (comment.Text == null || comment.Text.Trim().Length == 0)
			{
				comment.Text = "";

			}
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			if (await model.Post(comment.Text))
			{
				await DisplayAlert("Success", "Meeting at " + FactoryDate._instance.getdateTime(model.getMeetingTime()), "Ok!");

			}
			else
			{
				await DisplayAlert("error", "Couldnt Add some server issue", "Ok!");
			}
			DependencyService.Get<ICustomPrgressBar>().Stop();
			await Navigation.PopAsync();
		}

		async void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			var v = sender as SearchBar;
			listItems.IsRefreshing = true;
			listItems.ItemsSource = await model.getUsers(v.Text);
			listItems.IsRefreshing = false;
		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var item = e.Item as UserModel;
			if (item == null) return;
			model.setUser(item);
			((ListView)sender).SelectedItem = null;
		}

		void Handle_SelectedIndexChanged_Client(object sender, System.EventArgs e)
		{
			model.setClient(client.SelectedItem as string);

		}
		void Handle_SelectedIndexChanged_Proposal(object sender, System.EventArgs e)
		{
			model.setProposal(proposal.SelectedItem as string);
		}
		void Handle_SelectedIndexChanged_Label(object sender, System.EventArgs e)
		{
			model.setLabel(label.SelectedItem as string);
		}
		void Handle_selected_Tapped(object sender, System.EventArgs e)
		{
			var sen = (Image)sender;

			if (activated != null)
			{ activated.Source = getNotActivated(); }
			else
			{
				activated = sen;
				activated.Source = getNotActivated();
			}

			if (sen.Id == one.Id)
			{
				model.setMeetinType(MeetingType.TelephonicMeeting);
				activated = sen;
				sen.Source = "callactivated.png";

				return;
			}
			if (sen.Id == two.Id)
			{
				model.setMeetinType(MeetingType.EmailMeeting);
				activated = sen;
				sen.Source = "messageactivated.png";
				return;
			}

			model.setMeetinType(MeetingType.GroupMeeting);
			activated = sen;
			sen.Source = "groupactivated.png";
			return;

		}

		string getNotActivated()
		{
			if (activated.Id == one.Id)
			{
				return "callnotactivated.png";

			}
			if (activated.Id == two.Id)
			{
				return "messagenotactivated.png";
			}
			return "groupnotactivated.png";
		}

		void Handle_Agency_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			AgencyListView.IsVisible = true;
			AgencyListView.HeightRequest = 75;
			var v = sender as Entry;
			var result = model.AgencyModel.FindAll(s => s.Name.Contains(v.Text));
			AgencyListView.ItemsSource = result;

		}
		void Handle_Project_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			ProjectListView.IsVisible = true;
			ProjectListView.HeightRequest = 75;
			var v = sender as Entry;
			if (model.ProjectModel == null)
			{
				DisplayAlert("Error", "No Project Add Projects", "ok");
				return;
			}
			try
			{
				var result = model.ProjectModel.FindAll(s => s.ProjectName.Contains(v.Text));
				ProjectListView.ItemsSource = result;
			}
			catch (Exception es)
			{
			}

		}

		void Handle_Project_Item_Tapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			if (e.Item == null) return;
			var v = e.Item as ProjectModel;
			var projectlabel = new Label()
			{
				Text = v.ProjectName
			};

			projectlabel.GestureRecognizers.Add(Ptapped);
			projectContent.Children.Add(projectlabel);
			model.AddProjects(v);
			((ListView)sender).SelectedItem = null;
		}
		void Handle_Agency_Item_Tapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{

			if (e.Item == null) return;
			var v = e.Item as AgencyModel;
			var agencyLabel = new Label()
			{
				Text = v.Name
			};
			agencyLabel.GestureRecognizers.Add(Ataappe);
			agencyContent.Children.Add(agencyLabel);
			model.AddAgency(v);
			((ListView)sender).SelectedItem = null;

		}

		void Taappe_Tapped(object sender, EventArgs e)
		{
			agencyContent.Children.Remove((Label)(sender));

			model.AddAgency(model.AgencyModel.Find((obj) => obj.Name == ((Label)(sender)).Text));
		}

		void Ptapped_Tapped(object sender, EventArgs e)
		{
			projectContent.Children.Remove((Label)(sender));
			model.AddProjects(model.ProjectModel.Find((obj) => obj.Name == ((Label)(sender)).Text));
		}

		void Handle_AddProposal_Tapped(object sender, System.EventArgs e)
		{

			Navigation.PushAsync(new AddProposalPage(this.model.ProposalModel));
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (model == null || model.ProposalModel == null || proposal == null) return;
			proposal.Items.Clear();
			foreach (var item in model.ProposalModel)
			{
				proposal.Items.Add(item.Id + " " + item.Summary + " " + item.Amount);
			}
		}
	}
}
