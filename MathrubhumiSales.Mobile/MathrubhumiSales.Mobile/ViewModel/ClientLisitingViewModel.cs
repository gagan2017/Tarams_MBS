﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ClientLisitingViewModel : IAsyncInitialization
	{
		public ObservableCollection<ClientListingModel> listitemsAll { get; set; }
		public ObservableCollection<ClientListingModel> listitemsRecent { get; set; }
		public List<ClientModel> cm { get; set; }
		ListView list;
		public int Active { get; set; }
		public Task Initialization
		{
			get; private set;
		}

		public ClientLisitingViewModel()
		{
			listitemsAll = new ObservableCollection<ClientListingModel>();

			listitemsRecent = new ObservableCollection<ClientListingModel>();

		}

		public void Refresh()
		{
			Initialization = getData();
		}

		async public Task getData()
		{
			cm = await new ApiClient().getAll();
			listitemsAll.Clear();
			listitemsRecent.Clear();
			var count = -1;
			foreach (var item in cm)
			{
				listitemsAll.Add(new ClientListingModel(item));
				count++;
				if (count >= 20)
				{
					continue;
				}
				listitemsRecent.Add(new ClientListingModel(item));

			}
			Active = listitemsRecent.Count;
		}


	}
}



