using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Model;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	class ToDoDetailPage2ViewModel : IAsyncInitialization
	{
		TaskModel task;
		ListView listview;
		public CommentsBlueItem _blueItem { get; set; }
		public ObservableCollection<Comments> listofcomments { get; set; }

		public Task Initialization
		{
			get; private set;
		}

		public ToDoDetailPage2ViewModel(TaskModel task)
		{
			listofcomments = new ObservableCollection<Comments>();
			/*listofcomments.Add(new Comments()
			{
				Description = "I asdsadwill do that by the end of day today or early tomorrow",
				Timeago = DateTime.Today,
				Who = UserFactory._Instance.getUser(2).Name,
				ProfilePic = UserFactory._Instance.getUser(2).Profilepic
			}
			 );*/
			this.task = task;

		}

		internal void Start()
		{
			Initialization = GetData();
		}

		async public Task GetData()
		{
			var list = await new ApiComents().getAll(task);

			listofcomments.Clear();
			foreach (var item in list)
			{
				if(!(item.Comment==null|| item.Comment.Trim().Length==0))
					listofcomments.Add(new Comments(item));
			}

		}

		async public Task<bool> Post(string text)
		{
			var ret = await new ApiComents().Post(new TaskComment()
			{
				Comment = text,
				User = Sessions.instance.CurrentUser,
				CommentedTime = DateTime.Now,
				Task = task
			});
			return ret;
		}
		public void Refresh()
		{
			Initialization = GetData();
		}
	}
}