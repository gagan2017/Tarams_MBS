﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Model;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	class LandingPageViewModel : IAsyncInitialization
	{
		public event EventHandler StartLoader, StopLoader;

		public ObservableCollection<ListItemModel> TodayItems { get; set; }
		ObservableCollection<ListItemModel> SevenDaysItems { get; set; }
		ObservableCollection<ListItemModel> AllItems { get; set; }
		public LandingPageViewModel()
		{
			listItems = new ObservableCollection<ListItems>();
			staticItemSource = new ObservableCollection<AllPageModel>();
			listItemsSorted = new ObservableCollection<ListItemModel>();
			DummyListItemViewModel.Instance.Updated += Instance_Updated;

			TodayItems = new ObservableCollection<ListItemModel>();
			TodayItems.Add(new ListItemModel());
			SevenDaysItems = new ObservableCollection<ListItemModel>();
			AllItems = new ObservableCollection<ListItemModel>();
			staticItemSource.Add(new AllPageModel()
			{
				Listname = "Un Scheduled",
				Count = 15
			});
			staticItemSource.Add(new AllPageModel()
			{
				Listname = "Completed",
				Count = 223
			});
			if (DummyListItemViewModel.Instance.itemListUnsorted.Count != 0)
			{
				itemUnsorted = DummyListItemViewModel.Instance.itemListUnsorted;
                setTodayItems();
				setSevenDaysItems();
				setAllItems();
			}
		}


		public Percentage CC
		{
			get
			{
				return new Percentage()
				{
					StatusGained = false,
					PercentageTook = "23"
				};
			}
		}
		public Percentage Oly
		{
			get
			{
				return new Percentage()
				{
					StatusGained = false,
					PercentageTook = "8%"
				};
			}
		}

		public Percentage Cagr
		{
			get
			{
				return new Percentage()
				{
					StatusGained = true,
					PercentageTook = "12%"
				};
			}
		}

		public Percentage Sip
		{
			get
			{
				return new Percentage()
				{
					StatusGained = false,
					PercentageTook = "35.2"
				};
			}
		}

		internal void GetData()
		{
			Initialization =
				GetAll();
		}

		async Task GetAll()
		{
			var res = new ApiBase();
			res.Url = res.Head + "api/Data?authModel=" + JsonConvert.SerializeObject(res.idModel.AuthModel);
			var ans = await res.getAllAsync();
			var lis = JsonConvert.DeserializeObject<LaunchingPageCountModel>(ans.ToString());
			listItems.Clear();

			listItems.Add(new ListItems() { Listname = "Clients", Num = lis.ClientCount.ToString() });
			listItems.Add(new ListItems() { Listname = "Projects", Num = lis.ProjectCount.ToString() });
			listItems.Add(new ListItems() { Listname = "Teams", Num = lis.TeamCount.ToString() });

		}

		public ObservableCollection<ListItems> listItems { get; set; }
		public MonthAndYear data
		{
			get
			{
				return new MonthAndYear()
				{
					month = "Jan",
					year = 2010
				};
			}
		}

		public Task Initialization
		{
			get; private set;
		}


		List<ListItemModel> itemUnsorted;
		public string CurrentItems = "Today";


		public ObservableCollection<ListItemModel> listItemsSorted { get; set; }
		public ObservableCollection<AllPageModel> staticItemSource { get; set; }

		void Instance_Updated(object sender, EventArgs e)
		{
			itemUnsorted = DummyListItemViewModel.Instance.itemListUnsorted;
			setTodayItems();
			setSevenDaysItems();
			setAllItems();
			StopLoader?.Invoke(null,new EventArgs());
		}

		internal ObservableCollection<ListItemModel> setData(string text)
		{

			CurrentItems = text;
			switch (text)
			{
				case "Today": return TodayItems; break;
				case "7Days": return SevenDaysItems; break;
				case "All": return AllItems; break;
			}
			return null;

		}

		internal void Refresh()
		{
			StartLoader?.Invoke(null,new EventArgs());
			DummyListItemViewModel.Instance.Refresh();
			GetData();
		}

		public void setAllItems()
		{
			AllItems.Clear();
			foreach (var item in itemUnsorted)
			{
				AllItems.Add(item);
			}

		}

		public void setTodayItems()
		{
			TodayItems.Clear();

			var lObj = itemUnsorted.FindAll((obj) => obj.Time.DayOfYear == DateTime.Now.DayOfYear);
			if (lObj == null || lObj.Count == 0)
			{
				TodayItems.Add(new ListItemModel()
				{
					Heading = "Nothing Scheduled for Todays Date"
				});
				return;
			}
			TodayItems.Add(new ListItemModel()
			{
				Heading = "Today " + FactoryDate._instance.GetMonth(DateTime.Now)
			});
			foreach (var item in lObj)
			{
				TodayItems.Add(item);
			}
		}
		public void setSevenDaysItems()
		{


			SevenDaysItems.Clear();
			setTommorrowElements();
			setSevenDaysElements();

		}
		public void setTommorrowElements()
		{
			var lObj = itemUnsorted.FindAll((obj) => obj.Time.DayOfYear == (DateTime.Now.AddDays(1).DayOfYear));
			if (lObj == null || lObj.Count == 0)
			{
				return;
			}
			SevenDaysItems.Add(new ListItemModel()
			{
				Heading = "Tommorrow " + FactoryDate._instance.getTommorrowDate()
			});
			foreach (var item in lObj)
			{
				SevenDaysItems.Add(item);
			}
		}
		void setSevenDaysElements()
		{
			var lObj = itemUnsorted.FindAll((obj) => obj.Time.DayOfYear <= DateTime.Now.AddDays(7).DayOfYear && obj.Time.DayOfYear > DateTime.Now.AddDays(1).DayOfYear);
			if (lObj == null || lObj.Count == 0)
			{
				SevenDaysItems.Add(new ListItemModel()
				{
					Heading = "Nothing for upcomming 7 days"
				});
				return;
			}
			SevenDaysItems.Add(new ListItemModel()
			{
				Heading = "Up to 7 days Except Tommorrow"
			});
			foreach (var item in lObj)
			{
				SevenDaysItems.Add(item);
			}
		}

	}
}