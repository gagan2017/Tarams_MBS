﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class ClientListingFilterViewModel
	{
		public List<string> checkboxlist { get; set; }
		public Dictionary<string, bool> list { get; set; }
		public ClientListingFilterViewModel()
		{
			list = new Dictionary<string, bool>();
			list.Add("Last 7 Days", false);
			list.Add("Last month", false);
			list.Add("Construction", false);
			list.Add("Health care", false);
			list.Add("Fcmg", false);
			checkboxlist = new List<string>();
			foreach (var item in list)
			{
				checkboxlist.Add(item.Key);
			}
		}
		internal void Refresh(List<string> lists)
		{
			foreach (var item in lists)
			{
				list[item] = true;
			}

		}
	}
}

