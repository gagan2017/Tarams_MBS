﻿using System;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ProfilePageViewModel
	{
		public Profile profile;
		public int point = 10;
		public Boolean IsLoading { get; }
		public string name { get; set; }
		public string lb1text { get; set; }
		public string lb2 { get; set; }

		public FormattedString lb1 { get; set; }

		public System.Collections.Generic.List<ListItems> listItems
		{
			get
			{
				return new System.Collections.Generic.List<ListItems>()
				{
					new ListItems(){ Listname="Analytics" },
					new ListItems(){ Listname="Sales Data" },
					new ListItems(){ Listname="Collection Data" },
					new ListItems(){ Listname="Expenses" },
					new ListItems(){ Listname="Approvals" },
					new ListItems(){ Listname="Manage Users" },
					new ListItems(){ Listname="Agencies" },
					new ListItems(){ Listname="Help" }



				};
			}
		}
		public ProfilePageViewModel()
		{

			profile = new Profile()
			{
				name = Sessions.instance.CurrentUser.FirstName,
				id = "e#038",
				job = Sessions.instance.CurrentUser.Designation.Name,
				place = "Delhi South",
				type = "FMCG, Automobiles",
				d = "Television Vertical",
				imgUrl = Sessions.instance.CurrentUser.ProfilePicUrl
			};
			name = profile.name + "\r\n";
			lb1text = profile.id + "\r\n" + profile.job + " ,\r\n" + profile.place;
			lb2 = profile.type + "\r\n" + profile.d;

			lb1 = new FormattedString()
			{
				Spans = {
						new Span { Text = profile.name + " ", ForegroundColor = Color.Black, Font = Font.SystemFontOfSize(22) },
						new Span { Text = lb1text, ForegroundColor = Color.Gray, Font = Font.SystemFontOfSize(20) }
					}
			};

		}
	}
	public class Profile
	{
		public string name { get; set; }
		public string id { get; set; }
		public string job { get; set; }
		public string place { get; set; }
		public string type { get; set; }
		public string d { get; set; }
		public string imgUrl{ get; set; }
	}
}
