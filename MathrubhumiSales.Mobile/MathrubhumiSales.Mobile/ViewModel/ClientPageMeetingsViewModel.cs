﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class ClientPageMeetingsViewModel 
	{
		public List<CommentsRedItem> listOfRedItemComments { get; set; }

		public ClientPageMeetingsViewModel(List<CommentsRedItem> pRed)
		{
			listOfRedItemComments = new List<CommentsRedItem>();
			foreach (var item in pRed)
			{
				if (item.m.Id == 0) continue;
				item.Changedate(FactoryDate._instance.getdateTime(item.UpdateTime));

				listOfRedItemComments.Add(item);
			}
		}
	}
}

