﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class UserListViewModel
	{
		public int Id { get; set; }
		List<User> listOfUsers { get; set; }
		public string GroupName { get; set; }
		public string One { get; set; }
		public string Two { get; set; }
		public string Three { get; set; }
		public string Four { get; set; }
		public string Five { get; set; }
		public string Onei { get; set; }
		public string Twoi { get; set; }
		public string Threei { get; set; }
		public string Fouri { get; set; }
		public string Fivei { get; set; }

		public int CountOfRemainingUser{ get; set; }

		public UserListViewModel(Group g)
		{
			this.GroupName = g.GroupName;
			this.listOfUsers = g.ListOfUsersinGroup;
			NullCheck();

			if (listOfUsers.Count > 5)
			{
				CountOfRemainingUser = listOfUsers.Count - 5;
			}
			else
			{
				CountOfRemainingUser = -1;
			}
		}

		void NullCheck()
		{
			if (listOfUsers.Count>=1 && listOfUsers[0] != null)
			{
				One = listOfUsers[0].Name;
				Onei = listOfUsers[0].Profilepic;
			}
			if ( listOfUsers.Count>=2 && listOfUsers[1] != null)
			{
				Two = listOfUsers[1].Name;
				Twoi = listOfUsers[1].Profilepic;
			}
			if (listOfUsers.Count>=3 && listOfUsers[2] != null)
			{
				Three = listOfUsers[2].Name;
				Threei = listOfUsers[2].Profilepic;
			}
			if (listOfUsers.Count>=4 &&  listOfUsers[3] != null)
			{
				Four = listOfUsers[3].Name;
				Fouri = listOfUsers[3].Profilepic;
			}
			if (listOfUsers.Count>=5 && listOfUsers[4] != null)
			{
				Five = listOfUsers[4].Name;
				Fivei = listOfUsers[4].Profilepic;
			}








		}
	}

	public class UserGroupListViewModel
	{

		public List<UserListViewModel> list { get; set; }
		public UserGroupListViewModel()
		{
			
			list = new List<UserListViewModel>();
			foreach (var item in GroupFactory._instance.GetAll())
			{
				list.Add(new UserListViewModel(item));
			}


		}
	}
}
