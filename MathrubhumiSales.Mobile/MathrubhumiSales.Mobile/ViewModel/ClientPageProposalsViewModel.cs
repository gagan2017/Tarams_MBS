using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;

namespace MathrubhumiSales.Mobile
{
	public class ClientPageProposalsViewModel
	{
		public ObservableCollection<ProposalModal> listitems { get; set; }
		List<CommentsRedItem> pRedItems;

		public ClientPageProposalsViewModel(List<CommentsRedItem> p)
		{
			listitems = new ObservableCollection<ProposalModal>();
			Refresh(p);

		}
		internal CommentsRedItem GetElements(int ProposalId, int MeetingId)
		{
			return pRedItems.Find((obj) => obj.m.Id == MeetingId && obj.m.Proposal.Id == ProposalId);
		}

		internal void Refresh(List<CommentsRedItem> list)
		{
			listitems.Clear();
			pRedItems = list;
			foreach (var item in list)
			{
				if (item.m.Proposal==null || item.m.Proposal.Id == 0) continue;

				item.m.Proposal.DealStatus = item.m.DealStatus;
				listitems.Add(new ProposalModal(item.m.Proposal, item.m.Id));
			}
		}
	}
}
