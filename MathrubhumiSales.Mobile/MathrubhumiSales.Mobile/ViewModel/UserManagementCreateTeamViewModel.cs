﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class UserManagementCreateTeamViewModel
	{
		public List<User> listItems { get; set; }
		public UserManagementCreateTeamViewModel()
		{
			listItems = UserFactory._Instance.GetAll();

		}
		public List<string> contents()
		{
			var retitem = new List<string>();
			foreach (var item in listItems)
			{
				retitem.Add(item.Name);
			}
			return retitem;
		}
	}
}

