﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Class;
using MathrubhumiSales.Shared.Models;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using XamFormsImageResize;

namespace MathrubhumiSales.Mobile
{
	public partial class CreateClientContact : ContentPage
	{
		int id = -1;
		//MediaFile file { get; set; }
		AddClientContactViewModel model;
		ClientModel clientmodel;
		public CreateClientContact(ClientModel clientmodel)
		{
			InitializeComponent();
			model = new AddClientContactViewModel(picker);
			this.clientmodel = clientmodel;
			picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
		}

		void Picker_SelectedIndexChanged(object sender, EventArgs e)
		{
			var mod = model.Designationitems.Find((obj) => obj.Name == ((string)picker.SelectedItem));

			id = mod.Id;
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			string path = "";

			if (id == -1)
			{
				await DisplayAlert("Error", "Select Designation Pic", "Ok");
				return;
			}
			if (picker.SelectedItem == null)
			{
				await DisplayAlert("Error", "Select Designation", "Ok");
				return;
			}
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
				enFirstName,enLastName,enPhnumber,enEmail
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			if (ValidationClass._instance.EmailValidation(enEmail.Text))
			{
				await DisplayAlert("Error", "Please give valid email", "Ok!");
				return;
			}
			if (ValidationClass._instance.PhnoValidation(enPhnumber.Text))
			{
				await DisplayAlert("Error", "Please give valid Phone number", "ok!");
				return;
			}
			if (this.clientmodel.ClientContacts == null)
			{
				this.clientmodel.ClientContacts = new List<ClientContactModel>();
			}
			this.clientmodel.ClientContacts.Add(new ClientContactModel()
			{
				FirstName = enFirstName.Text,
				LastName = enLastName.Text,
				Email = enEmail.Text,
				ContactNumber = enPhnumber.Text,
				//	ProfilePic= await ResizeImage(),
				//ProfilePicpath = file.Path,
				DesignationId = id,
				Designation = picker.SelectedItem.ToString(),
				//ProfilePic = getPicByte()
			});
			DisplayAlert("Done", "Created Succesfully", "Ok");
			Navigation.PopAsync();
		}
		void Handle_Tapped(object sender, System.EventArgs e)
		{
		}

		/*async void Handle_Tapped(object sender, System.EventArgs e)
		{
			file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions());
			if (file != null)
			{
				var aPpath = file.AlbumPath;
				var path = file.Path;
				image.Aspect = Aspect.AspectFit;
				image.Source = path;
			}
		}
		byte[] getPicByte()
		{
			using (var memoryStream = new MemoryStream())
			{
				file.GetStream().CopyTo(memoryStream);
				file.Dispose();
				return memoryStream.ToArray();
			}
		}
		protected async Task<byte[]> ResizeImage()
		{
			/*var assembly = typeof(CreateClient).GetTypeInfo().Assembly;
			byte[] imageData;

			Stream stream = assembly.GetManifestResourceStream(ResourcePrefix + "OriginalImage.JPG");

			using (MemoryStream ms = new MemoryStream())
			{
				stream.CopyTo(ms);
				imageData = ms.ToArray();
			}

			return await ImageResizer.ResizeImage(getPicByte(), 100, 100);

			//this._photo.Source = ImageSource.FromStream(() => new MemoryStream(resizedImage));
		}*/
	}
}
