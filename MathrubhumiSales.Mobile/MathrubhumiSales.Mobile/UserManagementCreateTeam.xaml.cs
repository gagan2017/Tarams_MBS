﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class UserManagementCreateTeam : ContentPage
	{
		BaseCreateProjectAndGroup bc;

		//CreateProjectViewModel bc;
		public UserManagementCreateTeam(BaseCreateProjectAndGroup bindingContext)
		{
			bc = bindingContext;
			BindingContext = bc;
			InitializeComponent();
			dateTime.IsVisible = bc.IsTimeDateVissible;
			CustomCheckbox.InvokeCheckBoxClicked+= CustomCheckbox_InvokeCheckBoxClicked;
		}

		void CustomCheckbox_InvokeCheckBoxClicked(object sender, EventArgs e)
		{
			var sen = int.Parse((string)sender);
			bc.setUserById(sen);
		}

		async void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{

			listItems.IsRefreshing = true;
			var list = await bc.GetUsers(new MathrubhumiSales.Api.Models.SearchModel()
			{
				SearchText = ((SearchBar)sender).Text
			});
			listItems.ItemsSource = list;
			listItems.IsRefreshing = false;
			if (list.Count == 0)
			{
				return;
			}


		}
		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as UserModel;

			if (e == null) return; // has been set to null, do not 'process' tapped event

			bc.SetUsers(v);


			((ListView)sender).SelectedItem = null; // de-select the row

		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{

			if (groupname.Text.Trim().Length == 0)
			{
				DisplayAlert("error", "Enter Name", "ok!");
				return;
			}
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await bc.PostData(groupname.Text, dateTime.date);
			DependencyService.Get<ICustomPrgressBar>().Stop();
			DisplayAlert("Message", "Created successfully", "ok!");
			Navigation.RemovePage(this);
		}
	}
}
