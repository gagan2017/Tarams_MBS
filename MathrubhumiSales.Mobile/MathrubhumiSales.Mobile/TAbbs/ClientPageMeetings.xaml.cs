﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientPageMeetings : ContentView
	{
		ClientPageMeetingsViewModel bc;
		public ClientPageMeetings(List<CommentsRedItem> pRed)
		{
			bc = new ClientPageMeetingsViewModel(pRed)
			{

			};
			BindingContext = bc;
			InitializeComponent();
		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			var v = e.SelectedItem;
			if (v == null) return;
			((ListView)sender).SelectedItem = null;
		}
	}
}
