﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientPageProposals : ContentView
	{
		public static event EventHandler OptionsTapped;
		CommentsRedItem item;
		public static void optionsTapped(int proposalid, int meetingid)
		{
			Dictionary<string, int> obj = new Dictionary<string, int>();
			obj.Add("ProposalId", proposalid);
			obj.Add("MetingId", meetingid);
			OptionsTapped.Invoke(obj, new EventArgs());
		}
		ClientPageProposalsViewModel bc;

		public ClientPageProposals(List<CommentsRedItem> p)
		{
			bc = new ClientPageProposalsViewModel(p);
			BindingContext = bc;
			InitializeComponent();
			OptionsTapped += ClientPageProposals_OptionsTapped;
			popup.BackgroundColor = new Color(255, 255, 255, 0.88);

		}

		internal void Refresh(List<CommentsRedItem> list)
		{
			bc.Refresh(list);
		}

		async void ClientPageProposals_OptionsTapped(object sender, EventArgs e)
		{
			var sen = sender as Dictionary<string, int>;
			item = bc.GetElements(sen["ProposalId"], sen["MetingId"]);
			setPopUpVissible(true);
			cost.Text = "Rs " + item.m.Proposal.Amount;
			time.Text = FactoryDate._instance.getdateTime(item.m.Proposal.DateAndTime);
		}

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			if (popup.IsVisible)
			{
				setPopUpVissible(false);

				((ListView)sender).SelectedItem = null;
				return;
			}
			var v = e.SelectedItem;
			if (v == null) return;
			((ListView)sender).SelectedItem = null;

		}

		async void Handle_Cancel_Clicked(object sender, System.EventArgs e)
		{
			if (item == null) throw new Exception();
			item.m.DealStatus = Shared.Entities.Enums.DealStatus.Cancelled;
			await UpdateDB(item);

			setPopUpVissible(false);

		}

		async Task UpdateDB(CommentsRedItem item)
		{
			await new ApiMeeting().UpDateData(item.m);
			await ClientPage.refresh();
		}

		void setPopUpVissible(bool v)
		{
			popup.IsVisible = v;
		}

		async void Handle_Close_Clicked(object sender, System.EventArgs e)
		{
			if (item == null) throw new Exception();
			item.m.DealStatus = Shared.Entities.Enums.DealStatus.Closed;
			await UpdateDB(item);
			setPopUpVissible(false);
		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			setPopUpVissible(false);
		}
	}
}
