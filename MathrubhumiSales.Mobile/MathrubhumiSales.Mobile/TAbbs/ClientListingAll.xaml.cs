using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ClientListingAll : ContentView
	{

		ClientLisitingViewModel bc;
		public ClientListingAll(ClientLisitingViewModel bc)
		{
			this.bc = bc;
			BindingContext = bc;
			InitializeComponent();
			listview.ItemTemplate = new ClientListingDataTemplate();

		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as ClientListingModel;
			if (e == null) return; 
			Navigation.PushAsync(new ClientPage(null,v));
			((ListView)sender).SelectedItem = null; // de-select the
		}

		internal void Refresh()
		{
			
		}
	}
}
