﻿using System;
using System.Collections.Generic;

using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Class;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class AddAgencyPage : ContentPage
	{
		protected override void OnAppearing()
		{
			base.OnAppearing();
			model.refresh();

		}

		AddAgencyPageViewModel model;
		public AddAgencyPage()
		{
			model = new AddAgencyPageViewModel();
			BindingContext = model;
			InitializeComponent();
			list.ItemsSource = model.AgencyContacts;
		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			Navigation.PushAsync(new AddAgencieContactPage(model.model));
			DependencyService.Get<ICustomPrgressBar>().Stop();
		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
				Agency,Branch
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			model.model.Name = Agency.Text;
			model.model.Branch = Branch.Text;

			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await new ApiAgencies().Post(model.model);
			DependencyService.Get<ICustomPrgressBar>().Stop();
			await DisplayAlert("Completed", "Agency added", "Ok");
			Navigation.PopAsync();
		}
	}
}
