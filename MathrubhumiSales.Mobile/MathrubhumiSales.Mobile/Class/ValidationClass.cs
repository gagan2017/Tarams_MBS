﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile.Class
{
    class ValidationClass
    {
        public static ValidationClass _instance {get { return new ValidationClass(); }}
        private ValidationClass()
        {
                
        }
        public bool Validate(List<Entry> test)
        {
            foreach (var item in test)
            {
                if (item.Text==null || item.Text.Trim().Length == 0)
                {
                    return true;
                }
            }
            return false;
        }
        public bool EmailValidation(string email)
        {

          
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
                return false;
            else
                return true; 
        }
        public bool PhnoValidation(string phno)
        {
            if (phno.Trim().Length != 10)
            {
                return true;
            }
            return false;
        }
    }
}
