﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AddStyles
	{
		Label prevLabel;
		public AddStyles()
		{

		}
		public void setLabelClckedStyle(Label sender)
		{
			sender.BackgroundColor = Color.White;
			if (prevLabel != null && prevLabel != sender)
			{
				prevLabel.BackgroundColor = Color.Transparent;
			}
			prevLabel = sender;
		}
		public void setLabelClckedStyle(Label sender, List<Label> frameList)
		{
			foreach (var item in frameList)
			{
				item.BackgroundColor = Color.FromHex("#D3D3D3");
			}
			sender.BackgroundColor = Color.White;
		}
		public void setClckedStyle(Frame sender, List<Frame> frameList)
		{
			foreach (var item in frameList)
			{
				item.BackgroundColor = Color.FromHex("#D3D3D3");
			}
			sender.BackgroundColor = Color.White;
		}
	}
}

