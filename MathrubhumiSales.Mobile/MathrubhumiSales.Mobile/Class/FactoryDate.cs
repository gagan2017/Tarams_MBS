using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class FactoryDate
	{
		Dictionary<int, string> datetime;
		public static FactoryDate _instance { get { return new FactoryDate(); } }
		private FactoryDate()
		{
			datetime = new Dictionary<int, string>();
			datetime.Add(1, "Jan");
			datetime.Add(2, "Feb");
			datetime.Add(3, "Mar");
			datetime.Add(4, "Apr");
			datetime.Add(5, "May");
			datetime.Add(6, "Jun");
			datetime.Add(7, "Jul");
			datetime.Add(8, "Aug");
			datetime.Add(9, "Sep");
			datetime.Add(10, "Oct");
			datetime.Add(11, "Nov");
			datetime.Add(12, "Dec");

		}
		public string GetMonth(DateTime d)
		{
			return d.Day + " " + datetime[d.Month] + " " + d.Year;
		}
		public string getTime(DateTime dTime)
		{
			if (dTime.Hour > 12)
			{
				return (dTime.Hour - 12) + ":" + dTime.Minute + " PM";
			}
			return dTime.Hour + ":" + dTime.Minute + " Am";
		}
		public string getTime(TimeSpan dTime)
		{
			if (dTime.Hours > 12)
			{
				return (dTime.Hours - 12) + ":" + dTime.Minutes + " PM";
			}
			return dTime.Hours + ":" + dTime.Minutes + " Am";
		}
		string validdate(DateTime date)
		{
			if (date.Month == 2 && date.Year % 4 == 0)
			{
				if (date.Day == 28)
					return "29/Feb" + " " + date.Year;
				if (date.Day == 29)
				{
					return "1 Mar " + " " + date.Year;
				}
			}
			if (date.Month == 2 && date.Year % 4 != 0)
			{
				if (date.Day == 28)
					return "1 Mar " + " " + date.Year;
			}
			if (date.Month == 1 || date.Month == 3 || date.Month == 5 || date.Month == 7 || date.Month == 8 || date.Month == 10 || date.Month == 12)
			{
				if (date.Month == 12)
				{
					return "1 Jan " + date.Year + 1;
				}
				return "1 " + datetime[date.Month + 1] + " " + date.Year;
			}
			return date.Day + 1 + " " + datetime[date.Month] + " " + date.Year;
		}
		internal string getTommorrowDate()
		{
			var str = "";
			str = this.validdate(DateTime.Now);

			return str;
		}

		public string getTime(DateTime CTime, DateTime PTime)
		{
			string min = "", hour = "";
			if (CTime.DayOfYear == PTime.DayOfYear)
			{
				if (CTime.TimeOfDay.Hours == PTime.TimeOfDay.Hours)
				{
					return ((CTime.TimeOfDay.Minutes - PTime.TimeOfDay.Minutes) + " min ago");
				}
				else
				{
					return ((CTime.TimeOfDay.Hours - PTime.TimeOfDay.Hours) + " Hours ago");
				}
			}
			else
			{
				return ((CTime.DayOfYear - PTime.DayOfYear) + " days ago");
			}
			return "Time date not set properly";
		}

		internal string getDate(DateTime date)
		{
			return date.Day + "/" + date.Month + "/" + date.Year;
		}
		internal string getDateMonth(DateTime date)
		{
			return date.Day + "/" + date.Month + "/" + date.Year;
		}
		internal string getdateTime(DateTime getMeetingTime)
		{
			return getDate(getMeetingTime.Date) + " at " + getTime(getMeetingTime.TimeOfDay);
		}
	}
}
