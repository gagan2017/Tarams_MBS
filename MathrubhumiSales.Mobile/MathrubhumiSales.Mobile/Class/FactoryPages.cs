﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class FactoryPages
	{
		static FactoryPages instance { get; set; }

		ContentPage ManageUsersPage,GoogleSignIn, HomePage, Agencies, AddTask, CreateClient, ScheduleMeeting, FilterInLandingPage, ProfilePage, ClientListing, ClientFilter, UserManagementCreateTeam, ProjectPage, ErrorPage;
		CarouselPage MainPage;
		public static FactoryPages _instance { get { if (instance == null) instance = new FactoryPages(); return instance; } }
		private FactoryPages()
		{
		}
		public ContentPage GetPage(string s)
		{
			switch (s)
			{
				case "Manage Users": if (ManageUsersPage == null) ManageUsersPage = new ManageUsersPage(); return ManageUsersPage; break;
				case "Agencies": if (Agencies == null) Agencies = new Agencies(); return Agencies; break;
				case "Add Task": if (AddTask == null) AddTask = new AddTask(); return AddTask; break;  //return new AddTask(); break;
				case "Add Client": if (CreateClient == null) CreateClient = new CreateClient(); return CreateClient; break; //return new CreateClient(); break;
				case "Schedule Meeting": if (ScheduleMeeting == null) ScheduleMeeting = new ScheduleMeeting(); return ScheduleMeeting; break; //return new ScheduleMeeting(); break;
				case "FilterInLandingPage": if (FilterInLandingPage == null) FilterInLandingPage = new FilterInLandingPage(); return FilterInLandingPage; break; //return new FilterInLandingPage(); break;
				case "Profile Page": if (ProfilePage == null) ProfilePage = new ProfilePage(); return ProfilePage; break; //return new ProfilePage(); break;
				case "Clients": if (ClientListing == null) ClientListing = new ClientListing(); return ClientListing; break; //return new ClientListing(); break;
				case "ClientListingFilter": if (ClientFilter == null) ClientFilter = new ClientFilter(); return ClientFilter; break; //return new ClientFilter(); break;
				case "Projects": if (ProjectPage == null) ProjectPage = new ProjectPage(); return ProjectPage; break; //return new ProjectPage(); break;
				case "Create Project": if (UserManagementCreateTeam == null) UserManagementCreateTeam = new UserManagementCreateTeam(new CreateProjectViewModel()); return UserManagementCreateTeam; break; //return new UserManagementCreateTeam(new CreateProjectViewModel()); break;
				case "Teams": if (CreateClient == null) CreateClient = new CreateClient(); return CreateClient; break; //return new CreateClient(); break;

				case "HomePage": if (HomePage == null) HomePage = new HomePage(); return HomePage; break; //return new CreateClient(); break;
				case "GoogleSignIn": if (GoogleSignIn == null) GoogleSignIn = new GoogleSignIn(); return GoogleSignIn; break; //return new CreateClient(); break;

				default: if (ErrorPage == null) ErrorPage = new ErrorPage(); return ErrorPage; break; //return new ErrorPage();
			}
			return null;
		}
		public CarouselPage GetCaroselPage(string s)
		{
			switch (s)
			{
				case "MainPage": if (MainPage == null) MainPage = new MainPage(); return MainPage; break; //return new CreateClient(); break;
				default: if (ErrorPage == null) ErrorPage = new ErrorPage(); return null; break; //return new ErrorPage();

			}
		}
	}
}
