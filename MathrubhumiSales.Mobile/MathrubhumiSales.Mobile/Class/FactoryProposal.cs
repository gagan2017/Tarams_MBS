﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class FactoryProposal
	{
		public static FactoryProposal _instance
		{
			get
			{
				return new FactoryProposal();
			}
		}
		List<ProposalModal> listitems { get; set; }
		private FactoryProposal()
		{
			listitems = new List<ProposalModal>();
			listitems.Add(new ProposalModal()
			{
				EventName = "New Year Bash",
				Rate = 1500000,
				EDate = DateTime.Today,
				DealClosed = true,
				DDate = DateTime.Today
			});
			listitems.Add(new ProposalModal()
			{
				EventName = "Cristmas",
				Rate = 1500000,
				EDate = DateTime.Now,
				DealClosed = false,
				DDate = DateTime.Now
			});

		}
		public List<ProposalModal> getList()
		{
			return listitems;
		}
	}
}
