using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Model;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class Factory
	{
		Dictionary<string, ContentView> pages;

		FilterContent FilterContent;
		ClientPageMeetings Meetings;
		ClientPageProposals Proposals;
		ErrorContent ErrorContent;
		ClientListingAll ClientListingAll;
		int i = -1;

		internal void setEventHandler()
		{

		}

		internal void GiveRefresh(object obj = null)
		{
			switch (i)
			{
				case 1:
					DummyListItemViewModel.Instance.Refresh();
					break;
				case 2:
					this.Proposals.Refresh(obj as List<CommentsRedItem>);
					break;
				case 3:
				ClientListingAll.Refresh();
					break;

			}
		}

		public Factory(List<CommentsRedItem> p_RedItem)
		{
			i = 2;
			pages = new Dictionary<string, ContentView>();
			Meetings = new ClientPageMeetings(p_RedItem);
			Proposals = new ClientPageProposals(p_RedItem);
			pages.Add("Meetings", Meetings);
			pages.Add("Proposals", Proposals);
			pages.Add("Minutes", new ErrorContent());
			pages.Add("Attachments", new ErrorContent());
		}

		internal ContentView getContent(string text)
		{
			return pages[text];
		}

	}
}
