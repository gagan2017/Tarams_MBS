﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossPieCharts.FormsPlugin.Abstractions;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class MainPage : CarouselPage
	{
		bool isFirst = true;
		AddStyles styles;
		TapGestureRecognizer EditBackTapped;
		public event EventHandler ListSwypedLeft;
		EditCardPage editcardPage { get; set; }

		LandingPageViewModel bind;


		public MainPage()
		{
			NavigationPage.SetHasNavigationBar(this, false);
			ListSwypedLeft += MainPage_ListSwypedLeft;
			bind = new LandingPageViewModel();

			bind.StartLoader += (sender, e) => DependencyService.Get<ICustomPrgressBar>().Start(0);
			bind.StopLoader += (sender, e) => DependencyService.Get<ICustomPrgressBar>().Stop();

			BindingContext = bind;
			InitializeComponent();
			editcardPage = new EditCardPage();
			TaskMeetingListView.ItemTemplate = DummyListItemViewModel.Instance.itemTemplate;
			TaskMeetingListView.IsPullToRefreshEnabled = true;
			TaskMeetingListView.Refreshing += TaskMeetingListView_Refreshing;
			TaskMeetingListView.ItemSwypeLeft += TaskMeetingListView_ItemSwypeLeft;
			TaskMeetingListView.TaskItemTapped += TaskMeetingListView_TaskItemTapped; ;
			bind.GetData();
			bind.TodayItems.CollectionChanged += (sender, e) => { ContentIndicator.IsVisible = false; };
			styles = new AddStyles();
			styles.setLabelClckedStyle(one);
			popup.BackgroundColor = new Color(255, 255, 255, 0.95);
			staticItemsource.IsVisible = false;
			styles.setLabelClckedStyle(one);
			EditBackTapped = new TapGestureRecognizer();
			EditBackTapped.Tapped += EditBackTapped_Tapped;
			EditBackTapped.NumberOfTapsRequired = 1;
			editcardPage.EditBack.GestureRecognizers.Add(EditBackTapped);
			this.CurrentPageChanged += Handle_CurrentPageChanged;
			profilePic.Source = "Demoprofilepic.png";


		}

		void MainPage_ListSwypedLeft(object sender, EventArgs e)
		{
			Children.Add(editcardPage);
			CurrentPage = editcardPage;
		}

		void EditBackTapped_Tapped(object sender, EventArgs e)
		{

			this.Children.Remove(editcardPage);
			this.CurrentPage = mainPage;
		}



		async Task PushPage(string listname)
		{
			if (listname == "Teams" || listname == "FilterInLandingPage")
			{
				return;
			}
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			
			indicator.IsEnabled = true;
			var pag = FactoryPages._instance.GetPage(listname);
			NavigationPage.SetHasNavigationBar(pag, false);
			await Navigation.PushAsync(pag);
			NavigationPage.SetHasNavigationBar(pag, true);
			indicator.IsEnabled = false;
			DependencyService.Get<ICustomPrgressBar>().Stop();

		}

		void OnClick(object sender, System.EventArgs e)
		{

		}
		async void Handle_SingleTap(object sender, System.EventArgs e)
		{
			await PushPage("Profile Page");
		}
		bool IsEnabled = true;
		async private void listView1_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var v = e.Item as ListItems;
			if (e == null) return;
			// has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null;
			if (IsEnabled)
			{
				IsEnabled = false;
				await PushPage(v.Listname);
				IsEnabled = true;
			}
			// de-select the ro   }
		}
		void Handle_Tapped(object sender, System.EventArgs e)
		{
			ContentIndicator.IsVisible = true;
			if ((sender as Label) == null) return;
			styles.setLabelClckedStyle(sender as Label);
			var set = ((Label)sender).Text;
			if (set == bind.CurrentItems)
			{
				ContentIndicator.IsVisible = false;
				return;
			}
			TaskMeetingListView.ItemsSource = bind.setData(set);

			if (set == "All")
			{
				staticItemsource.IsVisible = true;

			}
			else
			{
				staticItemsource.IsVisible = false;
			}
			ContentIndicator.IsVisible = false;
			//temp = factorypage.getContent(((Label)sender).Text);
			//content.Children.Add(temp);
		}
		async void TaskMeetingListView_Refreshing(object sender, EventArgs e)
		{
			await DummyListItemViewModel.Instance.GetData();
			TaskMeetingListView.IsRefreshing = false;
		}

		void TaskMeetingListView_ItemSwypeLeft(object sender, EventArgs e)
		{
			ListSwypedLeft.Invoke(sender, e);
		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			if (e == null) return;
			var v = e.Item as ListItemModel;
			((ListView)sender).SelectedItem = null; //
								// has been set to null, do not 'process' tapped event
			if (v.Heading != null)
			{
				((ListView)sender).SelectedItem = null;
				return;
			}

			if (v._blueItem == null)
			{
				OnRedItemTapped(v._redItem);//
			}
			else
			{
				OnBlueItemTapped(v._blueItem);
			}

		}

		void TaskMeetingListView_TaskItemTapped(object sender, EventArgs e)
		{
			var v = sender as Label;
			OnBlueItemTapped(DummyListItemViewModel.Instance.getBlueItem(int.Parse(v.Text)));
		}

		async void OnBlueItemTapped(CommentsBlueItem _blueItem)
		{
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await Navigation.PushAsync(new ToDoDetailPage2(_blueItem));
			DependencyService.Get<ICustomPrgressBar>().Stop();
		}

		async void OnRedItemTapped(CommentsRedItem _redItem)
		{
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await Navigation.PushAsync(new ClientPage(_redItem, null));
			DependencyService.Get<ICustomPrgressBar>().Stop();
		}

		void Handle_Static_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			if (e == null) return;
			((ListView)sender).SelectedItem = null; //
		}
		async void Handle_PopUp_Tapped(object sender, System.EventArgs e)
		{
			await PushPage(((Label)sender).Text);
		}

		void Handle_FilterTapped(object sender, System.EventArgs e)
		{

		}
		void AddTapped_Tapped(object sender, EventArgs e)
		{
			PopUpVissible(!popup.IsVisible);
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			PopUpVissible(false);
			bind.Refresh();

			/*if (factorypage != null) 
				factorypage.GiveRefresh();*/

		}
		protected override bool OnBackButtonPressed()
		{
			if (editcardPage.isPageVissible)
			{
				this.CurrentPage = mainPage;
				Children.Remove(editcardPage);
				return true;
			}
			else
			{
				if (PopUpVissible(!popup.IsVisible))
				{
					return true;
				}
			}

			return base.OnBackButtonPressed();
		}
		bool PopUpVissible(bool val)
		{
			if (val)
			{
				addbtn.RotateTo(45);
			}
			else
			{
				addbtn.RotateTo(0);
			}
			popup.IsVisible = val;
			return !val;

		}
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			PopUpVissible(false);
		}
		void Handle_CurrentPageChanged(object sender, EventArgs e)
		{
			if (CurrentPage == editcardPage)
			{

			}
			else
			{
				if (Children.Contains(editcardPage))
				{
					Children.Remove(editcardPage);
				}

			}
		}
	}
}
