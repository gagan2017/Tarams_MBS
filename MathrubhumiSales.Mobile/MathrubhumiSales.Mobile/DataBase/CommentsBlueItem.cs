﻿using System;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CommentsBlueItem
	{
		public string Heading { get; set; }
		public TaskModel task { get; set; }
		public string Description { get; set; }
		public string Time { get { return FactoryDate._instance.getTime(UpdateTime); } }
		public DateTime UpdateTime { get; set; }
		public string Messagesrc { get; set; }
		public string Type { get; set; }
		public User By { get; set; }

		public string RatingSource { get; set; }
		public string MinAgo { get; set; }
		public string Name { get; set; }
		public string StoreName { get; set; }
		public User Who { get; set; }
		public string Number { get; set; }
		public string NumberOfMessages { get; set; }
		public bool HasNotification { get; set; }

		public string Notificationsource
		{
			get
			{
				if (HasNotification)
				{
					return "RedNotification.png";
				}
				return "RedNotification.png";
			}
		}
		public CommentsBlueItem()
		{
		}
		public CommentsBlueItem(string s)
		{
			Heading = s;
			/*UpdateTime = DateTime.Now;
			Type = "Propossaal";
			Description = "I asdsadwill do that by the end of day today or early tomorrow";

			By = UserFactory._Instance.getUser(3);
			Who = UserFactory._Instance.getUser(2);
			Rating = "**";
			MinAgo = "10min ago";
			Number = "2";
			Name = "Patty";
			StoreName = "Kosh Store";
			NumberOfMessages = "2";*/
		}
		public CommentsBlueItem(TaskModel task)
		{

			this.task = task;
			this.Heading = task.TaskSummary;
			if (!(task.Comment == null || task.Comment.Trim().Length == 0))
			{
				Description = task.Comment;
			}

			UpdateTime = task.TaskDate;
			Type = task.Name;
			By = new User(task.User);
		
			if (task.IsHighPriority)
			{
				RatingSource = "Rating.png";
			}
			MinAgo = FactoryDate._instance.getTime(DateTime.Now, task.CreatedTime);

			Number = task.Teams.Count.ToString();

			if (task.NoOfComments > 0)
			{
				Messagesrc = "Message.png";
				NumberOfMessages = (task.NoOfComments).ToString();
			}
			Name = task.User.FirstName;
			if (task.Labels != null)
				StoreName = task.Labels.Name;
		}
	}
}
