﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class UserFactory
	{
		public static UserFactory _Instance { get { return new UserFactory(); } }
		List<User> listOfUsers { get; set; }
		private UserFactory()
		{


			listOfUsers = new List<User>();
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Rick Zeigler",
				Id = 1

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Jenny",
				Id = 2

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Cheryl",
				Id = 3

			});


			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Katie",
				Id = 4

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Julia",
				Id = 12

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "Sally",
				Id = 5

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "gaga",
				Id = 6

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "a",
				Id = 7

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "b",
				Id = 8

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "c",
				Id = 9

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "d",
				Id = 10

			});
			listOfUsers.Add(new User()
			{
				Profilepic = "Demoprofilepic.png",
				Name = "e",
				Id = 11

			});
		}

		public List<User> GetAll()
		{
			return listOfUsers;
		}

		public User getUser(int id)
		{
			return listOfUsers.Find((obj) => obj.Id == id);
		}
		public User getCurrentUser()
		{
			return listOfUsers[0];
		}
		public List<User> getListOfUser(int count)
		{
			return listOfUsers.FindAll((obj) => obj.Id < count);
		}
		public User getUser(string name)
		{
			return listOfUsers.Find((obj) => obj.Name == name);
		}
	}
}
