﻿using System;
using MathrubhumiSales.Shared.Entities.Enums;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class CommentsRedItem
	{
		public MeetingModel m;
		public bool HasNotification;
		public string Heading { get; set; }
		public bool Hasbell { get; set; }
		public bool HasStar { get; set; }
		public DateTime UpdateTime { get; set; }
		public string Description { get; set; }
		public string MTime { get; set; }
		public string Notificationsource
		{
			get
			{
				if (HasNotification)
				{
					return "RedNotification.png";
				}
				return "RedNotification.png";
			}
		}
		public string RatingSource
		{
			get; set;
		}
		public string Time

		{
			get
			{
				return FactoryDate._instance.getTime(UpdateTime);
			}

		}

		public string Type { get; set; }
		public User By { get; set; }
		public string Cause { get; set; }
		public string Rating { get; set; }
		public CommentsRedItem()
		{

			/*Heading = "Anchor India Pvt Ltd",
			Description = "Discussion regarding event and proposal at Madison Avenue",
			Time = "4:30 P.M",
			Type = "Negotiation",
			By = "John.D",
			Cause = "New Year Celeb",
			Rating="****"
Heading = "Anchor India Pvt Ltd",
				Description = "Discussion regarding event and proposal at Madison Avenue",
				UpdateTime = DateTime.Now,
				Type = "Negotiation",
				By = UserFactory._Instance.getUser(1),
				Cause = "New Year Celeb",
				Rating = "***",
			*/

		}
		public CommentsRedItem(MeetingModel m, string heading)
		{
			this.m = m;
			Heading = heading;
			if (!(m.Comment == null || m.Comment.Trim().Length == 0))
			{
				Description = m.Comment;
			}
			else
			{
				//Description = "No Coms";

			}
			UpdateTime = m.MeetingTime;
			if (m.Label != null)
				Cause = m.Label.Name;
			
			Type = new ClientCategoryConverter().Getstring(m.Client.Category);

			By = new User(m.User);
			if (m.IsHighPriority)
			{
				RatingSource = "Rating.png";
			}
		}


		public void Changedate(string s)
		{
			if (m.MeetingTime.DayOfYear == DateTime.Now.DayOfYear)
			{
				s = "Today " + s;
			}
			MTime = s;
		}
	}
}

