﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class User : Base
	{
		public string Profilepic { get; set; }
		public string Name { get; set; }
		public User()
		{
		}
		public User(UserModel u)
		{
			if (u != null)
			{
				Name = u.FirstName;
				Profilepic = "Demoprofilepic.png";
			}

		}
	}
}
