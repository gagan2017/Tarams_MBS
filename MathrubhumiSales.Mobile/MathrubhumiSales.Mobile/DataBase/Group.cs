﻿using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class Group :Base
	{
		public string GroupName { get; set; }
		public List<User> ListOfUsersinGroup { get; set; }

	}
}
