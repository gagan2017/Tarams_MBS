using System;
using System.Collections.Generic;

namespace MathrubhumiSales.Mobile
{
	public class GroupFactory
	{
		public static GroupFactory _instance { get { return new GroupFactory(); } }
		List<Group> listOfGroup { get; set; }
		List<User> tempUserlist { get; set; }
		public GroupFactory()
		{
			listOfGroup = new List<Group>();
			listOfGroup.Add(new Group()
			{
				Id = 1,
				GroupName = "Executive Team",
				ListOfUsersinGroup = UserFactory._Instance.getListOfUser(5)

			});
			listOfGroup.Add(new Group()
			{
				Id = 2,
				GroupName = "Mumbai Team",
				ListOfUsersinGroup = UserFactory._Instance.getListOfUser(3)

			});
			listOfGroup.Add(new Group()
			{
				Id = 3,
				GroupName = "Example",
				ListOfUsersinGroup = UserFactory._Instance.getListOfUser(9)

			});

		}
		public List<Group> GetAll()
		{
			return listOfGroup;
		}

		internal void AddGroup(string name, List<User> userlist)
		{
			tempUserlist = new List<User>();

			foreach (var item in userlist)
			{
				tempUserlist.Add(new User()
				{
					Name = item.Name,
					Profilepic = item.Profilepic,
					Id = item.Id
				});
			}
			listOfGroup.Add(new Group()
			{
				GroupName = name,
				ListOfUsersinGroup =tempUserlist
				});
		}
	}
}
