﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Model;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class CommentFactory
	{
		public static CommentFactory _instance { get { return new CommentFactory(); } }
		List<MeetingModel> meeting { get; set; }
		List<TaskModel> task { get; set; }
		List<CommentsRedItem> listOfRedItems;
		List<CommentsBlueItem> listOfBlueItems;

		Dictionary<int, List<CommentsRedItem>> listofredComments;
		Dictionary<int, List<CommentsBlueItem>> listofBlueComments;
		public CommentFactory(ClientListingModel model)
		{
			listOfRedItems = new List<CommentsRedItem>();
		}
		async internal Task<List<CommentsRedItem>> FindEleFromApi(int id)
		{
			listOfRedItems = new List<CommentsRedItem>();
			meeting = await new ApiMeeting().getAll(id);
			foreach (var item in meeting)
			{
				listOfRedItems.Add(new CommentsRedItem(item, item.Client.Name));
			}
			return listOfRedItems;
		}

		async internal Task<List<CommentsRedItem>> FindBlueEleFromApi()
		{
			listOfBlueItems = new List<CommentsBlueItem>();
			task = await new ApiTask().GetAll();

			foreach (var item in task)
			{
				listOfBlueItems.Add(new CommentsBlueItem(item));
			}
			return listOfRedItems;
		}
		public CommentFactory()
		{

		}

	}
}

