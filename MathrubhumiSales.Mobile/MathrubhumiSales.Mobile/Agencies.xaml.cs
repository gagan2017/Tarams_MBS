﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.ViewModels;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class Agencies : ContentPage
	{
		AgencyViewModel bc;

		public Agencies()
		{
			bc = new AgencyViewModel();
			BindingContext = bc;
			InitializeComponent();
			addbtn.GestureRecognizers.Add(new TapGestureRecognizer((obj) => Navigation.PushAsync(new AddAgencyPage())));
			listView.RowHeight = 100;
			listView.ItemsSource = bc.item;
			listView.IsPullToRefreshEnabled = true;
			listView.Refreshing += ListView_Refreshing;
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			bc.start();
		}
		async void ListView_Refreshing(object sender, EventArgs e)
		{
			await bc.GetDatasAsync();
			listView.IsRefreshing = false;
		}

		private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var sen = e.Item as AgencyModel;
			Navigation.PushAsync(new AgencyListing(sen));
			((ListView)sender).SelectedItem = null; // de-select the row

		}



		private void Filter_Activated(object sender, EventArgs e)
		{
			//Navigation.PushAsync(new FilterAgency());
		}

		private void Navigate_Activated(object sender, EventArgs e)
		{
		}
	}
}
