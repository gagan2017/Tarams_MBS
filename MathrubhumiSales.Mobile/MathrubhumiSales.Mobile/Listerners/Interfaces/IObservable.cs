﻿using System;
namespace MathrubhumiSales.Mobile
{
	public interface IObservable
	{
		void Add(IObserver iob);
		void Remove(IObserver iob);
		void Notify();
	}


}
