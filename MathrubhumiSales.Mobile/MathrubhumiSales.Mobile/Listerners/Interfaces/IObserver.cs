﻿using System;
namespace MathrubhumiSales.Mobile
{
	public interface IObserver
	{
		void Update();
	}
}
