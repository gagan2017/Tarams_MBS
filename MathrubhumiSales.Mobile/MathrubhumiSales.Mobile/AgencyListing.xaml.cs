﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class AgencyListing : ContentPage
	{
		AgecncyContactListViewModel bc;
		public AgencyModel model;
		public AgencyListing(AgencyModel mod)
		{

			InitializeComponent();
			bc = new AgecncyContactListViewModel(mod.Id);

			model = mod;
			addbtn.GestureRecognizers.Add(new TapGestureRecognizer((obj) => Navigation.PushAsync(new AddAgencieContactPage(mod))));
			agencyname.Text = model.Name;
			agencyplace.Text = model.Branch;
			listView2.HasUnevenRows = true;
			listView2.ItemsSource = bc.AgencyContacts;
			listView2.IsPullToRefreshEnabled = true;
			listView2.Refreshing+= ListView2_Refreshing;
			bc.Refresh();

		}

		async void ListView2_Refreshing(object sender, EventArgs e)
		{
			await bc.GetDatasAsync();
			listView2.IsRefreshing = false;
		}

		private void SearchAgency_Activated(object sender, EventArgs e)
		{

		}

		private void listView2_ItemTapped(object sender, ItemTappedEventArgs e)
		{

			if (e == null) return; // has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null; // de-select the row	}


		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (bc != null)
			{
				bc.Refresh();
			}
		}
	}
}