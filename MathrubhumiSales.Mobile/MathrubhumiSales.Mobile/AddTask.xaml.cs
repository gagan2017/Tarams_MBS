﻿using System;
using System.Collections.Generic;

using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Class;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class AddTask : ContentPage
	{

		bool starTapped = false;
		AddTaskModel AssigneeTaskBinding;



		public AddTask()
		{
			AssigneeTaskBinding = new AddTaskModel();
			BindingContext = AssigneeTaskBinding;
			InitializeComponent();
			AssigneeTaskBinding.GetLabel(Labelpicker);
			CustomCheckbox.InvokeCheckBoxClicked += CustomCheckbox_InvokeCheckBoxClicked;
		}

		void CustomCheckbox_InvokeCheckBoxClicked(object sender, EventArgs e)
		{
			//var sen = ((string)sender).Split(' ');
			string Id = ((string)sender);
			string Name = " ";              //sen[1];
			AssigneeTaskBinding.IdPresent(Id, Name);

		}

		private void Labelpicker_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Labelpicker.SelectedItem == null) return;
			if (AssigneeTaskBinding.setLabel(Labelpicker.SelectedItem as string))
			{
				return;
			};

		}
		void Handle_Tapped(object sender, System.EventArgs e)
		{
			var sen = (Image)sender;
			if (starTapped)
			{
				AssigneeTaskBinding.taskModel.IsHighPriority = false;
				sen.Source = "StarNotSelected.png";
				starTapped = false;
				return;
			}
			sen.Source = "StarSelected.png";
			starTapped = true;
			AssigneeTaskBinding.taskModel.IsHighPriority = true;
		}

		async void AddLabel_Clicked(object sender, EventArgs e)
		{
			if (Comments.Text == null || Comments.Text.Trim().Length == 0)
			{
				AssigneeTaskBinding.taskModel.Comment = " ";
			}
			else
			{
				AssigneeTaskBinding.taskModel.Comment = Comments.Text;
			}
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
					TaskSummary
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			/*if (Labelpicker.SelectedItem == null)
			{
				await DisplayAlert("Error", "Select label", "Ok!");
				return;
			}*/
			AssigneeTaskBinding.taskModel.TaskDate = dateTimePicker.date;
			AssigneeTaskBinding.taskModel.TaskSummary = TaskSummary.Text;

			AssigneeTaskBinding.taskModel.User = new UserModel()
			{
				AuthModel = new AuthModel()
				{
					Email = Sessions.instance.CurrentUser.AuthModel.Email,
					GmailId = Sessions.instance.CurrentUser.AuthModel.GmailId,
					Token = Sessions.instance.CurrentUser.AuthModel.Token,
				},
				ContactNumber = Sessions.instance.CurrentUser.ContactNumber,
				Designation = new Models.DesignationModel()
				{
					Id = Sessions.instance.CurrentUser.Designation.Id,
					Name = Sessions.instance.CurrentUser.Designation.Name
				},
				Id = Sessions.instance.CurrentUser.Id,
				FirstName = Sessions.instance.CurrentUser.FirstName,
				LastName = Sessions.instance.CurrentUser.LastName,
				IsDeleted = Sessions.instance.CurrentUser.IsDeleted,
				ProfilePicUrl = Sessions.instance.CurrentUser.ProfilePicUrl

			}; ;
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			var res = await new ApiTask().Post(AssigneeTaskBinding.taskModel);
			DependencyService.Get<ICustomPrgressBar>().Stop();
			if (res)
			{
				await DisplayAlert("sucess", "Task added successful", "Ok!");
				await this.Navigation.PopAsync();
			}
			else
			{
				await DisplayAlert("error", "Cannot add Task", "Try!");
			}
		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{

			var val = e.Item as Assignee;
			if (val == null) return;
			if (val.isChecked == true)
			{
				val.isChecked = false;
			}
			else
			{
				val.isChecked = true;
			}
			AssigneeTaskBinding.setSelectedItem(val);

			listView3.ItemsSource = AssigneeTaskBinding.taskAsigneeDetails;
			((ListView)sender).SelectedItem = null;

		}

		async void Handle_SearchButtonPressed(object sender, System.EventArgs e)
		{
			if (((SearchBar)sender).Text.Trim().Length == 0)
			{
				DisplayAlert("Error", "Specify User Name toSearch", "ok");
				return;
			}
			listView3.IsRefreshing = true;
			var list = await AssigneeTaskBinding.getList(new MathrubhumiSales.Api.Models.SearchModel()
			{
				SearchText = ((SearchBar)sender).Text
			});
			listView3.ItemsSource = list;
			listView3.IsRefreshing = false;
			if (list.Count == 0)
			{
				DisplayAlert("No results", "Search not found", "ok");
				return;
			}
		}

		async void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			listView3.IsRefreshing = true;
			var list = await AssigneeTaskBinding.getList(new MathrubhumiSales.Api.Models.SearchModel()
			{
				SearchText = ((SearchBar)sender).Text
			});
			listView3.ItemsSource = list;
			listView3.IsRefreshing = false;
			if (list.Count == 0)
			{
				DisplayAlert("No results", "Search not found", "ok");
				return;
			}
		}

	}
}
