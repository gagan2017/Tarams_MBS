﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class GoogleSignIn : ContentPage
	{
		Xamarin.Auth.OAuth2Authenticator authenticator = null;
		public GoogleSignIn()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);

		}
		void Handle_Clicked(object sender, System.EventArgs e)
		{
			signIn.IsEnabled = false;
			authenticator
			 = new Xamarin.Auth.OAuth2Authenticator
				 (
					 clientId:
						 new Func<string>
							(
								 () =>
								 {
									 string retval_client_id = "oops something is wrong!";

									 // some people are sending the same AppID for google and other providers
									 // not sure, but google (and others) might check AppID for Native/Installed apps
									 // Android and iOS against UserAgent in request from 
									 // CustomTabs and SFSafariViewContorller
									 // TODO: send deliberately wrong AppID and note behaviour for the future
									 // fitbit does not care - server side setup is quite liberal
									 switch (Xamarin.Forms.Device.RuntimePlatform)
									 {
										 case "Android":
											 retval_client_id = "1093596514437-d3rpjj7clslhdg3uv365qpodsl5tq4fn.apps.googleusercontent.com";
											 break;
										 case "iOS":
											 retval_client_id = "1093596514437-cajdhnien8cpenof8rrdlphdrboo56jh.apps.googleusercontent.com";
											 break;
										 case "Windows":
											 retval_client_id = "1093596514437-t7ocfv5tqaskkd53llpfi3dtdvk4t35h.apps.googleusercontent.com";
											 break;
									 }
									 return retval_client_id;
								 }
						   ).Invoke(),
					 clientSecret: null,   // null or ""
					 authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
					 accessTokenUrl: new Uri("https://www.googleapis.com/oauth2/v4/token"),
					 redirectUrl:
						 new Func<Uri>
							(
								 () =>
								 {

									 string uri = null;

									 // some people are sending the same AppID for google and other providers
									 // not sure, but google (and others) might check AppID for Native/Installed apps
									 // Android and iOS against UserAgent in request from 
									 // CustomTabs and SFSafariViewContorller
									 // TODO: send deliberately wrong AppID and note behaviour for the future
									 // fitbit does not care - server side setup is quite liberal
									 switch (Xamarin.Forms.Device.RuntimePlatform)
									 {
										 case "Android":
											 uri =
												 "com.xamarin.traditional.standard.samples.oauth.providers.android:/oauth2redirect"
												 //"com.googleusercontent.apps.1093596514437-d3rpjj7clslhdg3uv365qpodsl5tq4fn:/oauth2redirect"
												 ;
											 break;
										 case "iOS":
											 uri =
												 "com.xamarin.traditional.standard.samples.oauth.providers.ios:/oauth2redirect"
												 //"com.googleusercontent.apps.1093596514437-cajdhnien8cpenof8rrdlphdrboo56jh:/oauth2redirect"
												 ;
											 break;
										 case "Windows":
											 uri =
												 "com.xamarin.auth.windows:/oauth2redirect"
												 //"com.googleusercontent.apps.1093596514437-cajdhnien8cpenof8rrdlphdrboo56jh:/oauth2redirect"
												 ;
											 break;
									 }

									 return new Uri(uri);
								 }
							 ).Invoke(),
					 scope:
								  //"profile"
								  "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.login"
								  ,
					 getUsernameAsync: null,
							  isUsingNativeUI: true
				 )
			 {
				 AllowCancel = true,
			 };

			authenticator.Completed +=
				async (s, ea) =>
					{

						StringBuilder sb = new StringBuilder();

						if (ea.Account != null && ea.Account.Properties != null)
						{
							sb.Append("Token = ").AppendLine($"{ea.Account.Properties["access_token"]}");
						}
						else
						{
							sb.Append("Not authenticated ").AppendLine($"Account.Properties does not exist");
							DisplayAlert("Authentication Results", sb.ToString(), "OK");
							signIn.IsEnabled = true;
						}
						var UserModel = await new ApiAuthModel().Post(new Shared.Models.AuthModel()
						{
							GmailId = ea.Account.Properties["id_token"]
						});
						if (UserModel == null||UserModel.Id == -1)
						{
							DisplayAlert("Error Loggin In", "User Doesnt exist Try Again", "Try Again");
							errText.IsVisible = true;
							await Navigation.PopModalAsync();
							signIn.IsEnabled = true;
							return;
						}
						Sessions.instance.AccessToken = UserModel.AuthModel.Token;
						Sessions.instance.CurrentUser = UserModel;

						await Navigation.PushAsync(new HomePage());

						Navigation.RemovePage(this);
						return;
					};

			authenticator.Error +=
				(s, ea) =>
					{
						signIn.IsEnabled = true;
						StringBuilder sb = new StringBuilder();
						sb.Append("Error = ").AppendLine($"{ea.Message}");


						DisplayAlert
								(
									"Authentication Error",
									sb.ToString(),
									"OK"
								);
						return;
					};

			// after initialization (creation and event subscribing) exposing local object 
			AuthenticationState.Authenticator = authenticator;


			PresentUILoginScreen(authenticator);
		}
		void PresentUILoginScreen(Xamarin.Auth.OAuth2Authenticator authenticator)
		{

			Xamarin.Auth.XamarinForms.AuthenticatorPage ap;
			ap = new Xamarin.Auth.XamarinForms.AuthenticatorPage()
			{
				Authenticator = authenticator,
			};

			NavigationPage np = new NavigationPage(ap);

			Navigation.PushModalAsync(np);

		}
	}
}
