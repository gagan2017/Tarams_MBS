﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class HomePage : ContentPage
	{
		HomePageViewModel vmodel { get; set; }
		TapGestureRecognizer ontapped;
		protected override void OnAppearing()
		{
			base.OnAppearing();

			//ApiAuthenticate.Instance.Post(Sessions.instance.getId().AuthModel);
		}
		public HomePage()
		{

			InitializeComponent();
			vmodel = new HomePageViewModel();
			NavigationPage.SetHasNavigationBar(this, false);
			//model.Quote = "You have to grow from  the inside out. None can teach you , none can  make you spiritual. There is no other teacher but your own soul.";
			//model.By = "-Swamy vivekananda";
			vmodel.start(this.Lblthought);
			ontapped = new TapGestureRecognizer();
			ontapped.Tapped += Handle_Clicked;
			ontapped.NumberOfTapsRequired = 1;
			imgLike.GestureRecognizers.Add(ontapped);


		}


		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			await Navigation.PushAsync(FactoryPages._instance.GetCaroselPage("MainPage"));
			Navigation.RemovePage(this);
			Sessions.instance.GetDisplay = DateTime.Now.AddDays(1).DayOfYear;
			DependencyService.Get<ICustomPrgressBar>().Stop();
		}
	}
}
