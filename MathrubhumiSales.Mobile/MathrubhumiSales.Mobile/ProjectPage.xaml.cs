﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class ProjectPage : ContentPage
	{
		ProjectsViewModel model;
		AddStyles style;
		ContentView temp;
		UserManagementCreateTeam AddProjectPage;
		protected override void OnAppearing()
		{
			base.OnAppearing();
			model.Refresh();
		}
		public ProjectPage()
		{
			model = new ProjectsViewModel();
			InitializeComponent();
			AddProjectPage = new UserManagementCreateTeam(new CreateProjectViewModel());
			listItems.ItemsSource = model.onGoingProjects;
			style = new AddStyles();
			style.setLabelClckedStyle(OnGoing);
			model.Updated += Model_Updated;
		}

		async void Model_Updated(object sender, EventArgs e)
		{
			if ((sender as ProjectsViewModel).onGoingProjects.Count == 0)
			{
				if ((sender as ProjectsViewModel).completedProjects.Count == 0)
				{
					var res = await DisplayAlert("Info.", "No Project Exist create new Project", "+ Add", "later");
					if (res)
					{
						Navigation.PushAsync(AddProjectPage);
					}
					return;
				}
				style.setLabelClckedStyle(Completed);
				listItems.ItemsSource = model.completedProjects;
			}

		}

		void Handle_Tapped(object sender, System.EventArgs e)
		{
			var sen = ((Label)sender);
			style.setLabelClckedStyle(sen);
			if (sen.Id == Completed.Id)
			{
				listItems.ItemsSource = model.completedProjects;
			}
			else
			{
				listItems.ItemsSource = model.onGoingProjects;
			}


		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v = e.Item as ListItems;
			if (e == null) return; // has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null;
		}
		void Handle__AddImage_Tapped(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(AddProjectPage);
		}
	}
}
