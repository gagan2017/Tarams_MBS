﻿using System;
namespace MathrubhumiSales.Mobile
{
	enum Status
	{
		Increase, Decrease
	}
	public class Percentage
	{
		public string PercentageTook { get; set; }
		bool status { get; set; }
		public bool StatusGained { get { return status; } 
			set {status=value; if (value) statusimage = "increase.png"; else statusimage = "decrease.png";
			
			} }
		public string statusimage { get; set; }
	}

}
