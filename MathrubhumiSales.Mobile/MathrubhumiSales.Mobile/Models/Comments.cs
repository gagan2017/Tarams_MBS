﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class Comments
	{
		public string ProfilePic { get; set; }
		public string Description { get; set; }
		public string Who { get; set; }
		public string Timeago { get; set; }
		public TaskComment model { get; set; }
		public Comments()
		{

		}
		public Comments(TaskComment model)
		{
			this.model = model;
			ProfilePic = "Demoprofilepic.png";
			Description = model.Comment;
			Who = model.User.FirstName;
			Timeago = FactoryDate._instance.getTime(DateTime.Now,model.CommentedTime);
		}

	}
}
