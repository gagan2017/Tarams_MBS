﻿using System;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class ClientListingModel
	{
		public ClientModel model;
		public int id { get; set; }
		public string Heading { get; set; }
		public string Activities
		{
			get
			{
				return NoActivities + " activities";
			}
		}
		public string Meetings { get { return NoMeetings + " meetings"; } }
		public int NoActivities { get; set; }
		public int NoMeetings { get; set; }

		public ClientListingModel(ClientModel mod)
		{
			id = mod.Id;
			Heading = mod.Name;
			NoActivities = mod.Agencies.Count;
			NoMeetings = mod.Agencies.Count;

			model = mod;

		}
		public ClientListingModel()
		{

		}

	}
}
