﻿using System;
namespace MathrubhumiSales.Mobile.Model
{
	public class ListItemModel
	{
		public int Priority { get; set; }
		public bool IsRed { get; set; }
		public CommentsRedItem _redItem { get; set; }
		public CommentsBlueItem _blueItem { get; set; }
		public ListItemModel()
		{
		}
	}
}
