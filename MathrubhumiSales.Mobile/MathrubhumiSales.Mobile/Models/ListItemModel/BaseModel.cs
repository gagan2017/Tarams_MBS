﻿using System;
namespace MathrubhumiSales.Mobile.BaseModelView
{
	public class BaseModel
	{
		public string Heading { get; set; }
		public string Description { get; set; }
		public string Time { get; set; }
		public string Type { get; set; }
		public string By { get; set; }
		public int priority { get; set; }
		public string Rating { get; set; }
		public BaseModel()
		{
		}
	}
}
