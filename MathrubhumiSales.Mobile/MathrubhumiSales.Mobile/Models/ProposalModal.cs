﻿using System;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ProposalModal
	{
		public int MeetingId { get; set; }
		public int ProposalId { get; set; }
		public ProposalModel p;
		public string EventName { get; set; }
		public string EventRate
		{
			get { return "Rs" + Rate.ToString(); }
		}
		public int Rate { get; set; }
		public string EventDate
		{
			get
			{

				return FactoryDate._instance.GetMonth(EDate);
			}
		}
		public string DealSource
		{
			get
			{
				if (valid())
				{

					return "";
				}
				if (!DealClosed) return "RedCircle.png";
				return "GreenCircle.png";
			}

		}
		public bool isOptionsVissible
		{
			get
			{
				if (valid())
				{
					return true;
				}
				return false;
			}
		}
		public DateTime EDate { get; set; }
		public string EventTime { get { return FactoryDate._instance.getTime(EDate); } }
		public bool DealClosed { get; set; }
		public string DealText
		{
			get
			{
				if (valid()) return "";
				if (!DealClosed) return "Deal Cancaled";
				return "Deal Closed";
			}
		}
		public string DealDate
		{
			get
			{
				if (valid()) return "";
				return FactoryDate._instance.GetMonth(DDate) + FactoryDate._instance.getTime(DDate);
			}
		}
		public string DealTime
		{
			get
			{
				if (valid()) return "DEAL time";
				return FactoryDate._instance.GetMonth(DDate) + FactoryDate._instance.getTime(DDate);
			}
		}

		public DateTime DDate
		{
			get; set;
		}
		bool valid()
		{
			if (p != null)
			{
				if (p.DealStatus == Shared.Entities.Enums.DealStatus.Running)
				{
					return true;
				}
			}
			return false;
		}


		/*EventName = "New Year Bash",
				Rate = 1500000,
				EDate = DateTime.Today,
				DealClosed = true,
				DDate = DateTime.Today*/
		public ProposalModal()
		{
		}
		public ProposalModal(ProposalModel p, int Meetingid)
		{
			this.p = p;
			MeetingId = Meetingid;
			ProposalId = p.Id;
			EventName = p.Summary;
			Rate = p.Amount;
			EDate = p.DateAndTime;
			if (p.DealStatus == Shared.Entities.Enums.DealStatus.Closed)
			{
				DealClosed = true;
			}
			DDate = p.DealStatusDate;

		}
	}
}
