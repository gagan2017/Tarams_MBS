namespace MathrubhumiSales.Mobile
{
	public class Assignee
	{
		public int Id
		{
			get; set;
		}
		public bool isChecked { get; set; }
		public bool isUser { get; set; }
		public string AssigneeName { get; set; }
		public string SectionName { get; set; }
	}
}