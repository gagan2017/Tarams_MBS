﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class CreateGroupViewModel : BaseCreateProjectAndGroup
	{
		public GroupModel groupModle;
		public CreateGroupViewModel()
		{
			ButtonName = "Create Group";
			LabelName = "Groups Name";
			listnoItems = new List<UserModel>();
			listItems = new ObservableCollection<UserModel>();
			groupModle = new GroupModel()
			{
				Members = new List<UserModel>(),

			};
		}

		async override public Task PostData(string name, DateTime d)
		{
			groupModle.Name = name;
			await ApiGroup.Instance.Post(groupModle);
		}

		public override bool SetUsers(UserModel v)
		{
			if (groupModle.Members.Find((obj) => obj.Id==v.Id) == null)
			{
				groupModle.Members.Add(v);
			}
			else
			{
				groupModle.Members.Remove(v);
			}
			return true;
		}
	}
}
