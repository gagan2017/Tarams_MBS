﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.ViewModels
{
	public class AgencyViewModel : IAsyncInitialization
	{
		public ObservableCollection<AgencyModel> item { get; set; }
		List<AgencyModel> items { get; set; }
		public ActivityIndicator activity;
		public AgencyViewModel()
		{
			item = new ObservableCollection<AgencyModel>();
		}

		public Task Initialization
		{
			get; private set;
		}
		public void start()
		{
			Initialization = GetDatasAsync();

		}
		public async Task GetDatasAsync()
		{

			items = await new ApiAgencies().GetAll();
			item.Clear();
			foreach (var v in items)
			{
				item.Add(v);
			}



		}


	}
}
