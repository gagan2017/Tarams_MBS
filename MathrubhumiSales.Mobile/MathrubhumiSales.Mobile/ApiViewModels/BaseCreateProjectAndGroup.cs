﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public interface IBaseCreateProjectAndGroup : IAsyncInitialization
	{
		Task<List<UserModel>> GetUsers(SearchModel text);
		Task PostData(string name, DateTime d);
		bool SetUsers(UserModel v);
	}

}
