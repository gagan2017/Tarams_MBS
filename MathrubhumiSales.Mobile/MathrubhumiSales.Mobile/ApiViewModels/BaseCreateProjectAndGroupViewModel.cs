﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public abstract class BaseCreateProjectAndGroup : IBaseCreateProjectAndGroup
	{
		public ObservableCollection<UserModel> listItems
		{
			get; set;
		}
		public string ButtonName { get; set; }
		public string LabelName { get; set; }
		public bool IsTimeDateVissible { get; set; }
		public List<UserModel> listnoItems { get; set; }
		public Task Initialization { get; }
		public abstract Task PostData(string name, DateTime d);

		public abstract bool SetUsers(UserModel v);
		public BaseCreateProjectAndGroup()
		{
			Initialization = getData();
		}

		internal void setUserById(int sen)
		{
			SetUsers(listnoItems.Find((obj) => obj.Id==sen));
		}

		async Task getData()
		{
			listnoItems = await new ApiUser().GetAll(new SearchModel()
			{
				SearchText = " "
			});
		}
		async public Task<List<UserModel>> GetUsers(SearchModel text)
		{
			listnoItems = await new ApiUser().GetAll(text);
			return listnoItems;
		}
	}
}
