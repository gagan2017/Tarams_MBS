﻿using System;
using System.Collections.Generic;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class GroupUserViewModelBase
	{

		public int CountOfRemainingUser { get; set; }
		public int Id { get; set; }
		public List<UserModel> listOfUsers { get; set; }
		public string GroupName { get; set; }
		public string One { get; set; }
		public string Two { get; set; }
		public string Three { get; set; }
		public string Four { get; set; }
		public string Five { get; set; }
		public string Onei { get; set; }
		public string Twoi { get; set; }
		public string Threei { get; set; }
		public string Fouri { get; set; }
		public string Fivei { get; set; }

		public void NullCheck()
		{
			if (listOfUsers.Count >= 1 && listOfUsers[0] != null)
			{
				One = listOfUsers[0].FirstName;
				Onei = "Demoprofilepic.png";
			}
			if (listOfUsers.Count >= 2 && listOfUsers[1] != null)
			{
				Two = listOfUsers[1].FirstName;
				Twoi = "Demoprofilepic.png";
			}
			if (listOfUsers.Count >= 3 && listOfUsers[2] != null)
			{
				Three = listOfUsers[2].FirstName;
				Threei = "Demoprofilepic.png";
			}
			if (listOfUsers.Count >= 4 && listOfUsers[3] != null)
			{
				Four = listOfUsers[3].FirstName;
				Fouri = "Demoprofilepic.png";
			}
			if (listOfUsers.Count >= 5 && listOfUsers[4] != null)
			{
				Five = listOfUsers[4].FirstName;
				Fivei = "Demoprofilepic.png";
			}
		}
		public GroupUserViewModelBase()
		{
		}
	}
}
