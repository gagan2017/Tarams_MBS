﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class ClientListingViewModel: IAsyncInitialization
	{
		public List<ClientModel> clients;

		public ClientListingViewModel(Factory f,ClientListing c)
		{
			Initialization = getClients(f,c);
		}

		async Task getClients(Factory f,ClientListing c)
		{
			clients = await new ApiClient().getAll();
			//f = new Factory(clients);
			//c.Start();
		}

		public Task Initialization
		{
			get;set;
		}

		public List<ClientModel> Clients
		{
			get
			{
				return clients;
			}

			set
			{
				clients = value;
			}
		}
	}
}
