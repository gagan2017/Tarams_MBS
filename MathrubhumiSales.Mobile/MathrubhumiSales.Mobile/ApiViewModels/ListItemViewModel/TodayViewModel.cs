﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MathrubhumiSales.Mobile
{
	public class TodayViewModel
	{
		public ObservableCollection<ListItemModel> itemList { get; set; }
		public List<ListItemModel> itemListUnsorted { get; set; }
		public int count { get { return 400; } }
		public TodayViewModel()
		{
			itemList = new ObservableCollection<ListItemModel>();
			DummyListItemViewModel.Instance.Updated += Instance_Updated;

		}
		void Instance_Updated(object sender, EventArgs e)
		{
			itemList.Clear();
			itemListUnsorted = DummyListItemViewModel.Instance.itemListUnsorted;
			getTodayElements();
		}

		public void getTodayElements()
		{
			var lObj = itemListUnsorted.FindAll((obj) => obj.Time.DayOfYear == DateTime.Now.DayOfYear);
			if (lObj == null || lObj.Count == 0)
			{
				itemList.Add(new ListItemModel()
				{
					Heading = "Nothing Scheduled for Todays Date" 
				});
				return;
			}
			itemList.Add(new ListItemModel()
			{
				Heading = "Today " + FactoryDate._instance.GetMonth(DateTime.Now)
			});
			foreach (var item in lObj)
			{
				itemList.Add(item);
			}
		}
	}
}
