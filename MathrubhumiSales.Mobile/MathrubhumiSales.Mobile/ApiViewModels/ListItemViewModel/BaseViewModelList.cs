﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public abstract class BaseViewModelList : IAsyncInitialization
	{

		public bool IsFirst=true;
		public abstract void UpdateUnsorted();

		public List<MeetingModel> clientModel { get; set; }
		public List<TaskModel> taskModel { get; set; }
		public Task Initialization { get; private set; }

		public BaseViewModelList()
		{
			Initialization = GetData();
		}

		public void start()
		{
			Initialization = GetData();
		}
		async public Task GetData()
		{
			
			clientModel = await new ApiMeeting().getAll(0);
			taskModel = await new ApiTask().GetAll();
			UpdateUnsorted();

		}
	}
}