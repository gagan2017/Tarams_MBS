using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Model;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AllPageViewModel
	{


		public int count { get { return 400; } }
		public ObservableCollection<ListItemModel> itemList;
		public List<AllPageModel> itemsource { get; set; }

		List<ListItemModel> itemListUnsorted;

		public AllPageViewModel()
		{

			itemsource = new List<AllPageModel>();
			itemsource.Add(new AllPageModel()
			{
				Listname = "UnScheduled",
				Count = 15
			});
			itemsource.Add(new AllPageModel()
			{
				Listname = "Conmpleted",
				Count = 223
			});
			itemList = new ObservableCollection<ListItemModel>();
			DummyListItemViewModel.Instance.Updated += Instance_Updated;
		}
		void Instance_Updated(object sender, EventArgs e)
		{
			itemList.Clear();
			foreach (var item in DummyListItemViewModel.Instance.itemListUnsorted)
			{
				itemList.Add(item);
			}

		}


	}
}
