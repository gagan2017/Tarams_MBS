﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MathrubhumiSales.Mobile
{
	public class SevenDaysViewModel
	{
		public ObservableCollection<ListItemModel> itemList { get; set; }
		public List<ListItemModel> itemListUnsorted { get; set; }
		public int count { get { return 400; } }


		public SevenDaysViewModel()
		{
			itemList = new ObservableCollection<ListItemModel>();
			DummyListItemViewModel.Instance.Updated += Instance_Updated;
		}

		void Instance_Updated(object sender, EventArgs e)
		{
			itemList.Clear();
			itemListUnsorted = DummyListItemViewModel.Instance.itemListUnsorted;
			getTommorrowElements();
			getSevenDaysElements();
		}

		public void getTommorrowElements()
		{
			var lObj = itemListUnsorted.FindAll((obj) => obj.Time.DayOfYear == (DateTime.Now.AddDays(1).DayOfYear));
			if (lObj == null || lObj.Count == 0)
			{
				return;
			}
			itemList.Add(new ListItemModel()
			{
				Heading = "Tommorrow " + FactoryDate._instance.getTommorrowDate()
			});
			foreach (var item in lObj)
			{
				itemList.Add(item);
			}
		}
		void getSevenDaysElements()
		{
			var lObj = itemListUnsorted.FindAll((obj) => obj.Time.DayOfYear <= DateTime.Now.AddDays(7).DayOfYear && obj.Time.DayOfYear > DateTime.Now.AddDays(1).DayOfYear);
			if (lObj == null || lObj.Count == 0)
			{
				itemList.Add(new ListItemModel()
				{
					Heading = "Nothing for upcomming 7 days"
				});
				return;
			}
			itemList.Add(new ListItemModel()
			{
				Heading = "Up to 7 days Except Tommorrow"
			});
			foreach (var item in lObj)
			{
				itemList.Add(item);
			}
		}
	}
}
