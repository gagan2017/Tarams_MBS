using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MathrubhumiSales.Mobile
{
	public class DummyListItemViewModel : BaseViewModelList
	{
		public event EventHandler Updated;
		static DummyListItemViewModel _instance { get; set; }
		public static DummyListItemViewModel Instance
		{
			get
			{
				if (_instance == null) _instance = new DummyListItemViewModel(); return _instance;
			}
		}
		async public Task AwaitRefresh()
		{
			await GetData();
		}
		public void Refresh()
		{
			
			start();
		}
		public bool GetTodayDate(DateTime day)
		{
			if (day.DayOfYear == DateTime.Now.DayOfYear)
			{
				return true;
			}
			return false;
		}
	
		public List<ListItemModel> itemListUnsorted { get; set; }
		public int count { get { return 400; } }
		public List<CommentsRedItem> commentredItems;
		public ListDataTemplate itemTemplate { get { return new ListDataTemplate(); } }

		public DummyListItemViewModel()
		{
			itemListUnsorted = new List<ListItemModel>();
          
		}
		async internal Task<List<CommentsRedItem>> getObjOfTappedItem(CommentsRedItem _redItem)
		{
			return await CommentFactory._instance.FindEleFromApi(_redItem.m.Client.Id);
		}

		internal CommentsBlueItem getBlueItem(int id)
		{
			return itemListUnsorted.Find((obj) => obj._blueItem!=null && obj._blueItem.task.Id==id)._blueItem;

		}

		public override void UpdateUnsorted()
		{
			itemListUnsorted = new List<ListItemModel>();
			if (clientModel != null)
			{
				foreach (var item in clientModel)
				{
					itemListUnsorted.Add(new ListItemModel()
					{
						_redItem = new CommentsRedItem(item, item.Client.Name)
					});
				}
			}
			if (taskModel != null)
			{
				foreach (var item in taskModel)
				{
					itemListUnsorted.Add(new ListItemModel()
					{
						_blueItem = new CommentsBlueItem(item)
					});
				}
			}
			itemListUnsorted.Sort((x, y) =>
						  {
							  return y.Time.CompareTo(x.Time);
						  });

			//itemListUnsorted=itemListUnsorted.FindAll((obj) => obj.Time == DateTime.Now || obj.Time > DateTime.Now);
			Updated?.Invoke(itemListUnsorted, new EventArgs());
		}



	}

}
