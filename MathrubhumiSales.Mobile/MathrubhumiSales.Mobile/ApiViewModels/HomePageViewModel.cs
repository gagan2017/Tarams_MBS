﻿using System;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class HomePageViewModel : IAsyncInitialization
	{
		public QuoteModel model { get; set; }
		public HomePageViewModel()
		{

		}
		Label bc;
		public void start(Label bc)
		{
			this.bc = bc;
			Initialization = GetData();
		}
		async Task GetData()
		{
			var list = await new ApiQuote().GetAll();
			var rnd = new Random()
				;
			var item = rnd.Next(list.Count);
			model = list[item];
			list.Remove(model);
			Sessions.instance.GetQuote = JsonConvert.SerializeObject(list);
			var fs = new FormattedString();
			fs.Spans.Add(new Span { Text = model.Quote, ForegroundColor = Color.Black, Font = Font.SystemFontOfSize(28) });
			fs.Spans.Add(new Span { Text = "\r\n" + model.By, ForegroundColor = Color.Gray, Font = Font.SystemFontOfSize(22) });

			bc.FormattedText = fs;
		}

		public Task Initialization
		{
			get; private set;
		}
	}
}
