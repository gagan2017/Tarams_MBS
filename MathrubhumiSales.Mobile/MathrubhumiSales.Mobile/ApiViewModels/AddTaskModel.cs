using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AddTaskModel : IAsyncInitialization
	{

		public List<LabelModel> label { get; set; }
		public List<UserModel> userlist { get; set; }
		public List<GroupModel> grouplist { get; set; }
		public TaskModel taskModel { get; set; }
		public ObservableCollection<Assignee> taskAsigneeDetails { get; set; }

		public Task Initialization
		{
			get; private set;
		}

		public AddTaskModel()
		{
			taskModel = new TaskModel();
			taskModel.Users = new List<UserModel>();
			taskModel.Teams = new List<GroupModel>();

		}

		internal bool setLabel(string v)
		{
			if (v == null)
			{
				return false;
			}
			var arr = v.Split(' ');
			this.taskModel.Labels = label.Find((obj) => obj.Id == int.Parse(arr[0].ToString()));
			return true;
		}

		public void GetLabel(Picker p)
		{
			Initialization = getLabel(p);
		}
		async Task getLabel(Picker p)
		{
			label = await new ApiLabel().GetAll();
			p.Items.Clear();
			foreach (var item in label)
			{

				p.Items.Add(item.Id + " " + item.Name);

			}
		}

		public async Task<ObservableCollection<Assignee>> getList(SearchModel searchModel)
		{
			taskAsigneeDetails = new ObservableCollection<Assignee>();
			userlist = await new ApiUser().GetAll(searchModel);
			grouplist = await ApiGroup.Instance.GetAll();

			foreach (var item in userlist)
			{
				taskAsigneeDetails.Add(new Assignee()
				{
					Id = item.Id,
					AssigneeName = item.FirstName,
					SectionName = item.LastName,
					isUser = true
				});
			}
			foreach (var item in grouplist)
			{
				taskAsigneeDetails.Add(new Assignee()
				{
					Id = item.Id,
					AssigneeName = item.Name,
					isUser = false,
					SectionName = item.Members.Count.ToString()
				});
			}
			return taskAsigneeDetails;
		}






		internal void IdPresent(string Name, string s)
		{

			foreach (var item in taskAsigneeDetails)
			{
				if (item.AssigneeName == Name)
				{
					setSelectedItem(item);
					break;
				}
			}


		}


		internal void setSelectedItem(Assignee val)
		{
			if (val.isUser)
			{
				var items = this.userlist.Find((obj) => obj.FirstName == val.AssigneeName);
				if (items != null)
				{
					if (taskModel.Users.Contains(items))
					{
						taskModel.Users.Remove(items);
					}
					else
					{
						taskModel.Users.Add(items);
					}

				}
				return;
			}
			var item = this.grouplist.Find((obj) => obj.Name == val.AssigneeName);
			if (item != null)
			{
				if (taskModel.Teams.Contains(item))
				{
					taskModel.Teams.Remove(item);

				}
				else
				{
					taskModel.Teams.Add(item);
				}
			}
		

		}






	}
}
