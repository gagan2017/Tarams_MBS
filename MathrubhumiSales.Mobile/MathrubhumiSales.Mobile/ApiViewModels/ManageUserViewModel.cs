﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ApiManageUserItem : GroupUserViewModelBase
	{

		public ApiManageUserItem(GroupModel g)
		{

			this.GroupName = g.Name;
			this.listOfUsers = g.Members;
			NullCheck();

			if (listOfUsers.Count > 5)
			{
				CountOfRemainingUser = listOfUsers.Count - 5;
			}
			else
			{
				CountOfRemainingUser = -1;
			}
		}
	}


	public class ManageUserViewModel : IAsyncInitialization
	{
		public ObservableCollection<bool> IsActivated { get; set; }
		public ObservableCollection<ApiManageUserItem> listuser { get; set; }

		public Task Initialization
		{
			get; private set;
		}

		public ManageUserViewModel()
		{
			IsActivated = new ObservableCollection<bool>();
			IsActivated.Add(true);

			listuser = new ObservableCollection<ApiManageUserItem>();

		}
		public void start()
		{
			Initialization = GetData();
		}
		async Task GetData()
		{
			listuser.Clear();
			var list = await ApiGroup.Instance.GetAll();

			if (list.Count == 0)
			{
				return;
			}
			foreach (var item in list)
			{
				listuser.Add(new ApiManageUserItem(item));
			}
		}
	}
}


