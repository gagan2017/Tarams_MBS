﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Shared.Models;

namespace MathrubhumiSales.Mobile
{
	public class CreateProjectViewModel : BaseCreateProjectAndGroup
	{
		
		public ProjectModel projectModle;
		public CreateProjectViewModel()
		{
			ButtonName = "Create Project";
			LabelName = "Project Name";
			IsTimeDateVissible = true;
			listnoItems = new List<UserModel>();
			listItems = new ObservableCollection<UserModel>();
			projectModle = new ProjectModel()
			{
				Users = new List<UserModel>()
			};
		}

		public async override Task PostData(string name, DateTime d)
		{
			projectModle.ProjectName = name;
			projectModle.TimeAndDate = d;
			await new ApiProject().Post(projectModle);
		}

		public override bool SetUsers(UserModel v)
		{
			if (this.projectModle.Users.Find((obj) => v.Id == obj.Id) == null)
			{
				projectModle.Users.Add(v);
			}
			else
			{
				projectModle.Users.Remove(v);
			}
			return true;
		}
	}
}
