﻿using System;
using System.Collections.ObjectModel;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AddAgencyPageViewModel
	{
		public AgencyModel model { get; set;}
		public ObservableCollection<AgencyContactModel> AgencyContacts { get; set; }
		public AddAgencyPageViewModel()
		{
			model = new AgencyModel()
			{
				AgencyContacts = new System.Collections.Generic.List<AgencyContactModel>()
			};
			AgencyContacts = new ObservableCollection<AgencyContactModel>();

		}
		public void refresh()
		{
			foreach (var item in model.AgencyContacts)
			{
				AgencyContacts.Remove(item);
				AgencyContacts.Add(item);
			}
		}
	}
}

