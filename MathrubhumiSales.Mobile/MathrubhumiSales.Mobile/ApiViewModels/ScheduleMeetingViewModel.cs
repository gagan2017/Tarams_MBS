using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Api.Models;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ScheduleMeetingViewModel : IAsyncInitialization
	{
		Picker client, label, proposal;
		ActivityIndicator activity;
		ApiMeeting meeting;
		MeetingModel meetinModel { get; set; }
		public List<ClientModel> ClientModel { get; set; }
		public List<AgencyModel> AgencyModel { get; set; }
		public List<UserModel> Users { get; set; }
		public List<ProjectModel> ProjectModel { get; set; }
		public List<LabelModel> LabelModel { get; set; }
		public List<ProposalModel> ProposalModel { get; set; }
		public Task Initialization
		{
			get; private set;
		}

		public ScheduleMeetingViewModel()
		{
			meeting = new ApiMeeting();
			meetinModel = new MeetingModel()
			{
				Projects = new List<ProjectModel>(),
				Users = new List<UserModel>(),
				Agencies = new List<AgencyModel>()
			};
		}

		internal void setUserbyId(int sen)
		{
			setUser(Users.Find((obj) => obj.Id == sen));
		}

		public DateTime getMeetingTime()
		{
			return meetinModel.MeetingTime;
		}
		public void Start(Picker client, Picker proposal, Picker Label, ActivityIndicator activity)
		{
			this.proposal = proposal;
			this.label = Label;
			this.client = client;
			this.activity = activity;
			Initialization = getData();
		}

		async Task getData()
		{
			this.activity.IsVisible = true;
			activity.IsRunning = true;
			var res = await meeting.getAllBeforPost();
			Users = res.Users;
			ClientModel = res.Clients;
			AgencyModel = res.Agencies;
			ProjectModel = res.Projects;
			LabelModel = res.Labels;
			ProposalModel = res.Proposals;
			/*Users = await new ApiUser().GetAll(new SearchModel(""));
			ClientModel = await new ApiClient().getAll();
			AgencyModel = await new ApiAgencies().GetAll();
			ProjectModel = await new ApiProject().GetAll();
			LabelModel = await new ApiLabel().GetAll();
			ProposalModel = await new ApiProposal().GetAll();*/
			this.activity.IsVisible = false;
			if (ClientModel != null)
			{
				client.Items.Clear();
				foreach (var item in ClientModel)
				{
					client.Items.Add(item.Name + ":" + item.Address);
				}
			}
			else
			{
				ClientModel = new List<Shared.Models.ClientModel>();
			}
			if (LabelModel != null)
			{
				label.Items.Clear();
				foreach (var item in LabelModel)
				{
					label.Items.Add(item.Id + " " + item.Name);
				}
			}
			else
			{
				LabelModel = new List<Shared.Models.LabelModel>();
			}
			if (ProposalModel != null)
			{
				proposal.Items.Clear();
				foreach (var item in ProposalModel)
				{
					proposal.Items.Add(item.Id + " " + item.Summary + " " + item.Amount);
				}
			}
			else
			{
				ProposalModel = new List<Shared.Models.ProposalModel>();
			}
			activity.IsVisible = false;
		}

		async internal Task<bool> Post(string comment)
		{

			meetinModel.Comment = comment;
			meetinModel.User = Sessions.instance.CurrentUser;
			return (await new ApiMeeting().Post(meetinModel)).IsSuccessStatusCode;

		}

		internal void setDateTime(DateTime date)
		{
			meetinModel.MeetingTime = date;
		}

		async internal Task<List<UserModel>> getUsers(string s)
		{
			return (await new ApiUser().GetAll(new SearchModel(s)));
		}

		public void setLabel(string s)
		{
			if (s == null)
			{
				return;
			}
			var arr = s.Split(' ');
			meetinModel.Label = LabelModel.Find((obj) => obj.Id == int.Parse(arr[0]));
		}
		public void setClient(string s)
		{
			if (s == null)
			{
				return;
			}
			var arr = s.Split(':');
			if (arr[1] == "") ;
			{
				arr[1] = null;
			}
			meetinModel.Client = ClientModel.Find((obj) => obj.Name == arr[0] && obj.Address == arr[1]);
		//	Users = meetinModel.Users;
		}
		public void setProposal(string s)
		{
			if (s == null)
			{
				return;
			}
			var arr = s.Split(' ');
			meetinModel.Proposal = ProposalModel.Find((obj) => obj.Id == int.Parse(arr[0]));
		}
		public void setUser(UserModel s)
		{
			if (meetinModel.Users.Find((obj) => obj.Id == s.Id) == null)
			{
				meetinModel.Users.Add(s);
			}
			else
			{
				meetinModel.Users.Remove(s);
			}

		}
		public void setIsMarked(bool T)
		{
			meetinModel.IsHighPriority = T;
		}
		public void setMeetinType(MeetingType T)
		{
			meetinModel.MeetingType = T;
		}
		internal void AddAgency(AgencyModel c)
		{
			if (meetinModel.Agencies.Count == 0)
			{
				meetinModel.Agencies.Add(c);
				return;
			}
			var item = meetinModel.Agencies.Find((obj) => obj.Id == c.Id);
			if (item == null)
			{
				meetinModel.Agencies.Add(item);
			}
			else
			{
				meetinModel.Agencies.Remove(item);
			}

		}
		internal void AddProjects(ProjectModel c)
		{
			if (meetinModel.Projects.Count == 0)
			{
				meetinModel.Projects.Add(c);
				return;
			}
			var item = meetinModel.Projects.Find((obj) => obj.Id == c.Id);

			if (item == null)
			{
				meetinModel.Projects.Add(item);
			}
			else
			{
				meetinModel.Projects.Remove(item);
			}

		}
		public string status = "";
		public bool ValidateNull()
		{
			if (this.meetinModel.Client == null)
			{
				status = "Add Client";
				return true;
			}
			if (this.meetinModel.Users == null || this.meetinModel.Users.Count == 0)
			{
				status = "Add Peoples";
				return true;
			}

			/*if (this.meetinModel.Projects == null || this.meetinModel.Projects.Count == 0)
			{
				status = "Add Projects";
				return true;
			}*/
			return false;
		}

		internal void refresh(ActivityIndicator activity)
		{
			getData();
		}
	}
}
