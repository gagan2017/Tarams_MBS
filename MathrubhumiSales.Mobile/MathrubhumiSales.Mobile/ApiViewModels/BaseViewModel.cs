﻿using System;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;

namespace MathrubhumiSales.Mobile
{
	public abstract class BaseViewModel : IAsyncInitialization
	{
		public  Task Initialization;
		public BaseViewModel()
		{
				
		}
		public event EventHandler StartLoading;
		public event EventHandler StopLoading;
		public abstract Task GetData();
		public void Refresh()
		{
			StartLoading?.Invoke(null, new EventArgs());
			GetData();
		}
		public void Updated()
		{
			StopLoading.Invoke(null,new EventArgs());
		}
	}
}
