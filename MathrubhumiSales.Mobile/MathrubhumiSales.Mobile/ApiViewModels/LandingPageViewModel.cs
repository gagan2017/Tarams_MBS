﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathrubhumiSales.Mobile.Models
{
    public class LandingPageViewModel
    {
        public System.Collections.Generic.List<ListItems> listItems
        {
            get
            {
                return new System.Collections.Generic.List<ListItems>()
                {
                    new ListItems(){ Listname="Clients",Num="260" },
                    new ListItems(){ Listname="Projects",Num="62" },
                    new ListItems(){ Listname="Teams",Num="18" }
                    



                };
            }
        }
        public MonthAndYear data
        {
            get
            {
                return new MonthAndYear()
                {
                    month = "Jan",
                    year = 2010
                };
            }
        }


    }
}
