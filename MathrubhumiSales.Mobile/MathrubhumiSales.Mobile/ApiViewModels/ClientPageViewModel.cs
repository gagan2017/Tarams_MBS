﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;

namespace MathrubhumiSales.Mobile
{
	public class ClientPageViewModel : IAsyncInitialization
	{
		List<CommentsRedItem> predList;
		public event EventHandler Update;

		public ClientPageViewModel()
		{

		}



		public ClientPageViewModel(List<CommentsRedItem> predList, CommentsRedItem pred2)
		{
			this.predList = predList;

			Initialization = GetData(pred2);

			//clientName.Text = pred[0].Heading;
			//temp = new Factory(pred).getContent(one.Text

		}

		public ClientPageViewModel(List<CommentsRedItem> predList, ClientListingModel clients)
		{
			this.predList = predList;

			Initialization = GetClient(clients);
		}

		public async Task GetClient(ClientListingModel clients)
		{
			var list = await new CommentFactory().FindEleFromApi(clients.model.Id);
			foreach (var item in list)
			{
				predList.Add(item);
			}
			Update?.Invoke(this, new EventArgs());
		}

		public async Task GetData(CommentsRedItem pred)
		{
			var list = await DummyListItemViewModel.Instance.getObjOfTappedItem(pred);
			foreach (var item in list)
			{
				predList.Add(item);
			}
			Update?.Invoke(this, new EventArgs());
		}

		public Task Initialization
		{
			get; private set;
		}
	}
}
