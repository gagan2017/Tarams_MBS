﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AgecncyContactListViewModel : IAsyncInitialization
	{

		public ObservableCollection<AgencyContactModel> AgencyContacts { get; set; }
		public int id { get; set; }
		public Task Initialization { get; private set; }
		public AgecncyContactListViewModel(int id)
		{
			this.id = id;
			AgencyContacts = new ObservableCollection<AgencyContactModel>();
		}
		public void Refresh()
		{
			Initialization = GetDatasAsync();
		}
		public async Task GetDatasAsync()
		{
			var items = await new ApiAgencyContact().GetById(id);
			if (items == null)
			{
				return;
			}
			if (AgencyContacts.Count != 0)
				AgencyContacts.Clear();
			foreach (var v in items)
			{
				AgencyContacts.Add(v);
			}
		}


	}
}
