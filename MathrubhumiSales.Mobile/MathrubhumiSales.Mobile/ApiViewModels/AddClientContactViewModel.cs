﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class AddClientContactViewModel : IAsyncInitialization
	{
		Picker p;
		public List<DesignationModel> Designationitems { get; set; }

		public AddClientContactViewModel(Picker p)
		{
			this.p = p;
			Initialization = GetDatasAsync();

		}

		async Task GetDatasAsync()
		{

			Designationitems = await new ApiClientDesignation().GetAll();
			p.Items.Clear();
			foreach (var item in Designationitems)
			{
				if (item.Name == null) continue;

				p.Items.Remove(item.Name);
				p.Items.Add(item.Name);
			}

		}

		public Task Initialization { get; private set; }
	}
}
