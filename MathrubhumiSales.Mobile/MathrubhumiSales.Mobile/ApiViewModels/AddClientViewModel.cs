﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Models;
using MathrubhumiSales.Shared.Entities.Enums;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{

	public class AddClientViewModel : IAsyncInitialization
	{


		public List<string> picker { get; set; }
		public ClientModel client { get; set; }
		public ObservableCollection<ClientContactModel> ClientContactList { get; set; }
		public List<AgencyModel> AgencyList { get; set; }
		public ObservableCollection<AgencyModel> itemSource { get; set; }
		public Task Initialization
		{
			get;
			private set;
		}
		public AddClientViewModel()
		{
			picker = new List<string>();
			picker.Add("Construction Category");
			picker.Add("FMCG Category");
			picker.Add("Healthcare Category");
			AgencyList = new List<AgencyModel>();
			itemSource = new ObservableCollection<AgencyModel>();

		}
		public void Start()
		{
			client = new ClientModel();
			ClientContactList = new ObservableCollection<ClientContactModel>();
			Initialization = getData();
		}

		async Task getData()
		{
			
			AgencyList = await new ApiAgencies().GetAll();
			foreach (var item in AgencyList)
			{
				itemSource.Add(item);
			}
		}

		internal ObservableCollection<AgencyModel> refreshAgency()
		{
			itemSource.Clear();
			foreach (var item in AgencyList)
			{
				itemSource.Add(item);
			}
			return itemSource;
		}

		public void refresh()
		{
			foreach (var item in client.ClientContacts)
			{
				ClientContactList.Add(item);
			}

		}

		internal ClientCategory getSelectedItem(string v)
		{
			switch (v)
			{
				case "Construction Category": return ClientCategory.ConstructionCategory;
				case "FMCG Category": return ClientCategory.FMCGCategory;
				case "Healthcare Category": return ClientCategory.HealthcareCategory;

			}
			return ClientCategory.ConstructionCategory;
		}

}
}