﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MathrubhumiSales.Interfaces;
using MathrubhumiSales.Shared.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public class ApiManageProjectsItem : GroupUserViewModelBase
	{
		public string Time { get; set; }
		public ApiManageProjectsItem()
		{
			GroupName = "None of the Projects Completed";
			CountOfRemainingUser = -1;
		}
		public ApiManageProjectsItem(ProjectModel g)
		{

			this.GroupName = g.ProjectName;
			this.listOfUsers = g.Users;
			this.Time = FactoryDate._instance.GetMonth(g.TimeAndDate);
			NullCheck();

			if (listOfUsers.Count > 5)
			{
				CountOfRemainingUser = listOfUsers.Count - 5;
			}
			else
			{
				CountOfRemainingUser = -1;
			}
		}
	}
	public class ProjectsViewModel : IAsyncInitialization
	{
		public ObservableCollection<ApiManageProjectsItem> onGoingProjects { get; set; }
		public ObservableCollection<ApiManageProjectsItem> completedProjects { get; set; }
		public List<ProjectModel> listProject { get; set; }
		public event EventHandler Updated;
		public Task Initialization
		{
			get; private set;
		}

		public ProjectsViewModel()
		{
			onGoingProjects = new ObservableCollection<ApiManageProjectsItem>();
			completedProjects = new ObservableCollection<ApiManageProjectsItem>();

		}

		async Task GetData()
		{
			onGoingProjects.Clear();
			var listProject = await new ApiProject().GetAll();

			if (listProject.Count == 0)
			{

			}
			foreach (var item in listProject)
			{
				if (item.ProjectName.Trim().Length == 0) continue;
				if (item.Completed)
					completedProjects.Add(new ApiManageProjectsItem(item));
				else
					onGoingProjects.Add(new ApiManageProjectsItem(item));
			}
			if (completedProjects.Count == 0)
			{
				completedProjects.Add(new ApiManageProjectsItem());
			}
			Updated?.Invoke(this,new EventArgs());
		}
		public ObservableCollection<ApiManageProjectsItem> GetCompletedProjects()
		{
			return completedProjects;
		}

		internal void Refresh()
		{
			Initialization = GetData();
		}
	}
}

