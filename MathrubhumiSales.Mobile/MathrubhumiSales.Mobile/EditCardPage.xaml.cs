using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class EditCardPage : ContentPage
	{
		EditCardViewModel model;
		public Image EditBack;
		public bool isPageVissible;
		protected override void OnAppearing()
		{
			base.OnAppearing();
			isPageVissible = true;
		}
		public EditCardPage()
		{
			model = new EditCardViewModel();
			model.SwipeRightCommand = new Command(HandleRightSwypeAction);
			BindingContext = model;
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			picker.BackgroundColor = Color.Transparent;
			picker.Items.Add("Negotiation");
			picker.SelectedItem = picker.Items[0];
			picker.HeightRequest = 40;
			EditBack = backimage;
		}

		void Handle_Star_Tapped(object sender, System.EventArgs e)
		{

		}
		void Handle_Item_Tapped(object sender, System.EventArgs e)
		{

		}
		void HandleRightSwypeAction(object obj)
		{
			this.Navigation.PopAsync();

		}
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			isPageVissible = false;
		}

		internal void Show()
		{

		}
	}
}
