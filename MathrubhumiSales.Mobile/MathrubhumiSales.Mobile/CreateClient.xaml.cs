﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MathrubhumiSales.Mobile.Api;
using MathrubhumiSales.Mobile.Class;
using MathrubhumiSales.Mobile.Models;
using Xamarin.Forms;

namespace MathrubhumiSales.Mobile
{
	public partial class CreateClient : ContentPage
	{
		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (model == null || model.client == null || model.client.ClientContacts == null) return;

			model.refresh();
			list.ItemsSource = model.ClientContactList;
		}
		AddClientViewModel model { get; set; }
		TapGestureRecognizer tappedProduct, tappedBrand, tappedAgency;
		public CreateClient()
		{
			model = new AddClientViewModel();
			tappedProduct = new TapGestureRecognizer();
			tappedProduct.Tapped += Tapped_Product_Tapped;
			tappedBrand = new TapGestureRecognizer();
			tappedBrand.Tapped += Tapped_Brand_Tapped;
			tappedAgency = new TapGestureRecognizer();
			tappedAgency.Tapped += Tapped_Agency_Tapped;
			InitializeComponent();
			listagency.ItemsSource = model.itemSource;
			picker.ItemsSource = model.picker;
			model.Start();
			model.client.Brands = new List<Shared.Models.BrandModel>();
			model.client.Products = new List<Shared.Models.ProductModel>();
			model.client.Agencies = new List<Models.AgencyModel>();
		}
		void Tapped_Agency_Tapped(object sender, EventArgs e)
		{
			var item = model.AgencyList.Find((obj) => obj.Name == removeItem(sender as Image, agencyContent));
			if (item == null) return;
			model.client.Agencies.Remove(model.client.Agencies.Find((obj) => obj.Name == removeItem(sender as Image, agencyContent)));


		}

		void Tapped_Brand_Tapped(object sender, EventArgs e)
		{
			var item = model.client.Brands.Find((obj) => obj.Name == removeItem(sender as Image, brandContent));
			if (item == null) return;
			model.client.Brands.Remove(item);
		}

		void Tapped_Product_Tapped(object sender, EventArgs e)
		{
			var item = model.client.Products.Find((obj) => obj.Name == removeItem(sender as Image, productContent));
			if (item == null) return;
			model.client.Products.Remove(item);

		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{

			Navigation.PushAsync(new CreateClientContact(model.client));
		}


		async Task<bool> AddContent(StackLayout stack, Entry s, TapGestureRecognizer tapped)
		{

			if (s.Text.Trim().Length == 0)
			{
				await DisplayAlert("Error", "Please Fill Fields ", "OK!");
				return false;
			}
			TempContentView tempview = new TempContentView(s.Text);
			tempview.close.GestureRecognizers.Add(tapped);

			stack.Children.Add(tempview);

			return true;
		}

		async void Handle_Brand_Tapped(object sender, System.EventArgs e)
		{
			if (model.client.Brands.Find((obj) => obj.Name.Trim() == Brand.Text.Trim()) != null)
			{
				return;
			}
			if (await AddContent(brandContent, Brand, tappedBrand))
			{
				model.client.Brands.Add(new Shared.Models.BrandModel()
				{
					Name = Brand.Text,
				});
			}
			Brand.Text = "";
		}



		async void Handle_Product_Tapped(object sender, System.EventArgs e)
		{
			if (model.client.Products.Find((obj) => obj.Name.Trim() == Product.Text.Trim()) != null)
			{
				;
			}
			if (await AddContent(productContent, Product, tappedProduct))
			{
				model.client.Products.Add(new Shared.Models.ProductModel()
				{
					Name = Product.Text,
				});
			}
			Product.Text = "";
		}
		async void Handle_Agency_Tapped(object sender, System.EventArgs e)
		{


			if (await AddContent(agencyContent, Agency, tappedAgency))
			{

			}
			Agency.Text = "";
		}

		string removeItem(Image img, StackLayout content)
		{
			TempContentView refrence = null; ;
			foreach (var item in content.Children)
			{
				var v = item as TempContentView;
				if (v.close == img)
				{
					refrence = v;
					break;
				}
			}
			if (refrence == null) return "";
			content.Children.Remove(refrence);
			return refrence.text.Text;

		}

		async void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{

			var v = e.SelectedItem as AgencyModel;
			if (v == null) return;
			Agency.Text = v.Name;
			if (model.client.Agencies.Find((obj) => obj.Id == v.Id) != null)
			{
				model.refreshAgency();
				((ListView)sender).SelectedItem = null;
			}
			if (await AddContent(agencyContent, Agency, tappedAgency))
			{
				model.client.Agencies.Add(v);
			}
			Agency.Text = "";
			((ListView)sender).SelectedItem = null;
		}

		void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			var v = sender as Entry;
			if (v.Text == "")
			{
				listagency.ItemsSource = model.refreshAgency();
				return;
			}
			var result = model.AgencyList.FindAll(s => s.Name.Contains(v.Text));
			listagency.ItemsSource = result;
		}

		async void Handle_Add_Clicked(object sender, System.EventArgs e)
		{
			if (ValidationClass._instance.Validate(new List<Entry>()
			{
				enAddress,enName
			}))
			{
				await DisplayAlert("Error", "Please fill all fields", "Ok!");
				return;
			}
			if (model.client.Brands.Count == 0 || model.client.Products.Count == 0 || model.client.Agencies.Count == 0)
			{
				var res = await DisplayAlert("Error", "fill brand product,agency", "Ok!", "skip");
				if (res)
				{
					return;
				}
			}
			if (picker.SelectedItem == null)
			{
				await DisplayAlert("Error", "Select Category", "Ok!", "skip");
				return;
			}
			foreach (var item in model.client.ClientContacts)
			{
				//item.ProfilePic = null;
				//item.ProfilePicpath = "";
			}
			model.client.Name = enName.Text;
			model.client.Address = enAddress.Text;
			model.client.Category = model.getSelectedItem(picker.SelectedItem as string);
			DependencyService.Get<ICustomPrgressBar>().Start(0);
			var result = await new ApiClient().Post(model.client);
			DependencyService.Get<ICustomPrgressBar>().Stop();
			if (result.IsSuccessStatusCode)
			{
				await DisplayAlert("Success", "Client added", "Ok!");

			}
			else
			{

				await DisplayAlert("Faliure", "Client could not be added ", "Ok!");
			}
			Navigation.PopAsync();
		}
	}
}
